theory MonaCo_More
imports MonaCo
begin

lemma (in norm) "ox (B p \<Phi>) = case_polarity fBall fBex p \<Phi> ox"
  unfolding ox_def[abs_def]
  apply simp
  done

lemma [simp]:
  "x |\<notin>| FV \<Longrightarrow> p < len I \<Longrightarrow> restrict_interp FV SV (fo_update x p I) = restrict_interp FV SV I"
  apply transfer
  apply (auto split: if_splits)
  done

lemma "x |\<notin>| fov \<phi> \<Longrightarrow>
  satb I (Q p (FO x) \<phi>) = satb I (B (pneg p) {|Q p (FO x) (B (pneg p) {||}), \<phi>|})"
  apply simp
  apply (subst (1 2 3 4) satb.sat_gen_restrict_interp[OF fsubset_refl fsubset_refl])
  apply auto
  done

lemma "satb I (Q p (FO x) (B p \<Phi>)) = satb I (B p (Q p (FO x) |`| \<Phi>))"
  apply auto
  done

end