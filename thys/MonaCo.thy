(*<*)
theory MonaCo
imports
  "~~/src/HOL/Library/Multiset"
  "$AFP/Formula_Derivatives/FSet_More"
  "$AFP/Formula_Derivatives/Automaton"
begin
(*>*)

section \<open>Preliminaries\<close>

context includes fset.lifting begin

lemma ffUnion_subset_iff: "ffUnion X |\<subseteq>| Y = fBall X (\<lambda>X. X |\<subseteq>| Y)"
  by transfer auto

lemma ffUnion_iff: "x |\<in>| ffUnion X = fBex X (\<lambda>X. x |\<in>| X)"
  by transfer simp

lemma ffUnion_fempty[simp]: "ffUnion {||} =  {||} "
  by transfer simp

lemma ffUnion_finsert[simp]: "ffUnion (finsert X Y) = X |\<union>| ffUnion Y"
  by transfer simp

abbreviation "fremove x X \<equiv> X |-| {|x|}"

lemma fremove_subset_iff:
  "fremove x X |\<subseteq>| Y \<longleftrightarrow> (if x |\<in>| X then X |\<subseteq>| finsert x Y else X |\<subseteq>| Y)"
  by transfer auto

lemma fMax_mono: "X \<noteq> {||} \<Longrightarrow> (\<forall>x. x |\<in>| X \<longrightarrow> (\<exists>y. y |\<in>| Y \<and> x \<le> y)) \<Longrightarrow> fMax X \<le> fMax Y"
  by transfer (auto 0 3 simp: not_le dest!: iffD1[OF Max_less_iff])

private lemma [simp]: "V \<inter> {z. z \<noteq> x} = V - {x}"
  by auto

lemma fMax_fimage_fun_upd:
  "fMax (f(x := n) |`| V) =
   (if x |\<in>| V then
     if V |-| {|x|} = {||}
     then n
     else max n (fMax (f |`| (V |-| {|x|})))
   else fMax (f |`| V))"
  by transfer (auto simp: max_def not_le fun_upd_def dest!: subset_singletonD)

end

lemma insort_sorted_list_of_set_min:
  fixes b A
  assumes "finite A" "\<forall>a \<in> A. b < a"
  shows "insort b (sorted_list_of_set (A - {b})) = b # sorted_list_of_set A"
  using assms by (induct A arbitrary: b rule: finite_linorder_min_induct)
    (auto simp: sorted_list_of_set_remove intro!: remove1_idem)

lemma set_sublists: "distinct ys \<Longrightarrow> sorted ys \<Longrightarrow>
  set (sublists ys) = {xs. distinct xs \<and> sorted xs \<and> set xs \<subseteq> set ys}"
  by (rule set_eqI, induct ys; rename_tac xs; case_tac xs)
    (auto 0 4 simp: Let_def sorted_Cons image_iff)

lemma distinct_sublists_sorted_list_of_set:
  "finite X \<Longrightarrow> distinct (sublists (sorted_list_of_set X))"
  by (induct X rule: finite_linorder_min_induct)
    (auto simp: insort_sorted_list_of_set_min Let_def distinct_map set_sublists)

lemma setsum_pow2_image_Suc:
  "finite X \<Longrightarrow> setsum (op ^ (2 :: nat)) (Suc ` X) = 2 * setsum (op ^ 2) X"
  by (induct X rule: finite_induct) (auto intro: trans[OF setsum.insert])

lemma setsum_pow2_insert0:
  "\<lbrakk>finite X; 0 \<notin> X\<rbrakk> \<Longrightarrow> setsum (op ^ (2 :: nat)) (insert 0 X) = Suc (setsum (op ^ 2) X)"
  by (induct X rule: finite_induct) (auto intro: trans[OF setsum.insert])

lemma setsum_pow2_upto: "setsum (op ^ (2 :: nat)) {0 ..< x} = 2 ^ x - 1"
  by (induct x) (auto simp: algebra_simps)

lemma setsum_pow2_inj:
  "\<lbrakk>finite X; finite Y; (\<Sum>x\<in>X. 2 ^ x :: nat) = (\<Sum>x\<in>Y. 2 ^ x)\<rbrakk> \<Longrightarrow> X = Y"
  (is "_ \<Longrightarrow> _ \<Longrightarrow> ?f X = ?f Y \<Longrightarrow> _")
proof (induct X arbitrary: Y rule: finite_linorder_max_induct)
  case (insert x X)
  from insert(2) have "?f X \<le> ?f {0 ..< x}" by (intro setsum_mono2) auto
  also have "\<dots> < 2 ^ x" by (induct x) simp_all
  finally have "?f X < 2 ^ x" .
  moreover from insert(1,2) have *: "?f X + 2 ^ x = ?f Y"
    using trans[OF sym[OF insert(5)] setsum.insert] by auto
  ultimately have "?f Y < 2 ^ Suc x" by simp

  have "\<forall>y \<in> Y. y \<le> x"
  proof (rule ccontr)
    assume "\<not> (\<forall>y\<in>Y. y \<le> x)"
    then obtain y where "y \<in> Y" "Suc x \<le> y" by auto
    from this(2) have "2 ^ Suc x \<le> (2 ^ y :: nat)" by (intro power_increasing) auto
    also from \<open>y \<in> Y\<close> insert(4) have "\<dots> \<le> ?f Y" by (metis order.refl setsum.remove trans_le_add1)
    finally show False using \<open>?f Y < 2 ^ Suc x\<close> by simp
  qed

  { assume "x \<notin> Y"
    with \<open>\<forall>y \<in> Y. y \<le> x\<close> have "?f Y \<le> ?f {0 ..< x}" by (intro setsum_mono2) (auto simp: le_less)
    also have "\<dots> < 2 ^ x" by (induct x) simp_all
    finally have "?f Y < 2 ^ x" .
    with * have False by auto
  }
  then have "x \<in> Y" by blast

  from insert(4) have "?f (Y - {x}) + 2 ^ x = ?f (insert x (Y - {x}))"by (subst setsum.insert) auto
  also have "\<dots> = ?f X + 2 ^ x" unfolding * using \<open>x \<in> Y\<close> by (simp add: insert_absorb)
  finally have "?f X = ?f (Y - {x})" by simp
  with insert(3,4) have "X = Y - {x}" by simp
  with \<open>x \<in> Y\<close> show ?case by auto
qed simp

lemma finite_pow2_eq:
  fixes n :: nat
  shows "finite {i. 2 ^ i = n}"
proof -
  have "(op ^ 2) ` {i. 2 ^ i = n} \<subseteq> {n}" by auto
  then have "finite ((op ^ (2 :: nat)) ` {i. 2 ^ i = n})" by (rule finite_subset) blast
  then show "finite {i. 2 ^ i = n}" by (rule finite_imageD) (auto simp: inj_on_def)
qed

lemma finite_pow2_le[simp]:
  fixes n :: nat
  shows "finite {i. 2 ^ i \<le> n}"
  by (induct n) (auto simp: le_Suc_eq finite_pow2_eq)

lemma le_pow2[simp]: "x \<le> y \<Longrightarrow> x \<le> 2 ^ y"
  by (induct x arbitrary: y) (force simp add: Suc_le_eq order.strict_iff_order)+

lemma ld_bounded: "2 ^ Max {i. 2 ^ i \<le> Suc n} \<le> Suc n" (is "2 ^ ?m \<le> Suc n")
proof -
  have "?m \<in> {i. 2 ^ i \<le> Suc n}" by (rule Max_in) (auto intro: exI[of _ 0])
  then show "2 ^ ?m \<le> Suc n" by simp
qed

fun ld where
  "ld 0 = 0"
| "ld (Suc 0) = 0"
| "ld n = Suc (ld (n div 2))"

lemma ld_alt[simp]: "n > 0 \<Longrightarrow> ld n = Max {i. 2 ^ i \<le> n}"
proof (safe intro!: Max_eqI[symmetric])
  assume "n > 0" then show "2 ^ ld n \<le> n" by (induct n rule: ld.induct) auto
next
  fix y
  assume "2 ^ y \<le> n"
  then show "y \<le> ld n"
  proof (induct n arbitrary: y rule: ld.induct)
    case (3 z)
    then have "y - 1 \<le> ld (Suc (Suc z) div 2)"
      by (cases y) simp_all
    then show ?case by simp
  qed (auto simp: le_eq_less_or_eq)
qed simp

abbreviation restrict1 :: "'a fset \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'a \<Rightarrow> ('b :: zero)" where
  "restrict1 X f x \<equiv> if x |\<in>| X then f x else 0"
abbreviation restrict2 :: "'a fset \<Rightarrow> ('a \<Rightarrow> 'b fset) \<Rightarrow> 'a \<Rightarrow> 'b fset" where
  "restrict2 X f x \<equiv> if x |\<in>| X then f x else {||}"

abbreviation REV0 :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "REV0 n x \<equiv> n - x - 1"


lemma eq_diff_iff2:
  fixes m n k :: nat
  shows "m \<le> k \<Longrightarrow> n \<le> k \<Longrightarrow> (k - m = k - n) = (m = n)"
  by auto

lemma REV0_inj:
  fixes P Q
  shows "fBall (P |\<union>| Q) (\<lambda>n. n < m) \<Longrightarrow> (REV0 m |`| P = REV0 m |`| Q) = (P = Q)"
  by (auto dest!: fset.inj_map_strong[rotated -1] iffD1[OF eq_diff_iff2, rotated -1]
    simp: fBall_def ball_Un)

declare REV0_inj[simplified, simp]

lemma inj_on_fimage_finter:
  fixes A B C
  shows "inj_on f (fset C) \<Longrightarrow> A |\<subseteq>| C \<Longrightarrow> B |\<subseteq>| C \<Longrightarrow> f |`| (A |\<inter>| B) = f |`| A |\<inter>| f |`| B"
  including fset.lifting by (transfer; subst inj_on_image_Int) auto

lemma inj_on_fimage_fset_diff:
  fixes A B C
  shows "inj_on f (fset C) \<Longrightarrow> A |-| B |\<subseteq>| C \<Longrightarrow> B |\<subseteq>| C \<Longrightarrow> f |`| (A |-| B) = f |`| A |-| f |`| B"
  including fset.lifting by (transfer; subst inj_on_image_set_diff) auto

lemma inj_on_REV0: "fBall P (\<lambda>n. n < m) \<Longrightarrow> inj_on (REV0 m) (fset P)"
  by (auto simp: inj_on_def fBall_def dest!: iffD1[OF eq_diff_iff2, rotated -1])

lemma eq_finsert:
   "finsert x Y = Z \<longleftrightarrow> (x |\<in>| Z \<and> Z - {|x|} = Y - {|x|})"
   "Z = finsert x Y \<longleftrightarrow> (x |\<in>| Z \<and> Z - {|x|} = Y - {|x|})"
  by auto

lemma fMin_eq_iff:
  assumes "X \<noteq> {||}"
  shows "fMin X = x \<longleftrightarrow> x |\<in>| X \<and> fBall X (\<lambda>y. x \<le> y)" "x = fMin X \<longleftrightarrow> x |\<in>| X \<and> fBall X (\<lambda>y. x \<le> y)"
  by (auto simp: assms intro: fMin_in fMin_eqI fMin_eqI[symmetric])

lemma fMax_eq_iff:
  assumes "X \<noteq> {||}"
  shows "fMax X = x \<longleftrightarrow> x |\<in>| X \<and> fBall X (\<lambda>y. y \<le> x)" "x = fMax X \<longleftrightarrow> x |\<in>| X \<and> fBall X (\<lambda>y. y \<le> x)"
  by (auto simp: assms intro: fMax_in fMax_eqI fMax_eqI[symmetric])

lemma fminus_eq_fempty_iff2:
  "{||} = X |-| Y \<longleftrightarrow> X |\<subseteq>| Y"
  by auto

lemma fremove_absorb: "x |\<notin>| X \<Longrightarrow> fremove x X = X"
  by auto

lemma fremove_finsert:
  fixes a b X
  shows "a \<noteq> b \<Longrightarrow> fremove a (finsert b X) = finsert b (fremove a X)"
  by auto

lemma Suc_fimage_REV0: "fBall X (\<lambda>x. x < n) \<Longrightarrow> Suc |`| REV0 n |`| X = REV0 (Suc n) |`| X"
  by (force simp: fBall_def fmember.rep_eq image_iff intro: bexI[rotated])

lemma fimage_REV0_Suc: "fBall X (\<lambda>x. x < n) \<Longrightarrow> REV0 (Suc n) |`| Suc |`| X = REV0 n |`| X"
  by (force simp: fBall_def fmember.rep_eq image_iff intro: bexI[rotated])

lemma finsert_0_fimage_REV0:
   "fBall X (\<lambda>x. x < n) \<Longrightarrow> finsert 0 (REV0 (Suc n) |`| X) = REV0 (Suc n) |`| (finsert n X)"
  by (force simp: fBall_def fmember.rep_eq image_iff intro: bexI[rotated])

lemma finsert_fimage_REV0:
   "fBall X (\<lambda>x. x < n) \<Longrightarrow> finsert n (REV0 n |`| X) = REV0 (Suc n) |`| (finsert 0 (Suc |`| X))"
  by (force simp: fBall_def fmember.rep_eq image_iff intro: bexI[rotated])

lemma REV0_REV0_comp[simplified, simp]:
  "fBall P (\<lambda>n. n < len I) \<Longrightarrow> (REV0 (len I) \<circ> REV0 (len I)) |`| P = P"
  apply (cases P)
  apply (auto simp: fBall.rep_eq fmember.rep_eq image_iff Suc_diff_Suc order.strict_implies_order)
  done

lemma le_pred: "n \<le> m - Suc 0 \<longleftrightarrow> (if m = 0 then n = 0 else Suc n \<le> m)"
  by auto

lemma fmax_pred[simp]:
  assumes "Suc m |\<in>| P"
  shows "fMax ((\<lambda>p. p - Suc 0) |`| P) = fMax P - 1"
  using fMax_ge[OF assms, unfolded Suc_le_eq] assms Suc_le_eq
  apply (auto simp: le_pred not_le gr0_conv_Suc fimage_iff
   intro!: fMax_eqI fBexI[of _ "fMax P"] fMax_in split: if_splits)
  by fastforce

lemma fmax_remove0[simp]:
  assumes "Suc m |\<in>| P"
  shows "fMax (fremove 0 P) = fMax P"
  using fMax_ge[OF assms, unfolded Suc_le_eq] assms by (auto intro!: fMax_eqI fMax_in)

lemma fimage_Suc_eq_fsingleton[simp]: "Suc |`| X = {|x|} \<longleftrightarrow> (\<exists>y. X = {|y|} \<and> x = Suc y)"
  by (cases x) (auto simp: fset_eq_iff)

lemma fMin_fimage_Suc: "X \<noteq> {||} \<Longrightarrow> fMin (Suc |`| X) = Suc (fMin X)"
  by (rule fMin_eqI) auto

lemma fMax_fimage_Suc: "X \<noteq> {||} \<Longrightarrow> fMax (Suc |`| X) = Suc (fMax X)"
  by (rule fMax_eqI) auto

lemma mod2_neq_Suc0: "x mod 2 \<noteq> Suc 0 \<longleftrightarrow> (\<exists>k. x = 2 * k)"
  by (cases "x mod 2") auto

lemma Max_pow2_le: "x < 2 ^ n \<Longrightarrow> Max {i. (2 :: nat) ^ i \<le> 2 ^ n + x} = n"
proof (rule Max_eqI, unfold mem_Collect_eq)
  fix y assume "x < 2 ^ n" "2 ^ y \<le> 2 ^ n + x"
  then have "(2 :: nat) ^ y < 2 ^ Suc n" by simp
  then show "y \<le> n" by (cases y) auto
qed simp_all

lemma eq_ldI:
  assumes "x < 2 ^ l" "2 ^ l + x = n"
  shows "l = ld n"
using assms proof ((cases "n > 0"; simp), intro Max_eqI[symmetric], unfold mem_Collect_eq)
  fix y
  assume "2 ^ y \<le> n"
  then have "2 ^ y \<le> 2 ^ l + x" using assms(2) by simp
  also have "\<dots> < 2 ^ Suc l" using assms(1) by simp
  finally show "y \<le> l" by (cases y) auto
qed simp_all

lemma setsum_pow2_REV_bounded:
  assumes "\<forall>x \<in> X. x < n"
  shows "(\<Sum>i\<in>X. (2 :: nat) ^ (n - Suc i)) < 2 ^ n" (is "?l < ?r")
proof -
  from assms have "?l \<le> (\<Sum>i\<in>{x. x < n}. 2 ^ (n - Suc i))"
    by (intro setsum_mono2) auto
  also have "\<dots> = (\<Sum>i\<in>{x. x < n}. 2 ^ i)"
    by (rule setsum.reindex_cong[where l = "\<lambda>x. n - Suc x"])
      (auto 0 3 simp: inj_on_def image_iff less_iff_Suc_add intro!: add.commute)
  also have "\<dots> < 2 ^ n"
    by (induct n) (auto simp: less_Suc_eq Collect_disj_eq)
  finally show ?thesis .
qed

lemma fremove_0_fimage_Suc[simp]: "fremove 0 (Suc |`| P) = Suc |`| P"
  by auto

lemma pred_alt: "x - Suc 0 = (case x of 0 \<Rightarrow> 0 | Suc m \<Rightarrow> m)"
  by (cases x) auto



section \<open>Alphabet\<close>

record ('fo, 'so) alphabet =
  afo :: "'fo fset"
  aso :: "'so fset"

definition mk_alphabet where
  "mk_alphabet FV SV =
     List.bind (map set (sublists (sorted_list_of_set (fset FV)))) (\<lambda>X.
       map (\<lambda>Y. \<lparr>afo = Abs_fset X, aso = Abs_fset Y\<rparr>)
         (map set (sublists (sorted_list_of_set (fset SV)))))"

lemma distinct_mk_alphabet[simp]: "distinct (mk_alphabet FV SV)"
  unfolding mk_alphabet_def List.bind_def
  by (rule distinct_concat) (auto 0 3 intro: sorted_distinct_set_unique
    simp: distinct_map distinct_sublists_sorted_list_of_set inj_on_def set_sublists Abs_fset_inject)

lemma set_mk_alphabet: "set (mk_alphabet FV SV) =
  (\<lambda>(X, Y). \<lparr>afo = Abs_fset X, aso = Abs_fset Y\<rparr>) ` (Pow (fset FV) \<times> Pow (fset SV))"
  unfolding mk_alphabet_def List.bind_def
  apply (auto simp: set_sublists image_iff Abs_fset_inject finite_subset)
  apply (rename_tac X Y)
  apply (rule_tac x = "sorted_list_of_set X" in exI; auto simp: finite_subset)
  apply (rename_tac X Y)
  apply (rule_tac x = "sorted_list_of_set Y" in exI; auto simp: finite_subset)
  done

abbreviation "restrict_letter FV SV b \<equiv> \<lparr>afo = afo b |\<inter>| FV, aso = aso b |\<inter>| SV\<rparr>"


section \<open>Interpretations with Length\<close>

record ('fo, 'so) interp0 =
  fo0_dom :: "'fo fset"
  fo0 :: "'fo \<Rightarrow> nat"
  so0 :: "'so \<Rightarrow> nat fset"

abbreviation ok_interp where
  "ok_interp I m \<equiv>
    (\<forall>x. x |\<in>| fo0_dom I \<longrightarrow> fo0 I x < m) \<and>
    (\<forall>x. x |\<notin>| fo0_dom I \<longrightarrow> fo0 I x = 0) \<and>
    (\<forall>X. \<forall>n. n |\<in>| so0 I X \<longrightarrow> n < m)"

typedef ('fo, 'so) interp = "{(I :: ('fo, 'so) interp0, m). ok_interp I m}"
  by (intro exI[of _ "(\<lparr>fo0_dom = {||}, fo0 = \<lambda>_. 0, so0 = \<lambda>_. {||} \<rparr>, 0)"]) auto

setup_lifting type_definition_interp

lift_definition interp0 :: "('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp0" is fst .
lift_definition len :: "('fo, 'so) interp \<Rightarrow> nat" is snd .
lift_definition restrict_interp :: "'fo fset \<Rightarrow> 'so fset \<Rightarrow> ('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp"
  is "\<lambda>FV SV (I, n).
   (\<lparr>fo0_dom = fo0_dom I |\<inter>| FV, fo0 = restrict1 FV (fo0 I), so0 = restrict2 SV (so0 I)\<rparr>, n)"
  by (auto split: if_splits)
abbreviation "fo_dom \<equiv> fo0_dom o interp0"
abbreviation "fo \<equiv> fo0 o interp0"
abbreviation "so \<equiv> so0 o interp0"
lift_definition fo_update :: "'fo \<Rightarrow> nat \<Rightarrow> ('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>x p (I, m). (fo0_dom_update (finsert x) (fo0_update (\<lambda>f. f(x := p)) I), max (Suc p) m)"
  by (force split: option.splits if_splits simp: max_def)
lift_definition so_update :: "'so \<Rightarrow> nat fset \<Rightarrow> ('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>X P (I, m). (so0_update (\<lambda>f. f(X := P)) I, if P = {||} then m else max (Suc (fMax P)) m)"
  by (fastforce dest: fMax_boundedD(1)[rotated] split: option.splits if_splits simp: max_def)

lemma interp0_restrict_interp[simp]:
  "interp0 (restrict_interp FV SV I) =
   \<lparr>fo0_dom = fo_dom I |\<inter>| FV, fo0 = restrict1 FV (fo I), so0 = restrict2 SV (so I)\<rparr>"
  by transfer (auto simp: fun_eq_iff)
lemma len_restrict_interp[simp]: "len (restrict_interp FV SV I) = len I"
  by transfer auto
lemma restrict_interp_update[simp]:
  "fo_update x p (restrict_interp FV SV I) = restrict_interp (finsert x FV) SV (fo_update x p I)"
  "so_update X P (restrict_interp FV SV I) = restrict_interp FV (finsert X SV) (so_update X P I)"
  by (transfer; auto simp: fun_eq_iff)+

lemma interp0_update[simp]:
  "interp0 (fo_update x n I) = fo0_dom_update (finsert x) (fo0_update (\<lambda>f. f(x := n)) (interp0 I))"
  "interp0 (so_update X N I) = so0_update (\<lambda>f. f(X := N)) (interp0 I)"
  by (transfer; auto)+

lemma fo_len[simp]: "x |\<in>| fo0_dom (interp0 I) \<Longrightarrow> fo0 (interp0 I) x = n \<Longrightarrow> n < len I"
  by transfer auto
lemma so_len[simp]: "n |\<in>| so0 (interp0 I) X \<Longrightarrow> n < len I"
  by transfer auto

lift_definition lift :: "nat \<Rightarrow> ('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>m (I, n). (\<lparr>fo0_dom = fo0_dom I,
   fo0 = restrict1 (fo0_dom I) ((\<lambda>r. r + (Suc m - n)) o fo0 I),
   so0 = fimage (\<lambda>r. r + (Suc m - n)) o so0 I\<rparr>, max (Suc m) n)"
  by force

lemma lift_id[simp]: "n < len I \<Longrightarrow> lift n I = I"
  by transfer (auto intro!: interp0.equality)

lift_definition REV :: "('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>(I, n). (\<lparr>fo0_dom = fo0_dom I,
   fo0 = restrict1 (fo0_dom I) (REV0 n o fo0 I), so0 = fimage (REV0 n) o so0 I\<rparr>, n)"
  by fastforce

lemma fo_update_REV[simp]: "fo_update x n (REV I) =
  (let J = lift n I in REV (fo_update x (REV0 (len J) n) J))"
  by transfer (auto simp: Let_def fun_eq_iff max_def)

lemma so_update_REV[simp]: "so_update X N (REV I) =
  (let J = if N = {||} then I else lift (fMax N) I in REV (so_update X (REV0 (len J) |`| N) J))"
  apply (transfer fixing: X)
  apply (auto 0 0 simp: Let_def fun_eq_iff max_def fmember.rep_eq image_iff Suc_le_eq)
  apply (metis (no_types, lifting) Suc_diff_Suc Suc_leI add_gr_0 diff_Suc_less diff_diff_cancel fMax_boundedD(1) less_imp_add_positive nat.inject notin_fset)
  apply (metis (no_types, lifting) Suc_diff_Suc Suc_leI add_gr_0 diff_Suc_less diff_diff_cancel fMax_boundedD(1) less_imp_add_positive nat.inject notin_fset)
  apply (metis (mono_tags, lifting))
  apply (metis (mono_tags, lifting))
  apply (metis (no_types, lifting) Suc_diff_Suc Suc_leI add_gr_0 diff_Suc_less diff_diff_cancel fMax_boundedD(1) less_imp_add_positive nat.inject notin_fset)
  apply (metis (no_types, lifting) Suc_diff_Suc Suc_leI add_gr_0 diff_Suc_less diff_diff_cancel fMax_boundedD(1) less_imp_add_positive nat.inject notin_fset)
  apply (metis (no_types, lifting) diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (mono_tags, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (mono_tags, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (mono_tags, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting))
  apply (metis (no_types, lifting))
  apply (metis (mono_tags, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (mono_tags, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting) Suc_le_lessD diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty order_refl zero_less_Suc)
  apply (metis (no_types, lifting) Suc_le_lessD diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty order_refl zero_less_Suc)
  apply (metis (no_types, lifting) Suc_le_lessD diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty order_refl zero_less_Suc)
  apply (metis (no_types, lifting) Suc_le_lessD diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty order_refl zero_less_Suc)
  apply (metis (no_types, lifting) Suc_le_lessD diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty order_refl zero_less_Suc)
  apply (metis (mono_tags, lifting) diff_Suc_less fMax_in fimageE fimage_is_fempty less_imp_Suc_add zero_less_Suc)
  apply (metis (no_types, lifting) Suc_le_lessD diff_Suc_Suc diff_Suc_less fMax_in fimageE fimage_is_fempty order_refl zero_less_Suc)
  done

lemma interp0_REV[simp]: "interp0 (REV I) =
  \<lparr>fo0_dom = fo_dom I,
  fo0 = restrict1 (fo_dom I) (REV0 (len I) o fo I),
  so0 = fimage (REV0 (len I)) o so I\<rparr>"
  by transfer (auto simp: fun_eq_iff)

lemma len_REV[simp]: "len (REV I) = len I"
  by transfer auto

lift_definition CONS :: "('fo, 'so) alphabet \<Rightarrow> ('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>b (I, n). (\<lparr>fo0_dom = fo0_dom I |\<union>| afo b,
   fo0 = \<lambda>x. if x |\<in>| afo b then 0 else restrict1 (fo0_dom I) (Suc o fo0 I) x,
   so0 = \<lambda>x. (if x |\<in>| aso b then finsert 0 else id) (Suc |`| so0 I x)\<rparr>, Suc n)"
  by (auto split: if_splits)

lemma interp0_CONS[simp]: "interp0 (CONS b I) =
  \<lparr>fo0_dom = fo_dom I |\<union>| afo b,
   fo0 = \<lambda>x. if x |\<in>| afo b then 0 else restrict1 (fo_dom I) (Suc o fo I) x,
   so0 = \<lambda>x. (if x |\<in>| aso b then finsert 0 else id) (Suc |`| so I x)\<rparr>"
  by transfer (auto simp: fun_eq_iff)

lemma fo_update_CONS[simp]: "fo_update x p (CONS b I) =
  (case p of
   0 \<Rightarrow> CONS (afo_update (finsert x) b) I
  | Suc m \<Rightarrow> CONS (afo_update (fremove x) b) (fo_update x m I))"
  by transfer (auto split: nat.splits simp: fun_eq_iff)

lemma so_update_CONS: "so_update X P (CONS b I) =
  CONS (aso_update (if 0 |\<in>| P then finsert X else fremove X) b)
   (so_update X ((\<lambda>p. p - 1) |`| fremove 0 P) I)"
  apply transfer
  apply (auto simp: fun_eq_iff fimage_iff max_def Suc_le_eq not_less gr0_conv_Suc le_pred
   dest!: fsubset_fsingletonD  split: nat.splits if_splits)
  apply (metis (mono_tags, lifting) Suc_pred antisym_conv2 fbspec finsert_fminus finsert_iff le0)
  apply (metis (mono_tags, lifting) Suc_pred antisym_conv2 le0)
  apply (metis (mono_tags, lifting) Suc_pred antisym_conv2 fbspec finsert_fminus finsert_iff le0)
  apply (metis (mono_tags, lifting) Suc_pred antisym_conv2 le0)
  apply (smt Suc_lessE diff_Suc_1 fMax_in fempty_iff finsert_iff fmax_pred fmax_remove0 fminus_finsert_absorb gr_implies_not0 less_irrefl_nat less_trans_Suc set_finsert)
  using fMax_ge apply fastforce
  using fMax_ge apply fastforce
  using fMax_ge apply fastforce
  apply (metis (mono_tags, lifting) Suc_pred fBexI le0 le_eq_less_or_eq)
  apply (metis (mono_tags, lifting) Suc_pred le0 le_eq_less_or_eq)
  apply (smt Suc_lessE diff_Suc_1 fMax_in fempty_iff finsert_iff fmax_pred fmax_remove0 fminus_finsert_absorb gr_implies_not0 less_irrefl_nat less_trans_Suc set_finsert)
  apply (metis fMax_in fempty_iff)
  apply simp
  apply simp
  done

lemma len_CONS[simp]: "len (CONS b I) = Suc (len I)"
  by transfer auto

lift_definition empty_interp :: "('fo, 'so) interp" is
  "(\<lparr>fo0_dom = {||}, fo0 = \<lambda>_. 0, so0 = \<lambda>_. {||}\<rparr>, 0)"
  by simp

lemma len_empty_interp[simp]: 
  "len empty_interp = 0"
  by transfer simp

lemma so_update_empty[simp]: "so_update x {||} empty_interp = empty_interp"
  by transfer auto

lemma interp0_empty_interp[simp]: 
  "interp0 empty_interp = \<lparr>fo0_dom = {||}, fo0 = \<lambda>_. 0, so0 = \<lambda>_. {||}\<rparr>"
  by transfer simp

lemma len_eq_0_iff[simp]: "len I = 0 \<longleftrightarrow> I = empty_interp"
  by transfer (auto intro!: interp0.equality)

lemma restrict_interp_CONS[simp]:
  "restrict_interp FV SV (CONS b I) = CONS (restrict_letter FV SV b) (restrict_interp FV SV I)"
  by transfer (auto simp: fun_eq_iff)

lemma fo_update_fo: "x |\<in>| fo_dom I \<Longrightarrow> fo_update x (fo I x) I = I"
  by (transfer fixing: x) (auto intro: interp0.equality)

lemma CONS_fo_update[simp]:
  "p < len I \<Longrightarrow>
  CONS (afo_update (finsert x) b) (fo_update x p I) = CONS (afo_update (finsert x) b) I"
  by transfer auto


lemma restrict_empty_interp[simp]: "restrict_interp FV SV empty_interp = empty_interp"
  by transfer (auto split: if_splits)

lift_definition head :: "('fo, 'so) interp \<Rightarrow> ('fo, 'so) alphabet" is
  "\<lambda>(I, n). \<lparr>afo = ffilter (\<lambda>x. fo0 I x = 0) (fo0_dom I), aso = Abs_fset {x. 0 |\<in>| so0 I x}\<rparr>" .
lift_definition tail :: "('fo, 'so) interp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>(I, n). case n of 0 \<Rightarrow> (I, 0) | Suc n \<Rightarrow>
    let dom = ffilter (\<lambda>x. fo0 I x \<noteq> 0) (fo0_dom I)
    in (\<lparr>fo0_dom = dom, fo0 = restrict1 dom (\<lambda>x. fo0 I x - 1),
      so0 = \<lambda>X. (\<lambda>x. x - 1) |`| (so0 I X - {|0|})\<rparr>, n)"
  by (force split: nat.splits simp: gr0_conv_Suc)

lemma ex_fold_CONS: "\<exists>xs J.
  restrict_interp FV SV I = fold CONS xs J \<and> len J = 0 \<and> len I = length xs \<and>
  (\<forall>x \<in> set xs. afo x |\<subseteq>| FV \<and> aso x |\<subseteq>| SV)"
proof (induct "len I" arbitrary: I)
  case (Suc m)
  from Suc(2) obtain a J where "restrict_interp FV SV I = CONS a (restrict_interp FV SV J)"
    (is "?I = CONS a ?J") and "afo a |\<subseteq>| FV \<and> aso a |\<subseteq>| SV"
    apply atomize_elim
    apply (rule exI[of _ "head ?I"])
    apply (rule exI[of _ "tail ?I"])
    apply transfer apply (auto split: if_splits simp: fun_eq_iff o_def fmember.rep_eq Abs_fset_inverse)
    done
  moreover with Suc(2) have "len ?J = m"
    by (simp add: len_restrict_interp[symmetric, of I FV SV])
  with Suc(1)[of J] obtain xs K where "?J = fold CONS xs K" "len K = 0" "len ?J = length xs"
     "\<forall>x \<in> set xs. afo x |\<subseteq>| FV \<and> aso x |\<subseteq>| SV" unfolding len_restrict_interp by auto
  ultimately show ?case
     by (intro exI[of _ "xs @ [a]"] exI[of _ K])
       (auto simp: len_restrict_interp[symmetric, of I FV SV])
qed simp

lemma le_fo_dom_restrict: "FV |\<subseteq>| fo_dom I \<longleftrightarrow> fo_dom (restrict_interp FV SV I) = FV"
  by transfer auto



section \<open>Formulas\<close>

datatype polarity = Pos --\<open>conjunctive\<close> | Neg --\<open>disjunctive\<close>

primrec pneg where
  "pneg Pos = Neg"
| "pneg Neg = Pos"

datatype (VFOV: 'fo, VSOV: 'so) var = FO 'fo | SO 'so

declare polarity.splits[split] var.splits[split]

datatype ('a, 'fo, 'so) aformula =
  A (pol: polarity) 'a
| B (pol: polarity) "('a, 'fo, 'so) aformula fset"
| Q (pol: polarity) "('fo, 'so) var" "('a, 'fo, 'so) aformula"

abbreviation "a \<equiv> A Pos"
abbreviation "na \<equiv> A Neg"
abbreviation "supr \<equiv> B Neg"
abbreviation "infi \<equiv> B Pos"
abbreviation "tt \<equiv> infi {||}"
abbreviation "ff \<equiv> supr {||}"
abbreviation "c p \<equiv> B p {||}"
abbreviation "all \<equiv> Q Pos"
abbreviation "ex \<equiv> Q Neg"

locale aatomic =
  fixes FOV0 :: "'a \<Rightarrow> 'fo set"
  and   SOV0 :: "'a \<Rightarrow> 'so set"
  assumes finite_support[simp]: "finite (FOV0 x)" "finite (SOV0 x)"
begin

primrec fov where
  "fov (A p x) = FOV0 x"
| "fov (B p \<Phi>) = \<Union>(fset (fov |`| \<Phi>))"
| "fov (Q p V \<phi>) = fov \<phi> - VFOV V"

primrec sov where
  "sov (A p x) = SOV0 x"
| "sov (B p \<Phi>) = \<Union>(fset (sov |`| \<Phi>))"
| "sov (Q p V \<phi>) = sov \<phi> - VSOV V"

lemma finite_fov[simp]: "finite (fov \<phi>)"
  by (induct \<phi>) auto
lemma finite_sov[simp]: "finite (sov \<phi>)"
  by (induct \<phi>) auto

end

locale sat_sat0_gen =
fixes sat0 :: "('fo, 'so) interp \<Rightarrow> 'a \<Rightarrow> bool"
and   fo_restrict :: "nat \<Rightarrow> nat \<Rightarrow> bool"
and   so_restrict :: "nat \<Rightarrow> nat fset \<Rightarrow> bool"  
begin

primrec sat_gen :: "('fo, 'so) interp \<Rightarrow> ('a, 'fo, 'so) aformula \<Rightarrow> bool" where
  "sat_gen I (A p x) = case_polarity id HOL.Not p (sat0 I x)"
| "sat_gen I (B p \<Phi>) = case_polarity fBall fBex p (sat_gen I |`| \<Phi>) id"
| "sat_gen I (Q p V \<phi>) = (case (p, V) of
   (Pos, FO x) \<Rightarrow> \<forall>p. fo_restrict (len I) p \<longrightarrow> sat_gen (fo_update x p I) \<phi>
  | (Pos, SO X) \<Rightarrow> \<forall>P. so_restrict (len I) P \<longrightarrow> sat_gen (so_update X P I) \<phi>
  | (Neg, FO x) \<Rightarrow> \<exists>p. fo_restrict (len I) p \<and> sat_gen (fo_update x p I) \<phi>
  | (Neg, SO X) \<Rightarrow> \<exists>P. so_restrict (len I) P \<and> sat_gen (so_update X P I) \<phi>)"

end


section \<open>Atomic Formulas\<close>

datatype (FOV0: 'fo, SOV0: 'so) atomic =
  Less 'fo 'fo
| In 'fo 'so
| Len nat
| Eq_Const bool 'fo nat
| Plus_FO 'fo 'fo nat
| Eq_FO 'fo 'fo
| Eq_SO 'so 'so
| Suc_SO bool bool 'so 'so
| Empty 'so
| Singleton 'so
| Subset 'so 'so
| Eq_Max 'fo 'so
| Eq_Min 'fo 'so
| Eq_Union 'so 'so 'so
| Eq_Inter 'so 'so 'so
| Eq_Diff 'so 'so 'so
| Disjoint 'so 'so
| Eq_Presb bool 'so nat

type_synonym ('fo, 'so) formula = "(('fo, 'so) atomic, 'fo, 'so) aformula"

inductive lformula0 where
  "lformula0 (Less x y)"
| "lformula0 (In x X)"
| "lformula0 (Eq_Const True x n)"
| "lformula0 (Plus_FO x y n)"
| "lformula0 (Eq_FO x y)"
| "lformula0 (Eq_SO X Y)"
| "lformula0 (Suc_SO False b X Y)"
| "lformula0 (Empty X)"
| "lformula0 (Singleton X)"
| "lformula0 (Subset X Y)"
| "lformula0 (Eq_Max x X)"
| "lformula0 (Eq_Min x X)"
| "lformula0 (Eq_Union X Y Z)"
| "lformula0 (Eq_Inter X Y Z)"
| "lformula0 (Eq_Diff X Y Z)"
| "lformula0 (Disjoint X Y)"
| "lformula0 (Eq_Presb True X Y)"

primrec sat0 :: "('fo, 'so) interp \<Rightarrow> ('fo, 'so) atomic \<Rightarrow> bool" where
  "sat0 I (Less x y) = (x |\<in>| fo_dom I \<and> y |\<in>| fo_dom I \<and> fo I x < fo I y)"
| "sat0 I (In x X) = (x |\<in>| fo_dom I \<and> fo I x |\<in>| so I X)"
| "sat0 I (Len n) = (len I = n)"
| "sat0 I (Eq_Const b x n) = (x |\<in>| fo_dom I \<and> (if b then fo I x = n else
    n < len I \<and> fo I x = len I - Suc n))"
| "sat0 I (Plus_FO x y n) = (x |\<in>| fo_dom I \<and> y |\<in>| fo_dom I \<and> fo I x = fo I y + n)"
| "sat0 I (Eq_FO x y) = (x |\<in>| fo_dom I \<and> y |\<in>| fo_dom I \<and> fo I x = fo I y)"
| "sat0 I (Eq_SO X Y) = (so I X = so I Y)"
| "sat0 I (Suc_SO cr cl X Y) =
   ((if cr then finsert (len I) else id) (so I X) =
    (if cl then finsert 0 else id) (Suc |`| so I Y))"
| "sat0 I (Empty X) = (so I X = {||})"
| "sat0 I (Singleton X) = (\<exists>x. so I X = {|x|})"
| "sat0 I (Subset X Y) = (so I X |\<subseteq>| so I Y)"
| "sat0 I (Eq_Max x X) = (x |\<in>| fo_dom I \<and> so I X \<noteq> {||} \<and> fo I x = fMax (so I X))"
| "sat0 I (Eq_Min x X) = (x |\<in>| fo_dom I \<and> so I X \<noteq> {||} \<and> fo I x = fMin (so I X))"
| "sat0 I (Eq_Union X Y Z) = (so I X = so I Y |\<union>| so I Z)"
| "sat0 I (Eq_Inter X Y Z) = (so I X = so I Y |\<inter>| so I Z)"
| "sat0 I (Eq_Diff X Y Z) = (so I X = so I Y |-| so I Z)"
| "sat0 I (Disjoint X Y) = (so I X |\<inter>| so I Y = {||})"
| "sat0 I (Eq_Presb b X n) = (setsum (\<lambda>i. 2 ^ (if b then i else len I - Suc i)) (fset (so I X)) = n)"

locale sat_gen = sat_sat0_gen sat0 fo_restrict so_restrict for fo_restrict so_restrict

primrec rev0 where
  "rev0 (Less x y) = Less y x"
| "rev0 (In x X) = In x X"
| "rev0 (Len n) = Len n"
| "rev0 (Eq_Const b x n) = Eq_Const (\<not> b) x n"
| "rev0 (Plus_FO x y n) = Plus_FO y x n"
| "rev0 (Eq_FO x y) = Eq_FO x y"
| "rev0 (Eq_SO X Y) = Eq_SO X Y"
| "rev0 (Suc_SO cr cl X Y) = Suc_SO cl cr Y X"
| "rev0 (Empty X) = Empty X"
| "rev0 (Singleton X) = Singleton X"
| "rev0 (Subset X Y) = Subset X Y"
| "rev0 (Eq_Max x X) = Eq_Min x X"
| "rev0 (Eq_Min x X) = Eq_Max x X"
| "rev0 (Eq_Union X Y Z) = Eq_Union X Y Z"
| "rev0 (Eq_Inter X Y Z) = Eq_Inter X Y Z"
| "rev0 (Eq_Diff X Y Z) = Eq_Diff X Y Z"
| "rev0 (Disjoint X Y) = Disjoint X Y"
| "rev0 (Eq_Presb b X n) = Eq_Presb (\<not> b) X n"

primrec d0 :: "('fo, 'so) atomic \<Rightarrow> ('fo, 'so) alphabet \<Rightarrow> ('fo, 'so) formula" where
  "d0 (Less x y) b = (case (x |\<in>| afo b, y |\<in>| afo b) of
    (True, True) \<Rightarrow> ff
  | (True, False) \<Rightarrow> tt
  | (False, True) \<Rightarrow> ff
  | (False, False) \<Rightarrow> a (Less x y))"
| "d0 (In x X) b = (case (x |\<in>| afo b, X |\<in>| aso b) of
    (True, True) \<Rightarrow> tt
  | (True, False) \<Rightarrow> ff
  | (False, True) \<Rightarrow> a (In x X)
  | (False, False) \<Rightarrow> a (In x X))"
| "d0 (Len n) b = (case n of 0 \<Rightarrow> ff | Suc m \<Rightarrow> a (Len m))"
| "d0 (Eq_Const l x n) b = (if l then
      case n of
        0 \<Rightarrow> if x |\<in>| afo b then tt else ff
      | Suc m \<Rightarrow> if x |\<in>| afo b then ff else a (Eq_Const l x m)
    else if x |\<in>| afo b then a (Len n) else a (Eq_Const l x n))"
| "d0 (Plus_FO x y n) b = (case (x |\<in>| afo b, y |\<in>| afo b) of
    (True, True) \<Rightarrow> if n = 0 then tt else ff
  | (False, True) \<Rightarrow> if n = 0 then ff else a (Eq_Const True x (n - 1))
  | (True, False) \<Rightarrow> ff
  | (False, False) \<Rightarrow> a (Plus_FO x y n))"
| "d0 (Eq_FO x y) b = (case (x |\<in>| afo b, y |\<in>| afo b) of
    (True, True) \<Rightarrow> tt
  | (False, False) \<Rightarrow> a (Eq_FO x y)
  | _ => ff)"
| "d0 (Eq_SO X Y) b = (if X |\<in>| aso b \<longleftrightarrow> Y |\<in>| aso b then a (Eq_SO X Y) else ff)"
| "d0 (Suc_SO cr cl X Y) b = (if cl \<longleftrightarrow> X |\<in>| aso b then a (Suc_SO cr (Y |\<in>| aso b) X Y) else ff)"
| "d0 (Empty X) b = (if X |\<in>| aso b then ff else a (Empty X))"
| "d0 (Singleton X) b = (if X |\<in>| aso b then a (Empty X) else a (Singleton X))"
| "d0 (Subset X Y) b = (if X |\<in>| aso b \<longrightarrow> Y |\<in>| aso b then a (Subset X Y) else ff)"
| "d0 (Eq_Min x X) b = (case (x |\<in>| afo b, X |\<in>| aso b) of
    (True, True) \<Rightarrow> tt
  | (False, False) \<Rightarrow> a (Eq_Min x X)
  | _ \<Rightarrow> ff)"
| "d0 (Eq_Max x X) b = (case (x |\<in>| afo b, X |\<in>| aso b) of
    (True, True) \<Rightarrow> a (Empty X)
  | (True, False) \<Rightarrow> ff
  | _ \<Rightarrow> a (Eq_Max x X))"
| "d0 (Eq_Union X Y Z) b = (if X |\<in>| aso b \<longleftrightarrow> Y |\<in>| aso b \<or> Z |\<in>| aso b then a (Eq_Union X Y Z) else ff)"
| "d0 (Eq_Inter X Y Z) b = (if X |\<in>| aso b \<longleftrightarrow> Y |\<in>| aso b \<and> Z |\<in>| aso b then a (Eq_Inter X Y Z) else ff)"
| "d0 (Eq_Diff X Y Z) b = (if X |\<in>| aso b \<longleftrightarrow> Y |\<in>| aso b \<and> Z |\<notin>| aso b then a (Eq_Diff X Y Z) else ff)"
| "d0 (Disjoint X Y) b = (if X |\<in>| aso b \<and> Y |\<in>| aso b then ff else a (Disjoint X Y))"
| "d0 (Eq_Presb l X n) b = (if l then
      if X |\<in>| aso b \<longleftrightarrow> n mod 2 \<noteq> 0
      then a (Eq_Presb l X (n div 2))
      else ff
    else
      if X |\<in>| aso b \<and> n \<noteq> 0
      then infi {|a (Eq_Presb l X (n - 2 ^ ld n)), a (Len (ld n))|}
      else if X |\<in>| aso b then ff else a (Eq_Presb l X n))"

primrec o0 :: "('fo, 'so) atomic \<Rightarrow> bool" where
  "o0 (Less x y) = False"
| "o0 (In x X) = False"
| "o0 (Len n) = (n = 0)"
| "o0 (Eq_Const l x n) = False"
| "o0 (Plus_FO x y n) = False"
| "o0 (Eq_FO x y) = False"
| "o0 (Eq_SO X Y) = True"
| "o0 (Suc_SO cl cr X Y) = (cl = cr)"
| "o0 (Empty X) = True"
| "o0 (Singleton X) = False"
| "o0 (Subset X Y) = True"
| "o0 (Eq_Min x X) = False"
| "o0 (Eq_Max x X) = False"
| "o0 (Eq_Union X Y Z) = True"
| "o0 (Eq_Inter X Y Z) = True"
| "o0 (Eq_Diff X Y Z) = True"
| "o0 (Disjoint X Y) = True"
| "o0 (Eq_Presb l X n) = (n = 0)"

global_interpretation sat: sat_gen "\<lambda>_ _. True" "\<lambda>_ _. True"
  defines sat = "sat_sat0_gen.sat_gen sat0 (\<lambda>_ _. True) (\<lambda>_ _. True)" .
global_interpretation satb: sat_gen "\<lambda>n p. p < n" "\<lambda>n P. fBall P (\<lambda>x. x < n)"
  defines satb = "sat_sat0_gen.sat_gen sat0 (\<lambda>n p. p < n) (\<lambda>n P. fBall P (\<lambda>x. x < n))" .

lemma sat0_eqI: "lformula0 x \<Longrightarrow> interp0 I = interp0 J \<Longrightarrow> sat0 I x = sat0 J x"
  by (induct x rule: lformula0.induct) (auto split: option.splits)

lemma sat0_rev0[simp]: "sat0 I (rev0 b) = sat0 (REV I) b"
proof (cases b)
  case (Plus_FO x y n) then show ?thesis
    by (auto simp: diff_Suc split: nat.splits dest: fo_len)
next
  case Eq_SO then show ?thesis by (auto dest!: iffD1[OF REV0_inj[simplified], rotated -1])
next
  case (Suc_SO cr cl X Y) show ?thesis
  proof ((cases cr; cases cl), goal_cases TT TF FT FF)
    case TT
    show ?thesis
      apply (simp only: Suc_SO TT rev0.simps sat0.simps
        if_True id_apply o_apply interp0_REV interp0.simps len_REV)
      apply (subst Suc_fimage_REV0)
       apply auto []
      apply (subst finsert_0_fimage_REV0)
       apply auto []
      apply (subst finsert_fimage_REV0)
       apply auto []
      apply (subst REV0_inj)
       apply (auto dest: so_len)
      done
  next
    case TF
    show ?thesis
      apply (simp only: Suc_SO TF rev0.simps sat0.simps
        if_True if_False id_apply o_apply interp0_REV interp0.simps len_REV)
      apply (subst Suc_fimage_REV0)
       apply auto []
      apply (subst finsert_fimage_REV0)
       apply auto []
      apply (subst REV0_inj)
       apply (auto dest: so_len)
      done
  next
    case FT
    show ?thesis
      apply (simp only: Suc_SO FT rev0.simps sat0.simps
        if_True if_False id_apply o_apply interp0_REV interp0.simps len_REV)
      apply (subst Suc_fimage_REV0)
       apply auto []
      apply (subst finsert_0_fimage_REV0)
       apply auto []
      apply (subst fimage_REV0_Suc[symmetric])
       apply auto []
      apply (subst REV0_inj)
       apply (auto dest: so_len)
      done
  next
    case FF
    show ?thesis
      apply (simp only: Suc_SO FF rev0.simps sat0.simps
        if_False id_apply o_apply interp0_REV interp0.simps)
      apply (subst Suc_fimage_REV0)
       apply auto []
      apply (subst fimage_REV0_Suc[symmetric])
       apply auto []
      apply (subst REV0_inj)
       apply (auto dest: so_len)
      done
  qed
next
  case (Singleton X)
  then show ?thesis
    apply simp
    apply (rule iffI)
     apply auto []
    apply (erule exE)
    apply (rename_tac p)
    apply (rule_tac x="len I - Suc p" in exI)
    apply (auto simp: fset_eq_iff) []
     apply (rename_tac p q)
     apply (drule_tac x="len I - Suc q" in spec)
     apply (drule iffD1)
     apply (erule fimageI)
     apply (metis Suc_diff_Suc diff_diff_cancel dual_order.strict_implies_order so_len)
    apply (drule_tac x="p" in spec)
    apply (auto simp: Suc_le_eq) []
    done
next
  case (Eq_Min x X) show ?thesis
    unfolding sat0.simps rev0.simps Eq_Min
    apply (intro iffI; elim conjE)
     apply (subst (asm) fMax_eq_iff; simp)
     apply (subst fMin_eq_iff; simp; elim conjE)
     apply (rule fBallI, frule fbspec, assumption)
     apply (rule diff_le_mono2; auto)
    apply (subst (asm) fMin_eq_iff; simp; elim conjE)
    apply (subst fMax_eq_iff; simp?; intro conjI)
    apply (erule fimageE; auto simp: Suc_le_eq dest: fo_len dest!: iffD1[OF eq_diff_iff2, rotated -1])
    apply (rule fBallI, frule fbspec, assumption)
    apply (auto simp: not_le Suc_le_eq dest: fo_len
      dest!: iffD1[OF eq_diff_iff2, rotated -1] iffD1[OF le_diff_conv2, rotated -1])
    done
next
  case (Eq_Max x X) show ?thesis
    unfolding sat0.simps rev0.simps Eq_Max
    apply (intro iffI; elim conjE)
     apply (subst (asm) fMin_eq_iff; simp)
     apply (subst fMax_eq_iff; simp; elim conjE)
     apply (rule fBallI, frule fbspec, assumption)
     apply (rule diff_le_mono2; auto)
    apply (subst (asm) fMax_eq_iff; simp; elim conjE)
    apply (subst fMin_eq_iff; simp?; intro conjI)
    apply (erule fimageE; auto simp: Suc_le_eq dest: fo_len dest!: iffD1[OF eq_diff_iff2, rotated -1])
    apply (rule fBallI, frule fbspec, assumption)
    apply (auto simp: not_le Suc_le_eq dest: fo_len
      dest!: iffD1[OF eq_diff_iff2, rotated -1] iffD1[OF le_diff_conv2, rotated -1])
    done
next
  case (Eq_Union X Y Z)
  then show ?thesis
    by (auto simp: fimage_funion[symmetric] dest!: iffD1[OF REV0_inj[simplified], rotated -1])
next
  case (Eq_Inter X Y Z)
  have "REV0 (len I) |`| so0 (interp0 I) Y |\<inter>| REV0 (len I) |`| so0 (interp0 I) Z =
    REV0 (len I) |`| (so0 (interp0 I) Y |\<inter>| so0 (interp0 I) Z)"
    by (rule inj_on_fimage_finter[OF inj_on_REV0, symmetric, where C = "so0 (interp0 I) Y |\<union>| so0 (interp0 I) Z"])
      auto
  with Eq_Inter show ?thesis by (auto simp: dest!: iffD1[OF REV0_inj[simplified], rotated -1])
next
  case (Eq_Diff X Y Z)
  have "REV0 (len I) |`| so0 (interp0 I) Y |-| REV0 (len I) |`| so0 (interp0 I) Z =
    REV0 (len I) |`| (so0 (interp0 I) Y |-| so0 (interp0 I) Z)"
    by (rule inj_on_fimage_fset_diff[OF inj_on_REV0, symmetric, where C = "so0 (interp0 I) Z |\<union>| so0 (interp0 I) Y"])
      auto
  with Eq_Diff show ?thesis by (auto simp: dest!: iffD1[OF REV0_inj[simplified], rotated -1])
next
  case (Eq_Presb b X n)
  show ?thesis
  proof (cases b)
    case True
    show ?thesis
      apply (simp only: Eq_Presb rev0.simps True simp_thms sat0.simps if_False if_True
        o_apply interp0_REV interp0.simps fimage.rep_eq)
      apply (subst setsum.reindex)
       apply (auto simp: inj_on_def fmember.rep_eq[symmetric] dest!: so_len)
      done
  next
    case False then show ?thesis
      apply (simp only: Eq_Presb rev0.simps False simp_thms sat0.simps if_False if_True
        o_apply interp0_REV interp0.simps fimage.rep_eq)
      apply (subst setsum.reindex)
       apply (auto simp: inj_on_def fmember.rep_eq[symmetric] dest!: so_len) []
      apply (rule arg_cong[of _ _ "\<lambda>x. x = n"])
      apply (rule setsum.cong[OF refl])
      apply (auto simp: fmember.rep_eq[symmetric] dest!: so_len) []
      done
  qed
qed (auto simp: Suc_le_eq
  dest: fo_len so_len dest!: iffD1[OF eq_diff_iff2, rotated -1] intro!: diff_less_mono2)

lemma d0_restrict_letter:
  "Abs_fset (FOV0 x) |\<subseteq>| FV \<Longrightarrow> Abs_fset (SOV0 x) |\<subseteq>| SV \<Longrightarrow>
  d0 x b = d0 x (restrict_letter FV SV b)"
  by (cases x)
    (auto simp: fmember.rep_eq Abs_fset_inverse Let_def ffilter_eq_fempty_iff
      split: nat.splits bool.splits)

lemma sat0_restrict_interp:
  assumes "Abs_fset (FOV0 x) |\<subseteq>| FV" "Abs_fset (SOV0 x) |\<subseteq>| SV"
  shows "sat0 I x = sat0 (restrict_interp FV SV I) x"
using assms proof (cases x)
qed (auto simp: less_eq_fset.rep_eq fmember.rep_eq Abs_fset_inverse intro: setsum.cong)

lemma finite_V:
  "finite (FOV0 x)"
  "finite (SOV0 x)"
  by (cases x; simp split: sum.splits)+

interpretation atomic: aatomic FOV0 SOV0 proof qed (simp_all add: finite_V)

lemma fov_d0: "atomic.fov (d0 x b) \<subseteq> FOV0 x - fset (afo b)"
  by (cases x) (auto simp: fmember.rep_eq Let_def split: nat.splits bool.splits)

lemma sov_d0: "atomic.sov (d0 x b) \<subseteq> SOV0 x"
  by (cases x) (auto simp: Let_def split: nat.splits bool.splits)

lemma (in sat_gen) sat_gen_d0[simp]:
  "Abs_fset (FOV0 x) |\<subseteq>| fo_dom I |\<union>| afo b \<Longrightarrow> sat_gen I (d0 x b) = sat0 (CONS b I) x"
proof (cases x)
  case (Suc_SO cr cl X Y) then show ?thesis
    apply (auto simp: o_def split: bool.splits)
    apply (smt Suc_inject fimageE fimage_finsert finsertI1 finsert_commute finsert_fminus image_iff notin_fset)+
    done
next
  case (Eq_Presb l X n)
  show ?thesis
  proof (cases l)
    case True
    show ?thesis
      apply (simp only: True Eq_Presb d0.simps if_True sat0.simps refl
        sat_gen.simps polarity.simps id_apply o_apply interp0_CONS interp0.simps)
      apply (split if_splits)+
      apply (simp only: sat_gen.simps polarity.case id_apply sat0.simps if_True
        finsert.rep_eq fimage.rep_eq o_apply fimage_fempty fBex_fempty simp_thms)
      apply (intro conjI impI)
         apply (subst setsum_pow2_insert0)
           apply simp
          apply blast
         apply (subst setsum_pow2_image_Suc)
          apply simp
         apply (metis One_nat_def even_Suc_div_two even_iff_mod_2_eq_zero
           nonzero_mult_divide_cancel_left odd_Suc_minus_one odd_two_times_div_two_nat zero_not_eq_two)
        apply (subst setsum_pow2_insert0)
          apply simp
         apply blast
        apply (subst setsum_pow2_image_Suc)
         apply simp
        apply (metis One_nat_def Suc_times_mod_eq lessI n_not_Suc_n numeral_2_eq_2)
       apply (subst setsum_pow2_image_Suc)
        apply simp
       apply fastforce
      apply (subst setsum_pow2_image_Suc)
       apply simp
      apply fastforce
      done
  next
    case False
    show ?thesis
      apply (simp only: False Eq_Presb d0.simps if_False sat0.simps refl
        sat_gen.simps polarity.simps id_apply o_apply interp0_CONS interp0.simps)
      apply (split if_splits)+
      apply (simp only: sat_gen.simps polarity.case id_apply sat0.simps if_False
        finsert.rep_eq fimage.rep_eq o_apply fimage_finsert fimage_fempty fBex_fempty
        ball_simps simp_thms len_CONS)
      using ld_bounded[of "n - 1"]
      apply (auto simp: setsum.reindex intro!: trans[OF setsum.insert] add_diff_inverse_nat)
      using setsum_pow2_REV_bounded[of "fset (so I X)" "len I"]
      apply (auto simp: setsum.reindex zero_notin_Suc_image
        dest: so_len[unfolded fmember.rep_eq o_apply])
      apply (subst Max_pow2_le[OF setsum_pow2_REV_bounded]; auto dest: so_len[unfolded fmember.rep_eq o_apply])+
      done
  qed
qed (auto simp: fmember.rep_eq less_eq_fset.rep_eq Abs_fset_inverse Let_def
    fMin_eq_iff fMin_fimage_Suc fMax_eq_iff fMax_fimage_Suc
    split: nat.splits bool.splits dest!: subset_singletonD)

lemma o0_alt: "o0 x = (\<forall>I. len I = 0 \<longrightarrow> sat0 I x)"
  by (cases x) auto

lemma rev0_rev0[simp]: "rev0 (rev0 x) = x"
  by (cases x) auto


section \<open>Functions on Formulas\<close>

primrec lformula where
  "lformula (A p x) = lformula0 x"
| "lformula (B p \<Phi>) = fBall (fimage lformula \<Phi>) id"
| "lformula (Q p V \<phi>) = lformula \<phi>"

primrec neg where
  "neg (A p x) = A (pneg p) x"
| "neg (B p \<Phi>) = B (pneg p) (neg |`| \<Phi>)"
| "neg (Q p V \<phi>) = Q (pneg p) V (neg \<phi>)"

primrec rev where
  "rev (A p x) = A p (rev0 x)"
| "rev (B p \<Phi>) = B p (rev |`| \<Phi>)"
| "rev (Q p V \<phi>) = Q p V (rev \<phi>)"

context includes fset.lifting begin

lift_definition fov :: "('fo, 'so) formula \<Rightarrow> 'fo fset" is atomic.fov by simp
lift_definition sov :: "('fo, 'so) formula \<Rightarrow> 'so fset" is atomic.sov by simp

end

definition equiv :: "('fo, 'so) formula \<Rightarrow> ('fo, 'so) formula \<Rightarrow> bool" (infix "\<cong>" 65) where
  "\<phi> \<cong> \<psi> = (\<forall>I. fov \<phi> |\<union>| fov \<psi> |\<subseteq>| fo_dom I \<longrightarrow> sat I \<phi> \<longleftrightarrow> sat I \<psi>)"

lemma sat_eqI: "lformula \<phi> \<Longrightarrow> interp0 I = interp0 J \<Longrightarrow> sat I \<phi> = sat J \<phi>"
proof (induction \<phi> arbitrary: I J)
  case A then show ?case using sat0_eqI by auto
next
  case B from B.IH[of _ I J] B.prems show ?case
    by (auto simp: fmember.rep_eq[symmetric])
next
  case (Q p V \<phi>) from Q.prems show ?case by (auto intro!: iff_allI iff_exI Q.IH)
qed

lemma (in sat_gen) sat_neg[simp]: "sat_gen I (neg \<phi>) = (\<not> sat_gen I \<phi>)"
  by (induct \<phi> arbitrary: I) (auto simp: fmember.rep_eq fBex.rep_eq fBall.rep_eq)

lemma satb_rev: "satb I (rev \<phi>) = satb (REV I) \<phi>"
proof (induction \<phi> arbitrary: I)
next
  case (B p \<Phi>)
  then show ?case by (auto simp: fBall.rep_eq fBex.rep_eq)
next
  case (Q p V \<phi>)
  show ?case apply (auto 0 1 simp add: Q.IH Let_def
   dest: spec[of _ "len I - _ - 1"] intro: exI[of _ "len I - _ - 1"])
   apply (drule_tac x = "len I - pa - 1" in spec; auto)
   apply (rule_tac x = "len I - pa - 1" in exI; auto)
   apply (rule_tac x = "len I - pa - 1" in exI; auto)
   apply (case_tac "P = {||}")
   apply simp
   apply (drule_tac x = "(\<lambda>n. len I - n - 1) |`| P" in spec; auto)
   apply (metis diff_Suc_less fbspec gr_implies_not0 linorder_neqE_nat)
   apply (subst (asm) (1 2) lift_id)
   apply (metis equalsffemptyD fMax_in fbspec)
   apply simp
   apply (case_tac "P = {||}")
   apply simp
   apply (drule_tac x = "(\<lambda>n. len I - n - 1) |`| P" in spec; auto)
   apply (metis diff_Suc_less fbspec gr_implies_not0 linorder_neqE_nat)
   apply (subst (asm) (1 2) lift_id)
   apply (metis (mono_tags, lifting) antisym_conv3 diff_Suc_less equalsffemptyD fMax_in fbspec fimageE fimage_is_fempty gr_implies_not0)
   apply simp
   apply (case_tac "P = {||}")
   apply blast
   apply (rule_tac x = "(\<lambda>n. len I - n - 1) |`| P" in exI; auto)
   apply (metis diff_Suc_less fbspec gr_implies_not0 linorder_neqE_nat)
   apply (subst (1 2) lift_id)
   apply (metis (mono_tags, lifting) antisym_conv3 diff_Suc_less equalsffemptyD fMax_in fbspec fimageE fimage_is_fempty gr_implies_not0)
   apply (metis (mono_tags, lifting) antisym_conv3 diff_Suc_less equalsffemptyD fMax_in fbspec fimageE fimage_is_fempty gr_implies_not0)
   apply simp
   apply (drule_tac x = "(\<lambda>n. len I - n - 1) |`| P" in spec; auto)
   apply (subst (asm) (1 2) lift_id)
   apply (metis (mono_tags, lifting) equalsffemptyD fMax_in fbspec)
   apply simp
   done
qed simp

primrec d :: "('fo, 'so) formula \<Rightarrow> ('fo, 'so) alphabet \<Rightarrow> ('fo, 'so) formula" where
  "d (A p x) b = case_polarity id neg p (d0 x b)"
| "d (B p \<Phi>) b = B p ((\<lambda>\<phi>. d \<phi> b) |`| \<Phi>)"
| "d (Q p V \<phi>) b = B p {|
    (case V of
      FO x \<Rightarrow> d \<phi> (afo_update (finsert x) b)
    | SO X \<Rightarrow> Q p V (d \<phi> (aso_update (finsert X) b))),
    (case V of
      FO x \<Rightarrow> Q p V (d \<phi> (afo_update (fremove x) b))
    | SO X \<Rightarrow> Q p V (d \<phi> (aso_update (fremove X) b)))|}"

lemma fov_neg[simp]: "atomic.fov (neg \<phi>) = atomic.fov \<phi>"
  by (induct \<phi>) auto

lemma sov_neg[simp]: "atomic.sov (neg \<phi>) = atomic.sov \<phi>"
  by (induct \<phi>) auto

lemma fov_simps[simp]:
  "fov (A p x) = Abs_fset (FOV0 x)"
  "fov (B p \<Phi>) = ffUnion (fov |`| \<Phi>)"
  "fov (Q p V \<phi>) = (case V of FO x \<Rightarrow> fov \<phi> |-| {|x|} | SO X \<Rightarrow> fov \<phi>)"
  by (auto simp add:
   fset_eq_iff fmember.rep_eq ffUnion.rep_eq fov.rep_eq fimage.rep_eq Abs_fset_inverse)

lemma sov_simps[simp]:
  "sov (A p x) = Abs_fset (SOV0 x)"
  "sov (B p \<Phi>) = ffUnion (sov |`| \<Phi>)"
  "sov (Q p V \<phi>) = (case V of FO x \<Rightarrow> sov \<phi> | SO X \<Rightarrow> sov \<phi> |-| {|X|})"
  by (auto simp add:
   fset_eq_iff fmember.rep_eq ffUnion.rep_eq sov.rep_eq fimage.rep_eq Abs_fset_inverse)

lemma d_restrict_letter: "fov \<phi> |\<subseteq>| FV \<Longrightarrow> sov \<phi> |\<subseteq>| SV \<Longrightarrow> d \<phi> b = d \<phi> (restrict_letter FV SV b)"
proof (induction \<phi> arbitrary: b FV SV)
  case (A p x)
  then show ?case using d0_restrict_letter[of x FV SV b] by simp
next
  case (B p \<Phi>)
  from B.IH[of _ FV SV b] B.prems show ?case
   by (auto simp: fmember.rep_eq fBall.rep_eq image_iff ffUnion_subset_iff)
next
  case (Q p V \<phi>)
  show ?case unfolding d.simps aformula.inject simp_thms
  proof (goal_cases Q)
   case Q
   let ?FV = "case V of FO x \<Rightarrow> finsert x FV | SO X \<Rightarrow> FV"
   let ?SV = "case V of FO x \<Rightarrow> SV | SO X \<Rightarrow> finsert X SV"
   note Q_IH = Q.IH[symmetric, of ?FV ?SV]
   show ?case (is "{|?\<phi>, ?\<psi>|} = {|?\<phi>', ?\<psi>'|}")
   proof -
     from Q.prems have "?\<phi> = ?\<phi>'"
       by (cases V) (auto intro!: box_equals[OF arg_cong[of _ _ "d _"] Q_IH Q_IH])
     moreover from Q.prems have "?\<psi> = ?\<psi>'"
       by (cases V) (auto intro!: box_equals[OF arg_cong[of _ _ "d _"] Q_IH Q_IH])
     ultimately show ?case by simp
   qed
  qed
qed

lemma (in sat_gen) sat_gen_restrict_interp:
  "fov \<phi> |\<subseteq>| FV \<Longrightarrow> sov \<phi> |\<subseteq>| SV \<Longrightarrow> sat_gen I \<phi> = sat_gen (restrict_interp FV SV I) \<phi>"
proof (induction \<phi> arbitrary: I FV SV)
  case (A p x)
  then show ?case using sat0_restrict_interp[of x FV SV I] by simp
next
  case (B p \<Phi>)
  from B.IH[of _ FV SV I] B.prems show ?case
   by (auto simp: fmember.rep_eq fBall.rep_eq fBex.rep_eq image_iff ffUnion_subset_iff)
next
  case (Q p V \<phi>)
  let ?FV = "case V of FO x \<Rightarrow> finsert x FV | SO X \<Rightarrow> FV"
  let ?SV = "case V of FO x \<Rightarrow> SV | SO X \<Rightarrow> finsert X SV"
  from Q.IH[symmetric, of ?FV ?SV] Q.prems show ?case by (auto simp: fminus_single_finsert)
qed

lemma fov_d_aux: "atomic.fov (d \<phi> b) \<subseteq> atomic.fov \<phi> - fset (afo b)"
proof (induct \<phi> arbitrary: b)
  case A then show ?case by (auto intro!: fov_d0)
qed fastforce+

lemma fov_d: "fov (d \<phi> b) |\<subseteq>| fov \<phi> - afo b"
  unfolding fov.abs_eq less_eq_fset.rep_eq by (auto simp: Abs_fset_inverse fov_d_aux)

lemma sov_d_aux: "atomic.sov (d \<phi> b) \<subseteq> atomic.sov \<phi>"
proof (induct \<phi> arbitrary: b)
  case A then show ?case by (auto intro!: sov_d0)
qed fastforce+

lemma sov_d: "sov (d \<phi> b) |\<subseteq>| sov \<phi>"
  unfolding sov.abs_eq less_eq_fset.rep_eq by (auto simp: Abs_fset_inverse sov_d_aux) 

lemma sat_CONS_fo_update[simp]:
  "lformula \<phi> \<Longrightarrow> sat (CONS (afo_update (finsert x) b) (fo_update x p I)) \<phi> =
  sat (CONS (afo_update (finsert x) b) I) \<phi>"
  by (rule sat_eqI) auto

lemma sat_d:
  "fov \<phi> |\<subseteq>| fo_dom I |\<union>| afo b \<Longrightarrow> sat I (d \<phi> b) = sat (CONS b I) \<phi>"
proof (induction \<phi> arbitrary: b I)
  case (B p \<Phi>)
  from B.IH[of _ I b] B.prems show ?case
    by (auto simp: fmember.rep_eq fBall.rep_eq fBex.rep_eq ffUnion_subset_iff)
next
  case (Q p V \<phi>)
  from Q.prems show ?case
     apply (auto simp: fremove_subset_iff gr0_conv_Suc Q.IH split: if_splits nat.splits)
     apply (subst (asm) Q.IH; auto)
     apply (subst Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst Q.IH; auto)
     apply (subst Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst (asm) Q.IH; auto)
     apply (subst Q.IH; auto)
     apply (auto simp: so_update_CONS) []

     apply (drule_tac x = "finsert 0 (Suc |`| P)" in spec)
     apply (auto simp: so_update_CONS) []
     apply (erule iffD1[OF arg_cong[of _ _ "\<lambda>P. sat (CONS _ (so_update _ P _)) _"], rotated])
     apply (auto simp: fimage_iff) []

     apply (drule_tac x = "fremove 0 (Suc |`| P)" in spec)
     apply (auto simp: so_update_CONS fimage_iff) []
     apply (erule iffD1[OF arg_cong[of _ _ "\<lambda>P. sat (CONS _ (so_update _ P _)) _"], rotated])
     apply (auto simp: fimage_iff) []

     apply (rule_tac x = "finsert 0 (Suc |`| P)" in exI)
     apply (auto simp: so_update_CONS) []
     apply (erule iffD1[OF arg_cong[of _ _ "\<lambda>P. sat (CONS _ (so_update _ P _)) _"], rotated])
     apply (auto simp: fimage_iff) []

     apply (rule_tac x = "fremove 0 (Suc |`| P)" in exI)
     apply (auto simp: so_update_CONS) []
     apply (erule iffD1[OF arg_cong[of _ _ "\<lambda>P. sat (CONS _ (so_update _ P _)) _"], rotated])
     apply (auto simp: fimage_iff) []

     apply (auto simp: so_update_CONS split: if_splits) []
     done
qed simp

lemma satb_d:
  "fov \<phi> |\<subseteq>| fo_dom I |\<union>| afo b \<Longrightarrow> satb I (d \<phi> b) = satb (CONS b I) \<phi>"
proof (induction \<phi> arbitrary: b I)
  case (B p \<Phi>)
  from B.IH[of _ I b] B.prems show ?case
    by (auto simp: fmember.rep_eq fBall.rep_eq fBex.rep_eq ffUnion_subset_iff)
next
  case (Q p V \<phi>)
  from Q.prems show ?case
    apply (auto simp: fremove_subset_iff gr0_conv_Suc Q.IH split: if_splits nat.splits)
    apply (subst (asm) Q.IH; auto)
    apply (subst Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst Q.IH; auto)
    apply (subst Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst (asm) Q.IH; auto)
    apply (subst Q.IH; auto)
    apply (auto simp: so_update_CONS) []
    apply (drule spec; erule mp; auto simp add: pred_alt split: nat.splits)
    apply (drule spec; erule mp; auto simp add: pred_alt split: nat.splits)
    
    apply (drule_tac x = "finsert 0 (Suc |`| P)" in spec)
    apply (auto simp: so_update_CONS o_def) []
    
    apply (drule_tac x = "fremove 0 (Suc |`| P)" in spec)
    apply (auto simp: so_update_CONS o_def split: if_splits) []
    
    apply (rule_tac x = "finsert 0 (Suc |`| P)" in exI)
    apply (auto simp: so_update_CONS o_def) []
    
    apply (rule_tac x = "fremove 0 (Suc |`| P)" in exI)
    apply (auto simp: so_update_CONS o_def) []
    
    apply (auto simp: so_update_CONS split: if_splits) []
    apply (rule_tac x = "(\<lambda>x. x - 1) |`| fremove 0 P" in exI)
    apply (auto simp add: pred_alt split: nat.splits)
    apply (drule_tac x = "(\<lambda>x. x - 1) |`| P" in spec)
    apply (auto simp add: pred_alt dest!: fbspec less_imp_Suc_add split: nat.splits)
    done
qed simp

primrec ob :: "('fo, 'so) formula \<Rightarrow> bool" where
  "ob (A p x) = case_polarity id HOL.Not p (o0 x)"
| "ob (B p \<Phi>) = case_polarity fBall fBex p (ob |`| \<Phi>) id"
| "ob (Q p V \<phi>) = case_var (\<lambda>_. case_polarity True False p) (\<lambda>_. ob \<phi>) V"

lemma ob_alt: "ob \<phi> = satb empty_interp \<phi>"
  by (induct \<phi>) (auto simp: o0_alt fmember.rep_eq[symmetric])


section \<open>Coinductive Semantics\<close>

primcorec L where
  "L FV SV \<I> = Lang (\<exists>I. len I = 0 \<and> I \<in> \<I>)
     (\<lambda>a. if afo a |\<subseteq>| FV \<and> aso a |\<subseteq>| SV then L FV SV {\<BB>. CONS a \<BB> \<in> \<I>} else Zero)"

lemma in_language_Zero[simp]: "\<not> in_language Zero w"
  by (induct w) auto

lemma L_empty: "L FV SV {} = Zero"
  by coinduction auto

lemma in_language_L_afo_aso:
  "in_language (L FV SV I) w \<Longrightarrow> x \<in> set w \<Longrightarrow> afo x |\<subseteq>| FV \<and> aso x |\<subseteq>| SV"
  by (induct w arbitrary: x I) (auto split: if_splits)

lemma to_language_empty[simp]: "to_language {} = Zero"
  by coinduction auto

lemma Zero_eq_to_language_iff[simp]: "Zero = to_language X \<longleftrightarrow> X = {}"
  by (subst to_language_in_language[of Zero, symmetric],
    unfold inj_eq[OF bij_is_inj[OF to_language_bij]]) auto

lemma L_alt: "L FV SV \<I> =
  to_language {xs. \<exists>I \<in> \<I>. \<exists>J. I = fold CONS (List.rev xs) J \<and> len J = 0 \<and>
    (\<forall>x \<in> set xs. afo x |\<subseteq>| FV \<and> aso x |\<subseteq>| SV)}"
  by (coinduction arbitrary: \<I>) (auto simp: L_empty dest!: spec[of _ "{}"])

lemma in_language_L_alphabet[simp]:
  "in_language (L FV SV V) w \<Longrightarrow> x \<in> set w \<Longrightarrow> x \<in> set (mk_alphabet FV SV)"
  unfolding set_mk_alphabet image_iff
  by (drule (1) in_language_L_afo_aso)
    (auto simp: fset_inverse less_eq_fset.rep_eq
      intro!: bexI[of _ "fset (afo x)"] bexI[of _ "fset (aso x)"])

definition "lang FV SV V \<phi> = L FV SV {I. sat I \<phi> \<and> V |\<subseteq>| fo_dom I}"
definition "langb FV SV V \<phi> = L FV SV {I. satb I \<phi> \<and> V |\<subseteq>| fo_dom I}"


section \<open>M2L Procedure\<close>

interpretation M2L: DA "mk_alphabet FV SV"
  "\<lambda>\<phi>. (FV, \<phi>)" "\<lambda>b (V, \<phi>). (V |-| afo b, d \<phi> b)" "\<lambda>(V, \<phi>). V = {||} \<and> ob \<phi>"
  "\<lambda>(V, \<phi>). fov \<phi> |\<subseteq>| V \<and> V |\<subseteq>| FV \<and> sov \<phi> |\<subseteq>| SV" "\<lambda>(V, \<phi>). langb FV SV V \<phi>"
  "\<lambda>\<phi>. fov \<phi> |\<subseteq>| FV \<and> sov \<phi> |\<subseteq>| SV" "langb FV SV FV"
  by unfold_locales
    (auto 0 3 simp: langb_def satb_d ob_alt fminus_fsubset_conv ac_simps set_mk_alphabet
        fmember.rep_eq Abs_fset_inverse finite_subset
      dest: in_language_L_alphabet
      intro!: arg_cong[of _ _ "L FV SV"] dest: fset_mp[OF fov_d] fset_mp[OF sov_d])

lemma M2L_check_eqv_soundness:
  "\<lbrakk>fov \<phi> |\<union>| fov \<psi> |\<subseteq>| fo_dom I; M2L.check_eqv (fov \<phi> |\<union>| fov \<psi>) (sov \<phi> |\<union>| sov \<psi>) \<phi> \<psi>\<rbrakk> \<Longrightarrow>
   satb I \<phi> \<longleftrightarrow> satb I \<psi>"
  apply (rule box_equals[OF _
    satb.sat_gen_restrict_interp[symmetric, of \<phi> "fov \<phi> |\<union>| fov \<psi>" "sov \<phi> |\<union>| sov \<psi>" I]
    satb.sat_gen_restrict_interp[symmetric, of \<psi> "fov \<phi> |\<union>| fov \<psi>" "sov \<phi> |\<union>| sov \<psi>" I]])
  unfolding le_fo_dom_restrict[of "fov \<phi> |\<union>| fov \<psi>" I "sov \<phi> |\<union>| sov \<psi>"]
  using ex_fold_CONS[of "fov \<phi> |\<union>| fov \<psi>" "sov \<phi> |\<union>| sov \<psi>" I]
  apply (auto simp: langb_def L_alt set_eq_iff inj_eq[OF bij_is_inj[OF to_language_bij]]
    simp del: interp0_restrict_interp
    dest!: M2L.soundness[unfolded rel_language_eq])
  apply (drule_tac x="List.rev xs" in spec)
  apply auto
  apply (drule_tac x="List.rev xs" in spec)
  apply auto
  done

section \<open>Emptiness Check\<close>

lemma FOV0_rev0[simp]: "FOV0 (rev0 x) = FOV0 x"
  by (cases x) auto

lemma rev_rev[simp]: "rev (rev \<phi>) = \<phi>"
  by (induct \<phi>) (auto simp: fmember.rep_eq image_iff)

lemma fold_rev_telescope: "fold (\<lambda>a b. rev (f a (rev b))) w (rev \<phi>) = rev (fold f w \<phi>)"
  by (induct w arbitrary: \<phi>) auto

lemma fov_rev[simp]: "fov (rev \<phi>) = fov \<phi>"
  by (induct \<phi>) (auto simp: fmember.rep_eq ffUnion.rep_eq)

definition "zero = \<lparr>afo = {||}, aso = {||}\<rparr>"
definition "SNOC b I = REV (CONS b (REV I))"

lemma afo_zero[simp]: "afo zero = {||}"
  unfolding zero_def by simp

lemma fo_dom_SNOCs:
  "fo_dom ((SNOC b ^^ k) I) = (if k = 0 then fo_dom I else fo_dom I |\<union>| afo b)"
  by (induct k) (auto simp: SNOC_def)

locale norm =
  fixes norm :: "('fo :: linorder, 'so :: linorder) formula \<Rightarrow> ('fo, 'so) formula"
  assumes sat_norm: "sat I (norm \<phi>) = sat I \<phi>"
  and satb_norm: "satb I (norm \<phi>) = satb I \<phi>"
  and norm_rev: "norm (rev \<phi>) = rev (norm \<phi>)"
  and fov_norm: "fov (norm \<phi>) |\<subseteq>| fov \<phi>"
  and sov_norm: "sov (norm \<phi>) |\<subseteq>| sov \<phi>"
  and lformula_norm: "lformula \<phi> \<Longrightarrow> lformula (norm \<phi>)"
  and finite_norm: "finite {fold (\<lambda>a \<phi>. norm (d \<phi> a)) w \<phi> | w. True}"
begin

context
  fixes p :: polarity
  and \<psi> :: "('fo, 'so) formula"
begin

definition "rd b \<phi> = norm (rev (d (rev \<phi>) b))"

lemma fold_rd: "fold rd w (norm \<phi>) = rev (fold (\<lambda>a \<phi>. norm (d \<phi> a)) w (norm (rev \<phi>)))"
proof (induction w arbitrary: \<phi>)
  case (Cons x xs)
  then show ?case
    unfolding fold.simps o_apply rd_def Cons.IH norm_rev
    by (intro fold_rev_telescope)
qed (simp add: norm_rev)

lemma finite_rd: "finite {fold rd w (norm \<phi>) | w. True}"
  by (subst fold_rd, rule finite_surj[OF finite_norm, where f = rev]) auto
lemma finite_rd_replicate: "finite {fold rd (replicate k zero) (norm \<phi>) | k. True}"
  by (rule finite_subset[OF _ finite_rd]) (auto intro: exI[of _ "replicate _ zero"])

definition "fut = B p (Abs_fset {fold rd (replicate k zero) (norm \<psi>) | k. True})"

lemma satb_rd[simplified]:
  assumes "fov (rev \<phi>) |\<subseteq>| fo_dom (REV I) |\<union>| afo b"
  shows "satb I (rd b \<phi>) \<longleftrightarrow> satb (SNOC b I) \<phi>"
  unfolding rd_def SNOC_def satb_norm satb_rev satb_d[OF assms] ..

lemma fov_rd: "fov (rd b \<phi>) |\<subseteq>| fov \<phi> - afo b"
  unfolding rd_def using fov_norm fov_d by fastforce

lemma fov_rd_zeros: "fov ((rd zero^^k) \<phi>) |\<subseteq>| fov \<phi>"
  by (induct k arbitrary: \<phi>) (auto dest: fset_mp[OF fov_rd])

lemma satb_fold_rd:
  "fov \<phi> |\<subseteq>| fo_dom I |\<union>| afo b \<Longrightarrow>
   satb I (fold rd (replicate k b) \<phi>) \<longleftrightarrow> satb ((SNOC b^^k) I) \<phi>"
proof (induction k arbitrary: I \<phi>)
  case (Suc k)
  show ?case
    unfolding fold.simps o_apply replicate.simps
    apply (subst Suc.IH)
    using fsubset_trans fov_rd Suc.prems apply fast
    apply (subst satb_rd)
     apply (auto simp add: fo_dom_SNOCs[simplified] dest: fset_mp[OF Suc.prems])
    done
qed simp

lemma satb_fut:
  assumes "fov \<psi> |\<subseteq>| fo_dom I"
  shows "satb I fut \<longleftrightarrow>
    (case p of
      Pos \<Rightarrow> (\<forall>k. satb ((SNOC zero^^k) I) \<psi>)
    | Neg \<Rightarrow> (\<exists>k. satb ((SNOC zero^^k) I) \<psi>))"
  using satb_fold_rd[OF fsubset_trans[OF fov_norm], OF fsubset_trans[OF assms], of I zero]
    finite_rd_replicate[of \<psi>]
  unfolding fut_def
  by (auto simp: fBall.rep_eq fBex.rep_eq fmember.rep_eq Abs_fset_inverse satb_norm)

lemma fov_fut: "fov fut |\<subseteq>| fov \<psi>"
  unfolding fut_def using finite_rd_replicate[of \<psi>]
  by (auto simp: fmember.rep_eq ffUnion.rep_eq Abs_fset_inverse
    dest!: fset_mp[OF fov_rd_zeros, unfolded fmember.rep_eq]
           fset_mp[OF fov_norm, unfolded fmember.rep_eq])

end

primrec finalize :: "('fo, 'so) formula \<Rightarrow> ('fo, 'so) formula" where
  "finalize (A p x) = A p x"
| "finalize (B p \<Phi>) = B p (finalize |`| \<Phi>)"
| "finalize (Q p V \<phi>) = fut p (Q p V (finalize \<phi>))"

definition ox :: "('fo, 'so) formula \<Rightarrow> bool" where
  "ox \<phi> = ob (finalize \<phi>)"

lemma len_SNOCs: "len ((SNOC x ^^ i) I) = len I + i"
  by (induct i arbitrary: I) (auto simp: SNOC_def)

lemma interp0_SNOC_zero:
  "interp0 (SNOC zero I) = interp0 I"
  unfolding zero_def SNOC_def
  by transfer (force simp: o_def fun_eq_iff intro!: interp0.equality)

lemma interp0_SNOCs_zero[simp]:
  "interp0 ((SNOC zero ^^ i) I) = interp0 I"
  by (induct i arbitrary: I) (auto simp: interp0_SNOC_zero)

lemma fov_finalize: "fov (finalize \<phi>) |\<subseteq>| fov \<phi>"
  by (induct \<phi>)
    (auto simp: fmember.rep_eq ffUnion.rep_eq fsubset_iff
      dest!: fset_mp[OF fov_fut, unfolded fmember.rep_eq])

lemma sat_fo_update_SNOCs_zero[simp]:
  assumes "lformula \<phi>"
  shows "sat (fo_update x p ((SNOC zero ^^ k) I)) \<phi> = sat (fo_update x p I) \<phi>"
  by (rule sat_eqI[OF assms]) auto

lemma sat_so_update_SNOCs_zero[simp]:
  assumes "lformula \<phi>"
  shows "sat (so_update X P ((SNOC zero ^^ k) I)) \<phi> = sat (so_update X P I) \<phi>"
  by (rule sat_eqI[OF assms]) auto

lemma satb_finalize:
  "fov \<phi> |\<subseteq>| fo_dom I \<Longrightarrow> lformula \<phi> \<Longrightarrow> satb I (finalize \<phi>) \<longleftrightarrow> sat I \<phi>"
proof (induction \<phi> arbitrary: I)
  case (Q p V \<phi>)
  from Q.prems show ?case
    unfolding finalize.simps
    apply (subst satb_fut)
    apply (auto dest!: fset_mp[OF fov_finalize]) []
    apply (auto simp add: satb_fut len_SNOCs)
    apply (subst (asm) Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    subgoal for x q by (drule spec2[where x = "Suc q"]; erule mp; simp)
    subgoal for x q by (drule spec2[where x = "Suc q"]; erule mp; simp)
    apply (subst Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    apply (subst (asm) Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    apply (subst Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    subgoal for x q by (rule exI[where x = "Suc q"]; rule exI[where x = q]; simp)
    subgoal for x q by (rule exI[where x = "Suc q"]; rule exI[where x = q]; simp)
    apply (subst (asm) Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    subgoal for X Q
      by (drule spec2[where x = "if Q = {||} then 0 else Suc (fMax Q)"]; erule mp; auto dest: fMax_ge)
    apply (subst Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    apply (subst (asm) Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    apply (subst Q.IH; auto simp add: fremove_subset_iff split: if_splits) []
    subgoal for X Q
      by (rule exI[where x = "if Q = {||} then 0 else Suc (fMax Q)"]; rule exI[where x = Q]; auto dest: fMax_ge)
    done
qed (auto simp: fBall.rep_eq fBex.rep_eq ffUnion_subset_iff)

lemma ox_alt: "lformula \<phi> \<Longrightarrow> fov \<phi> = {||} \<Longrightarrow> ox \<phi> = sat empty_interp \<phi>"
  unfolding ox_def o_apply ob_alt
  by (subst satb_finalize) auto

lemma lformula_neg: "lformula \<phi> \<Longrightarrow> lformula (neg \<phi>)"
  by (induct \<phi>) (auto simp: fBall.rep_eq)

lemma lformula_d0: "lformula0 x \<Longrightarrow> lformula (d0 x b)"
  by (induct x rule: lformula0.induct)
    (auto simp: Let_def intro: lformula0.intros split: nat.splits bool.splits)

lemma lformula_d: "lformula \<phi> \<Longrightarrow> lformula (d \<phi> b)"
  by (induct \<phi> arbitrary: b) (auto intro: lformula_d0 lformula_neg simp: fBall.rep_eq)

lemma fset_fold_d: 
  "fset (fst (fold (\<lambda>b (V, \<phi>). (V |-| afo b, norm (d \<phi> b))) w (V, \<phi>))) \<subseteq> fset V"
  by (induct w arbitrary: V \<phi>) force+


section \<open>WS1S Procedure\<close>

interpretation WS1S: DFA "mk_alphabet FV SV"
  "\<lambda>\<phi>. (FV, norm \<phi>)" "\<lambda>b (V, \<phi>). (V |-| afo b, norm (d \<phi> b))"
  "\<lambda>(V, \<phi>). V = {||} \<and> ox \<phi>"
  "\<lambda>(V, \<phi>). fov \<phi> |\<subseteq>| V \<and> V |\<subseteq>| FV \<and> sov \<phi> |\<subseteq>| SV \<and> lformula \<phi>" "\<lambda>(V, \<phi>). lang FV SV V \<phi>"
  "\<lambda>\<phi>. fov \<phi> |\<subseteq>| FV \<and> sov \<phi> |\<subseteq>| SV \<and> lformula \<phi>" "lang FV SV FV"
proof (unfold_locales; clarify?)
  fix V \<phi>
  have *: "fset (fst (fold (\<lambda>b (V, \<phi>). (V |-| afo b, norm (d \<phi> b))) w (V, \<phi>))) \<subseteq> fset V"
    (is "fset (fst (?P w)) \<subseteq> _") for w
    by (induct w arbitrary: V \<phi>) force+
  have [simp]: "snd (?P w) = fold (\<lambda>b \<phi>. norm (d \<phi> b)) w \<phi>" for w
    by (induct w arbitrary: V \<phi>) auto
  show "finite {?P w | w. w \<in> lists (set (mk_alphabet FV SV))}"
  proof (intro finite_surj[where f = "\<lambda>(V, \<phi>). (Abs_fset V, \<phi>)", OF finite_cartesian_product[OF
      iffD2[OF finite_Pow_iff finite_fset[of V]] finite_norm[of \<phi>]]], safe, drule sym)
    fix V' \<phi>' w
    assume "?P w = (V', \<phi>')"
    moreover with *[of w] have "fset V' \<subseteq> fset V" by auto
    ultimately show "(V', \<phi>') \<in> (\<lambda>(V, y). (Abs_fset V, y)) `
      (Pow (fset V) \<times> {fold (\<lambda>a \<phi>. norm (d \<phi> a)) w \<phi> |w. True})"
      by (intro image_eqI[of _ _ "(fset V', \<phi>')"])
        (auto simp add: fset_inverse intro!: exI[of _ w] dest: arg_cong[of _ _ snd])
  qed
qed (auto 0 3 simp: lang_def sat_d ox_alt fminus_fsubset_conv ac_simps set_mk_alphabet
        fmember.rep_eq Abs_fset_inverse finite_subset sat_norm
      dest: in_language_L_alphabet
      dest!:
        fset_mp[OF fov_norm, unfolded fmember.rep_eq]
        fset_mp[OF sov_norm, unfolded fmember.rep_eq]
        fset_mp[OF fov_d, unfolded fmember.rep_eq]
        fset_mp[OF sov_d, unfolded fmember.rep_eq]
      intro!: arg_cong[of _ _ "L FV SV"] lformula_norm lformula_d)

definition "WS1S_check_eqv = WS1S.check_eqv"

lemma WS1S_check_eqv_soundness:
  "\<lbrakk>fov \<phi> |\<union>| fov \<psi> |\<subseteq>| fo_dom I; WS1S_check_eqv (fov \<phi> |\<union>| fov \<psi>) (sov \<phi> |\<union>| sov \<psi>) \<phi> \<psi>\<rbrakk> \<Longrightarrow>
   sat I \<phi> \<longleftrightarrow> sat I \<psi>"
  unfolding WS1S_check_eqv_def
  apply (rule box_equals[OF _
    sat.sat_gen_restrict_interp[symmetric, of \<phi> "fov \<phi> |\<union>| fov \<psi>" "sov \<phi> |\<union>| sov \<psi>" I]
    sat.sat_gen_restrict_interp[symmetric, of \<psi> "fov \<phi> |\<union>| fov \<psi>" "sov \<phi> |\<union>| sov \<psi>" I]])
  unfolding le_fo_dom_restrict[of "fov \<phi> |\<union>| fov \<psi>" I "sov \<phi> |\<union>| sov \<psi>"]
  using ex_fold_CONS[of "fov \<phi> |\<union>| fov \<psi>" "sov \<phi> |\<union>| sov \<psi>" I]
  apply (auto simp: lang_def L_alt set_eq_iff inj_eq[OF bij_is_inj[OF to_language_bij]]
    simp del: interp0_restrict_interp
    dest!: WS1S.soundness[unfolded rel_language_eq])
  apply (drule_tac x="List.rev xs" in spec)
  apply auto
  apply (drule_tac x="List.rev xs" in spec)
  apply auto
  done

lemma WS1S_check_eqv_completeness:
  "\<lbrakk>lformula \<phi>; lformula \<psi>; \<phi> \<cong> \<psi>\<rbrakk> \<Longrightarrow>
   WS1S_check_eqv (fov \<phi> |\<union>| fov \<psi>) (sov \<phi> |\<union>| sov \<psi>) \<phi> \<psi>"
  unfolding WS1S_check_eqv_def equiv_def
  apply (rule WS1S.completeness[unfolded rel_language_eq]; simp)
  apply (auto simp add: lang_def L_alt inj_eq[OF bij_is_inj[OF to_language_bij]] set_eq_iff)
  done

end

section \<open>User-Relevant Formulas\<close>

datatype (xFOV0: 'fo, xSOV0: 'so) xatomic =
  xLess 'fo 'fo
| xIn 'fo 'so
| xEq_Const 'fo nat
| xPlus_FO 'fo 'fo nat
| xEq_FO 'fo 'fo
| xEq_SO 'so 'so
| xSuc_SO 'so 'so
| xEmpty 'so
| xSingleton 'so
| xSubset 'so 'so
| xEq_Max 'fo 'so
| xEq_Min 'fo 'so
| xEq_Union 'so 'so 'so
| xEq_Inter 'so 'so 'so
| xEq_Diff 'so 'so 'so
| xDisjoint 'so 'so
| xEq_Presb 'so nat

record ('fo, 'so) xinterp =
  xfo :: "'fo \<Rightarrow> nat"
  xso :: "'so \<Rightarrow> nat fset"

primrec xsat0 :: "('fo, 'so) xinterp \<Rightarrow> ('fo, 'so) xatomic \<Rightarrow> bool" where
  "xsat0 I (xLess x y) = (xfo I x < xfo I y)"
| "xsat0 I (xIn x X) = (xfo I x |\<in>| xso I X)"
| "xsat0 I (xEq_Const x n) = (xfo I x = n)"
| "xsat0 I (xPlus_FO x y n) = (xfo I x = xfo I y + n)"
| "xsat0 I (xEq_FO x y) = (xfo I x = xfo I y)"
| "xsat0 I (xEq_SO X Y) = (xso I X = xso I Y)"
| "xsat0 I (xSuc_SO X Y) = (xso I X = Suc |`| xso I Y)"
| "xsat0 I (xEmpty X) = (xso I X = {||})"
| "xsat0 I (xSingleton X) = (\<exists>x. xso I X = {|x|})"
| "xsat0 I (xSubset X Y) = (xso I X |\<subseteq>| xso I Y)"
| "xsat0 I (xEq_Max x X) = (xso I X \<noteq> {||} \<and> xfo I x = fMax (xso I X))"
| "xsat0 I (xEq_Min x X) = (xso I X \<noteq> {||} \<and> xfo I x = fMin (xso I X))"
| "xsat0 I (xEq_Union X Y Z) = (xso I X = xso I Y |\<union>| xso I Z)"
| "xsat0 I (xEq_Inter X Y Z) = (xso I X = xso I Y |\<inter>| xso I Z)"
| "xsat0 I (xEq_Diff X Y Z) = (xso I X = xso I Y |-| xso I Z)"
| "xsat0 I (xDisjoint X Y) = (xso I X |\<inter>| xso I Y = {||})"
| "xsat0 I (xEq_Presb X n) = (setsum (\<lambda>i. 2 ^ i) (fset (xso I X)) = n)"

primrec trans0 where
  "trans0 (xLess x y) = Less x y"
| "trans0 (xIn x X) = In x X"
| "trans0 (xEq_Const x n) = Eq_Const True x n"
| "trans0 (xPlus_FO x y n) = Plus_FO x y n"
| "trans0 (xEq_FO x y) = Eq_FO x y"
| "trans0 (xEq_SO X Y) = Eq_SO X Y"
| "trans0 (xSuc_SO X Y) = Suc_SO False False X Y"
| "trans0 (xEmpty X) = Empty X"
| "trans0 (xSingleton X) = Singleton X"
| "trans0 (xSubset X Y) = Subset X Y"
| "trans0 (xEq_Max x X) = Eq_Max x X"
| "trans0 (xEq_Min x X) = Eq_Min x X"
| "trans0 (xEq_Union X Y Z) = Eq_Union X Y Z"
| "trans0 (xEq_Inter X Y Z) = Eq_Inter X Y Z"
| "trans0 (xEq_Diff X Y Z) = Eq_Diff X Y Z"
| "trans0 (xDisjoint X Y) = Disjoint X Y"
| "trans0 (xEq_Presb X n) = Eq_Presb True X n"

type_synonym ('fo, 'so) xformula = "(('fo, 'so) xatomic, 'fo, 'so) aformula"

primrec xsat :: "('fo, 'so) xinterp \<Rightarrow> ('fo, 'so) xformula \<Rightarrow> bool" where
  "xsat I (A p x) = case_polarity id HOL.Not p (xsat0 I x)"
| "xsat I (B p \<Phi>) = case_polarity fBall fBex p (xsat I |`| \<Phi>) id"
| "xsat I (Q p V \<phi>) = (case (p, V) of
   (Pos, FO x) \<Rightarrow> \<forall>p. xsat (xfo_update (\<lambda>f. f(x := p)) I) \<phi>
  | (Pos, SO X) \<Rightarrow> \<forall>P. xsat (xso_update (\<lambda>f. f(X := P)) I) \<phi>
  | (Neg, FO x) \<Rightarrow> \<exists>p. xsat (xfo_update (\<lambda>f. f(x := p)) I) \<phi>
  | (Neg, SO X) \<Rightarrow> \<exists>P. xsat (xso_update (\<lambda>f. f(X := P)) I) \<phi>)"

lemma finite_xV:
  "finite (xFOV0 x)"
  "finite (xSOV0 x)"
  by (cases x; simp)+

primrec trans where
  "trans (A p x) = A p (trans0 x)"
| "trans (B p \<Phi>) = B p (trans |`| \<Phi>)"
| "trans (Q p V \<phi>) = Q p V (trans \<phi>)"

interpretation xatomic: aatomic xFOV0 xSOV0 proof qed (simp_all add: finite_xV)

context includes fset.lifting begin

lift_definition xfov :: "('fo, 'so) xformula \<Rightarrow> 'fo fset" is xatomic.fov by simp
lift_definition xsov :: "('fo, 'so) xformula \<Rightarrow> 'so fset" is xatomic.sov by simp

end

lemma FOV0_trans0[simp]: "FOV0 (trans0 x) = xFOV0 x"
  by (cases x) auto

lemma fov_trans[simp]: "atomic.fov (trans \<phi>) = xatomic.fov \<phi>"
  by (induct \<phi>) auto

context begin

private definition bound where
  "bound X = (if X = {||} then 0 else Suc (fMax X))"

private lemma bound_gt: "x |\<in>| X \<Longrightarrow> x < bound X"
  unfolding bound_def using fMax_ge by fastforce

private lemma bound_empty[simp]: "bound {||} = 0"
  unfolding bound_def by simp

private lemma bound_finsert[simp]: "bound (finsert x X) = max (Suc x) (bound X)"
  unfolding bound_def by auto

private lemma bound_fimage_fun_upd[simp]: "bound (f(x := n) |`| V) =
  (if x |\<in>| V then
   if V |-| {|x|} = {||}
   then Suc n
   else max (Suc n) (bound (f |`| (V |-| {|x|})))
  else bound (f |`| V))"
  unfolding bound_def by (auto simp: fMax_fimage_fun_upd)

lift_definition transi :: "'fo fset \<Rightarrow> 'so fset \<Rightarrow> ('fo, 'so) xinterp \<Rightarrow> ('fo, 'so) interp" is
  "\<lambda>FV SV I. (\<lparr>fo0_dom = FV, fo0 = restrict1 FV (xfo I), so0 = restrict2 SV (xso I)\<rparr>,
    max (bound (xfo I |`| FV)) (bound (ffUnion (xso I |`| SV))))"
  by (auto simp: less_max_iff_disj notin_fset[symmetric] ffUnion_iff intro!: bound_gt)

private lemma interp0_transi[simp]:
  "interp0 (transi FV SV I) = \<lparr>fo0_dom = FV, fo0 = restrict1 FV (xfo I), so0 = restrict2 SV (xso I)\<rparr>"
  by transfer simp

lemma lformula0_trans0[simp]: "lformula0 (trans0 x)"
  by (cases x) (auto intro: lformula0.intros)

lemma lformula_trans[simp]: "lformula (trans \<phi>)"
  by (induct \<phi>) (auto simp: fBall.rep_eq)

lemma sat_trans:
  "xfov \<phi> |\<subseteq>| FV \<Longrightarrow> xsov \<phi> |\<subseteq>| SV \<Longrightarrow>
  sat (transi FV SV I) (trans \<phi>) \<longleftrightarrow> xsat I \<phi>"
unfolding xfov.abs_eq xsov.abs_eq less_eq_fset.rep_eq
  Abs_fset_inverse[simplified, OF xatomic.finite_fov]
  Abs_fset_inverse[simplified, OF xatomic.finite_sov]
proof (induction \<phi> arbitrary: I FV SV)
  case (A p a)
  then show ?case by (cases a) (auto simp: fmember.rep_eq)
next
  case (B p \<Phi>)
  from B.IH[of _ FV SV I] B.prems show ?case
   by (auto simp: fmember.rep_eq fBall.rep_eq fBex.rep_eq UN_subset_iff)
next
  case (Q p V \<phi>)
  from Q.prems show ?case unfolding xsat.simps
   by (cases p; cases V; hypsubst_thin; unfold prod.case polarity.case var.case)
     (subst Q.IH[symmetric]; auto intro!: iff_allI iff_exI sat_eqI split: if_splits simp: fun_upd_comp fun_eq_iff)+
qed

lemma sat_trans':
  "xfov \<phi> |\<subseteq>| fo_dom I \<Longrightarrow>
   sat I (trans \<phi>) \<longleftrightarrow> xsat \<lparr>xfo = fo I, xso = so I\<rparr> \<phi>"
unfolding xfov.abs_eq less_eq_fset.rep_eq Abs_fset_inverse[simplified, OF xatomic.finite_fov]
proof (induction \<phi> arbitrary: I)
  case (A p a)
  then show ?case by (cases a) (auto simp: fmember.rep_eq)
next
  case (B p \<Phi>)
  from B.IH[of _ I] B.prems show ?case
   by (auto simp: fmember.rep_eq fBall.rep_eq fBex.rep_eq UN_subset_iff)
next
  case (Q p V \<phi>)
  from Q.prems show ?case unfolding xsat.simps
   by (cases p; cases V; hypsubst_thin; unfold trans.simps prod.case polarity.case var.case sat.sat_gen.simps)
     (subst Q.IH; auto intro!: iff_allI iff_exI sat_eqI split: if_splits simp: fun_upd_comp fun_eq_iff)+
qed

end

definition xequiv :: "('fo, 'so) xformula \<Rightarrow> ('fo, 'so) xformula \<Rightarrow> bool" (infix "\<cong>\<^sub>x" 65) where
  "\<phi> \<cong>\<^sub>x \<psi> = (\<forall>I. xsat I \<phi> \<longleftrightarrow> xsat I \<psi>)"

definition ok_xinterp where
  "ok_xinterp I m \<equiv>
    (\<forall>x. xfo I x < m) \<and>
    (\<forall>X. \<forall>n. n |\<in>| xso I X \<longrightarrow> n < m)"

lemma xequiv_equiv:
  "trans \<phi> \<cong> trans \<psi> \<longleftrightarrow> \<phi> \<cong>\<^sub>x \<psi>"
proof
  assume eq: "trans \<phi> \<cong> trans \<psi>"
  show "\<phi> \<cong>\<^sub>x \<psi>" unfolding xequiv_def
    by (intro allI box_equals[OF eq[unfolded equiv_def, THEN spec, THEN mp]
     sat_trans[where FV = "xfov \<phi> |\<union>| xfov \<psi>" and SV = "xsov \<phi> |\<union>| xsov \<psi>"]
     sat_trans[where FV = "xfov \<phi> |\<union>| xfov \<psi>" and SV = "xsov \<phi> |\<union>| xsov \<psi>"]])
     (auto simp: xfov.rep_eq fmember.rep_eq fov.rep_eq)
next
  assume eq: "\<phi> \<cong>\<^sub>x \<psi>"
  show "trans \<phi> \<cong> trans \<psi>" unfolding equiv_def
    by (intro allI impI box_equals[OF eq[unfolded xequiv_def, THEN spec]
     sat_trans'[symmetric] sat_trans'[symmetric]])
     (auto simp: xfov.rep_eq fmember.rep_eq fov.rep_eq)
qed

definition (in norm) "check_eqv \<phi> \<psi> =
  (let \<phi> = trans \<phi>; \<psi> = trans \<psi> in WS1S_check_eqv (fov \<phi> |\<union>| fov \<psi>) (sov \<phi> |\<union>| sov \<psi>) \<phi> \<psi>)"

lemma (in norm) check_eqv_soundness:
  "check_eqv \<phi> \<psi> \<Longrightarrow> \<phi> \<cong>\<^sub>x \<psi>"
  unfolding check_eqv_def Let_def xequiv_equiv[symmetric] equiv_def
  by (blast dest: WS1S_check_eqv_soundness)

lemma (in norm) check_eqv_completeness:
  "\<phi> \<cong>\<^sub>x \<psi> \<Longrightarrow> check_eqv \<phi> \<psi>"
  unfolding check_eqv_def Let_def xequiv_equiv[symmetric]
  by (blast intro: lformula_trans WS1S_check_eqv_completeness)

(*<*)
end
(*>*)