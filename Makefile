NAME=src/monaco
OCAMLBUILD=ocamlbuild -use-ocamlfind -use-menhir -pkg safa \
           -plugin-tags "package(js_of_ocaml.ocamlbuild)" -yaccflag --explain
OCAMLFIND=ocamlfind
OBJS=$(wildcard _build/*.cm* _build/*.a _build/*.o)
# OBJS=$(wildcard _build/*.{cmi,cmo,cma,cmx,cmxa,a,o})

ifndef PREFIX
  PREFIX=/usr/local
else
  PREFIX:=${PREFIX}
endif

standalone:
	$(OCAMLBUILD) $(NAME).native

install: bc-lib nc-lib standalone 
	@$(OCAMLFIND) install $(NAME) META $(OBJS)
	install $(NAME).native $(PREFIX)/bin/$(NAME)

uninstall:
	$(OCAMLFIND) remove $(NAME)
	rm -f ${PREFIX}/bin/$(NAME)

lib: 
	$(OCAMLBUILD) $(NAME).cmxa

doc:
	$(OCAMLBUILD) $(NAME).docdir/index.html

clean:
	$(OCAMLBUILD) -clean 

bc-standalone:
	$(OCAMLBUILD) $(NAME).byte

bc-lib:
	$(OCAMLBUILD)  $(NAME).cma

nc-lib:
	$(OCAMLBUILD)  $(NAME).cmxa

web:
	$(OCAMLBUILD) -I src applet/applet.js

run:
	$(OCAMLBUILD) $(NAME).native 
	./monaco.native $(CMD)

tests::
	$(OCAMLBUILD) $(NAME).native 
	$(OCAMLBUILD) -I src tests/tests.native 
	./tests.native $(CMD)

#	find tests -name *.mona -exec ./monaco.native $(CMD) {} \;

benchs::
	$(OCAMLBUILD) -I src tests/bench.native 
	./bench.native $(CMD)

dantoni:
	$(OCAMLBUILD) -I src tests/dantoni.native
	./dantoni.native $(CMD)

adder:
	$(OCAMLBUILD) -I src tests/adder.native 
	./adder.native $(CMD)

strand: standalone
	find benchmarks/strand -name *.mona -exec ./monaco.native $(CMD) {} false \;

gaston: standalone
	find benchmarks -name *.mona -exec ./monaco.native $(CMD) {} \;

tacas-17: standalone
	find benchmarks/tacas-17 -name *.mona -exec ./monaco.native $(CMD) {} \;

