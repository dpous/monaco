module S = Monaco

module Html = Dom_html

let (>>=) = Lwt.bind

let rec empty box = 
  match Js.Opt.to_option box##firstChild with 
    | Some c -> Dom.removeChild box c; empty box 
    | None -> ()

let app p mk =
  let e = mk Html.document in
  Dom.appendChild p e;
  e

let add_string p s = 
  Dom.appendChild p (Html.document##createTextNode (Js.string s))

let formatter box = Format.(
  let b = ref "" in
  let q = Stack.create() in
  let output s i n = b := !b^String.sub s i n in 
  let flush () = add_string (Stack.top q) !b; b := "" in
  let out_newline () = flush(); ignore (app (Stack.top q) Html.createBr) in
  let clear () = Stack.clear q; Stack.push box q; empty box in
  let mark_open_tag color =
    flush();
    let d = app (Stack.top q) Html.createDiv in 
    d##style##cssText <- (Js.string ("color:"^color)); 
    Stack.push d q; ""
  in
  let mark_close_tag _ = flush(); ignore (Stack.pop q); "" in
  let f = make_formatter output flush in
  let o = pp_get_formatter_out_functions f () in
  let t = pp_get_formatter_tag_functions f () in
  pp_set_formatter_out_functions f {o with out_newline};
  pp_set_formatter_tag_functions f {t with mark_open_tag; mark_close_tag};
  pp_set_print_tags f false;
  pp_set_mark_tags f true;
  Stack.push box q;
  f, clear )
let print (f,_) = Format.fprintf f
let clear (_,f) = f()

let arg x z = try List.assoc x (Url.Current.arguments) with Not_found -> z

let onload _ =

  let get s = 
    Js.Opt.get (Html.document##getElementById(Js.string s))
      (fun () -> assert false)
  in
  let input ?(r=1) s i = 
    let box = app (get s) Html.createTextarea in
    box##rows <- r; box##cols <- 30;
    box##value <- Js.string i;
    box
  in
  (* let input ?(r=1) s i =  *)
  (*   let box = get s in *)
  (*   box##textContent <- Js.some (Js.string i); *)
  (*   box *)
  (* in *)
  let output box = formatter (get box) in

  let xbox = input "x" (arg "x" "x<=y -> y<=z -> x<=z" ) in
  let ybox = input "y" (arg "y" "T") in
  let xlog = output "xlog" in
  let ylog = output "ylog" in
  let log = output "log" in
  let answer = output "answer" in
  let iterations = output "iterations" in
  let options = get "options" in

  let m2l = app options Html.createSelect in
  add_string (app m2l Html.createOption) "WS1S semantics";
  add_string (app m2l Html.createOption) "M2L semantics";
  m2l##selectedIndex <- if !S.m2l then 1 else 0;
  ignore (app options Html.createBr);

  let hk = app options (Html.createInput ~_type:(Js.string "checkbox")) in
  hk##checked <- Js.bool !S.hk;
  add_string options "Use disjoint set forests (Hopcroft and Karp)";
  ignore (app options Html.createBr);

  let dontrestrict = app options (Html.createInput ~_type:(Js.string "checkbox")) in
  dontrestrict##checked <- Js.bool !S.dontrestrict;
  add_string options "Do not restrict free firstorder variables";
  ignore (app options Html.createBr);

  (* let construction = app options Html.createSelect in *)
  (* add_string (app construction Html.createOption) "Brzozowski's derivatives"; *)
  (* add_string (app construction Html.createOption) "Antimirov' partial derivatives"; *)
  (* ignore (app options Html.createBr); *)

  let rendering = app options Html.createSelect in
  add_string (app rendering Html.createOption) "Local rendering (slow)";
  add_string (app rendering Html.createOption) "Google charts rendering (buggy)";
  add_string (app rendering Html.createOption) "No rendering (fast)";
  
  let lgraph = get "lgraph" in
  let ggraph = app (get "ggraph") Html.createImg in

  let draw_graph () =
    match rendering##selectedIndex with
      | 0 -> 
	let s = Trace.render !S.hk "\\\"" in
	let s = "document.getElementById(\"lgraph\").innerHTML=Viz(\""^s^"\",\"svg\",\"dot\");" in
	ignore (Js.Unsafe.eval_string s);
      | 1 -> 
	let s = Trace.render !S.hk "\"" in
	let s = Url.urlencode s in
	let s = "https://chart.googleapis.com/chart?cht=gv&chl="^s in
	ggraph##src <- Js.string s;
      | _ -> ()
  in

  let add_example =
    let tbl = app (get "examples") Html.createTable in
    fun (x,v,y) -> 
      let r = app tbl Html.createTr in
      r##onclick <- Html.handler (fun _ -> 
	xbox##value <- Js.string x;
	ybox##value <- Js.string y;
	Js._false);
      let rem = app (app r Html.createTd) Html.createImg in 
      rem##src <- Js.string "rem.png"; 
      rem##onclick <- Html.handler (fun _ -> Dom.removeChild tbl r; Js._false);
      add_string (app r Html.createTd) (" "^x^" "^v^" "^y);
  in
  let add_btn = app (get "examples") Html.createButton in
  add_string add_btn "add current";

  let parse f log x =
    try Some (f x) 
    with
      | Failure m -> print log "@{<red>%s@}%!" m; None
      | e -> print log "@{<red>%s@}%!" (Printexc.to_string e); None
  in

  let value box = Js.to_string (box##value) in
  (* let value box = match Js.Opt.to_option (box##textContent) with *)
  (*   | Some x -> Js.to_string x *)
  (*   | None -> "" *)
  (* in *)
  
  let last_answer = ref "" in
  let onchange _ =
    clear log; clear xlog; clear ylog; clear answer; clear iterations;
    empty lgraph; ggraph##src <- Js.string ""; add_btn##disabled <- Js._true;
    let x = parse S.parse_expr xlog (value xbox) in
    let y = parse S.parse_expr ylog (value ybox) in
    (try match x,y with
      | None,_ | _,None -> ()
      | Some x, Some y -> 
	Stats.reset();
	Bdd.reset_caches();
	let r = S.equiv x y in
	add_btn##disabled <- Js._false;
	(match r with
	  | None -> last_answer:="="; print answer "=%!"; 
	  | Some g -> 
	    last_answer:="≠"; print answer "≠%!"; 
	    print log "counter-example: %a%!" Types.print_gstring g;
	);
	print iterations "(%i iterations, %i BDD unifications)%!"
	  (Stats.get "iterations") (Stats.get "unify calls");
	draw_graph()
     with 
       | e -> print log "@{<red>%s@}%!" (Printexc.to_string e));
    Js._false
  in

  let rec dyn_preview xo yo n =
    let x = value xbox in
    let y = value ybox in
    let n =
      if xo=x && yo=y then max 0 (n-1) 
      else (ignore(onchange()); 20)
    in
    Lwt_js.sleep (if n = 0 then 0.5 else 0.1) >>= fun () ->
    dyn_preview x y n
  in
  
  hk##onchange <- Html.handler (fun _ -> S.hk := Js.to_bool (hk##checked); onchange());
  m2l##onchange <- Html.handler (fun _ -> S.m2l := m2l##selectedIndex = 1; onchange());
  dontrestrict##onchange <- Html.handler (fun _ -> S.dontrestrict := Js.to_bool (dontrestrict##checked); onchange());
  (* construction##onchange <- Html.handler (fun _ ->  *)
  (*   (match construction##selectedIndex with *)
  (*     | 0 -> S.construction := `Brzozowski *)
  (*     | _ -> S.construction := `Antimirov); *)
  (*   onchange ()); *)
  rendering##onchange <- Html.handler (fun _ -> onchange());
  add_btn##onclick <- Html.handler (fun _ -> 
    add_example (value xbox,!last_answer,value ybox); 
    Js._false);

  List.iter add_example [
    "x<=y -> y<=z -> x<=z","=","T";
    "x<y /\\ y<x","=","F";
    "\\E y, x<y","=","T";
    "\\E Y, \\A y, x<y -> y \\in Y","=","F";
    "\\E Y, \\A y, y<x -> y \\in Y","=","T";
    "\\A t, (\\A x, x<=t -> x\\in X1) -> (\\A x, x<=t -> x\\in X1)","=","T";
    "\\A t, (\\A x, x<=t -> x\\in X1 -> x\\in X2) -> (\\A x, x<=t -> x\\in X1) -> (\\A x, x<=t -> x\\in X2)","=","T";
    "\\A t, (\\A x, x<=t -> x\\in X1 -> x\\in X2 -> x\\in X3) -> (\\A x, x<=t -> x\\in X1) -> (\\A x, x<=t -> x\\in X2) -> (\\A x, x<=t -> x\\in X3)","=","T";
  ];

  ignore (dyn_preview "" "" 0);
  Js._false

let _ =
  S.trace := true;
  Html.window##onload <- Html.handler onload
