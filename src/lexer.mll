{
(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Parser
open Types
open Util

let kwd ~mona lexbuf w = 
  if mona then match w with
  | "ws2s" | "m2l-tree" | "guide" | "universe" 
  | "tree" | "root" | "variant" | "type" | "in_state_space" 
  | "sometype" | "const_tree"
  | "import" | "export" | "include"
  | "allpos" | "prefix" | "restrict" | "execute"
   -> lexing_error lexbuf "unsupported MONA feature: %s" w
  | "ws1s"	    -> HDR `WS1S
  | "m2l-str"	    -> HDR `M2L
  | "const" 	    -> CONST
  | "pconst" 	    -> PCONST
  | "assert" 	    -> ASSERT
  | "var0"	    -> VAR K0
  | "var1"	    -> VAR K1
  | "var2"	    -> VAR K2
  | "macro"	    -> MACRO
  | "pred"	    -> MACRO
  | "where" 	    -> WHERE
  | "defaultwhere0" -> DFTW K0
  | "defaultwhere1" -> DFTW K1
  | "defaultwhere2" -> DFTW K2
  | _ -> ID w
  else ID w

}

let blank = [' ' '\t' ]+
let newline = ['\r' '\n' ] | "\r\n"
let num = ['0'-'9' ]+
let alpha = ['a'-'z' 'A'-'Z' '$']
let alphanums = ['a'-'z' 'A'-'Z' '$' '0'-'9' '_' '\'' '"' ]*

rule token m = parse
  | newline                           { Lexing.new_line lexbuf; token m lexbuf }
  | blank                             { token m lexbuf }
  | "false" | "⊥"		      { FALSE }
  | "true" | "⊤" 		      { TRUE }
  | "all" | "∀"			      { A_ }
  | "all0"    			      { A K0 }
  | "all1"    			      { A K1 }
  | "all2"    			      { A K2 }
  | "ex" | "∃" 		      	      { E_ }
  | "ex0"    			      { E K0 }
  | "ex1"    			      { E K1 }
  | "ex2"    			      { E K2 }
  | "let0"    			      { LET K0 }
  | "let1"    			      { LET K1 }
  | "let2"    			      { LET K2 }
  | "in" | "∈"		     	      { IN }
  | "notin" | "∉"	     	      { NIN }
  | "sub" | "⊆"			      { SUB }
  | "notsub" | "⊄"		      { NSUB }
  | "empty" | "∅"		      { EMPTY }
  | "min" 		      	      { MIN }
  | "max" 		      	      { MAX }
  | "union" | "∪"	      	      { UNION }
  | "inter" | "∩"	      	      { INTER }
  | num as n        		      { NUM (int_of_string n) }
  | (alpha alphanums) as s            { kwd m lexbuf s }
  | ("m2l-" alpha alphanums) as s     { kwd m lexbuf s }
  | '~' | "¬"                         { NEG }
  | '&' | "∧"			      { CNJ }
  | '|' | "∨" 			      { DSJ }
  | "=>" | "->"			      { IMP }
  | "<=>"  | "<->"		      { IFF }
  | '='                               { EQ }
  | "=_1"                             { EQ1 }
  | "=_2"                             { EQ2 }
  | "~=" | "≠"   	              { NEQ }
  | "~=_1" | "≠_1"   	              { NEQ1 }
  | "~=_2" | "≠_2"   	              { NEQ2 }
  | '<'                               { LT }
  | '>'                               { GT }
  | "<=" | "≤"                        { LE }
  | ">=" | "≥"                        { GE }
  | '+'                               { PLUS }
  | '*'                               { MULT }
  | '-'                               { MINUS }
  | '%'                               { PERCENT }
  | '\\'                              { BACKSLASH }
  | '/'                               { DIV }
  | '('                               { LPAR }
  | ')'                               { RPAR }
  | '{'                               { LBRK }
  | '}'                               { RBRK }
  | ','                               { COMMA }
  | ':'                               { COLON }
  | ';'                               { SEMI }
  | ('.' | '^') as c		      { if m then 
                                         lexing_error lexbuf "unsupported MONA feature: `%c'" c 
    	  			      	else lexing_error lexbuf "unexpected character: `%c'" c }
  | "/*"                              { skip m 1 lexbuf }
  | '#'                               { skip_line m lexbuf }
  | _ as c                            { lexing_error lexbuf "unexpected character: `%c'" c }
  | eof                               { EOF }

and skip m n = parse
  | newline                           { Lexing.new_line lexbuf; skip m n lexbuf }
  | "*/"                              { if n=1 then token m lexbuf else skip m (n-1) lexbuf }
  | "/*"                              { skip m (n+1) lexbuf }
  | _                                 { skip m n lexbuf }
  | eof                               { lexing_error lexbuf "unterminated comment" }

and skip_line m = parse
  | newline                           { Lexing.new_line lexbuf; token m lexbuf }
  | _                                 { skip_line m lexbuf }
  | eof                               { EOF }

{
  let token ?(mona=false) lexbuf = token mona lexbuf
}
