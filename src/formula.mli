(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** internal representation for M2L/WS1S formulas *)

open Common
open Types

(** M2L (hash-consed) expressions *)
type expr_ 
type expr = expr_ hval

(** sets of expressions *)
type expr_set = expr_ hset 

(** smart constructors for normalised expressions: 
    disjunctions are sorted and associated to the left, 
    duplicates and [F] are removed, negations are simplified *)
val sup: expr_set -> expr
val inf: expr_set -> expr
val dsj: expr -> expr -> expr
val cnj: expr -> expr -> expr
val imp: expr -> expr -> expr
val iff: expr -> expr -> expr
val xor: expr -> expr -> expr
val neg: expr -> expr
val ex_r: varset -> expr -> expr
val fa_r: varset -> expr -> expr
val ex0: var0 -> expr -> expr
val ex1: var1 -> expr -> expr
val ex2: var2 -> expr -> expr
val fa0: var0 -> expr -> expr
val fa1: var1 -> expr -> expr
val fa2: var2 -> expr -> expr
val ff: expr
val tt: expr

(** atomic expressions *)
val le: var1 -> var1 -> expr
val lt: var1 -> var1 -> expr
val mem: var1 -> var2 -> expr
val eqn: var1 -> var1 -> int -> expr
val eqk: var1 -> int -> expr
val sub: var2 -> var2 -> expr
val emp: var2 -> expr
val v0: var0 -> expr
                                     
(** free variables *)
val fv: expr -> varset

(** M2L output *)
val epsilon_m2l: expr -> bool

(** WS1S output *)
val epsilon_ws1s: expr -> bool

(** left and right derivatives *)
val lderiv: expr -> expr node span
val rderiv: expr -> expr node span

(** right derivatives w.r.t 0s *)
val rderiv_0: expr -> expr

(** WS1S simplification of a formula (closed sub-formulas are replaced
 by either true or false, useless 0- and 1-order quantifications are
 removed) *)
val ws1s_simplify: expr -> expr

(** Brzozowski's automaton for M2L *)
val brzozowski_m2l: (expr,unit,var,bool) Automata.sdfa

(** Brzozowski's automaton for WS1S *)
val brzozowski_ws1s: (expr,unit,var,bool) Automata.sdfa

(** Brzozowski's automaton for WS1S, with simplifications *)
val brzozowski_ws1s': (expr,unit,var,bool) Automata.sdfa


(** adding toplevel restrictions *)
val restrict1: expr -> expr
val restrict2: expr -> expr -> expr*expr
                   
(** a partial order contained in logical implication 
    the number [n] bounds the recursion depth, unless it is negative.
    (i.e., the greater the more precise and the more expensive, -1 being a top value) *)
val compare: ?n: int -> expr -> expr -> [`Ge|`Lt|`Un]

(** pretty-printing expressions *)
val print_expr: Format.formatter -> expr -> unit
