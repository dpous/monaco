(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Common
open Types

val print_unit: Format.formatter -> unit -> unit
val print_bool: Format.formatter -> bool -> unit
val print_var: Format.formatter -> var -> unit
val print_varset: Format.formatter -> varset -> unit
val print_gstring: gstring formatter

val failwith: ('a, Format.formatter, unit, 'b) format4 -> 'a
val error: int -> ('a, Format.formatter, unit, 'b) format4 -> 'a

exception ParsingError_ of Lexing.position * Lexing.position * string

val parsing_error: Lexing.position -> Lexing.position ->
                   ('a, Format.formatter, unit, 'b) format4 -> 'a
val lexing_error: Lexing.lexbuf ->
                  ('a, Format.formatter, unit, 'b) format4 -> 'a

exception ParsingError of string

val locate_parsing_errors:
  Lexing.lexbuf -> exn -> string -> ('a -> 'b) -> 'a -> 'b

val exit_on_parsing_errors: ('a -> 'b) -> 'a -> 'b

val vs_empty: varset
val vs_union: varset -> varset -> varset
val vs_inter: varset -> varset -> varset
val vs_diff: varset -> varset -> varset
val vs_fold: (kind -> var -> 'a -> 'a) -> varset -> 'a -> 'a
val vs_of_lists: var0 list -> var1 list -> var2 list -> varset
val vs_singleton: kind -> var -> varset
val vs_flatten: varset list -> varset
val vs_mem: kind * var -> varset -> bool
val vs_conflicts: varset -> var set
val set_of_varset: varset -> var set
