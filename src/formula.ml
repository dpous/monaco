(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(* efficient representaion of WS1S / M2L formulas *)

open Common
open Hashcons
open Types
open Util

(* atomic expressions *)
type atom =
  | Lt of var1 * var1           (* x < y *)
  | Mem of var1 * var2          (* x in Y *)
  | V0 of var0                  (* order-0 variable *)
  | Eq of var1 * var1 * int     (* x = y + n, n must be >=0 *)
  | EqK of bool * var1 * int    (* x = n, n must be >=0 (Boolean set to false for reversal) *)
  | Len of int                  (* length is n *)
  (* restrictions *)
  | R1 of var1                  (* order-1 *)
  | R0 of var0                  (* order-0 *)
  (* below: not necessary *)
  | Sub of var2 * var2          (* X subset Y *)
  | Emp of var2

(* hash-consed formulas built from atomic expressions, Boolean connectives, anq quantifiers *)
type head =
  | A of bool * atom		(* atomatic formula (negated when Boolean is true) *)
  | B of bool * expr_set	(* n-ary Boolean connectives (disjunction when Boolean is false)*)
  | Q of bool * varset * expr	(* quantifiers (exists when Boolean is false)*)
and expr_ = {h: head; v: varset; o: bool}
and expr = expr_ hval
and expr_set = expr_ hset

let hash x = x.hkey
let head x = x.node.h
let fv x = x.node.v
let epsilon_m2l x = x.node.o

let hashcons =
  let hash x = match x.h with
    | A(p,a) -> Hashtbl.hash (p,a)
    | B(p,xs) -> Hashtbl.hash (p,Hset.hash xs)
    | Q(p,vs,x) -> Hashtbl.hash (p,x.hkey,vs)
  in
  let equal x y = match x.h,y.h with
    | A(p,a),A(p',a') -> p=p' && a=a'
    | B(p,xs),B(p',xs') -> p=p' && Hset.equal xs xs'
    | Q(p,vs,x),Q(p',vs',x') -> p=p' && x==x'&& vs=vs'
    | _ -> false
  in
  let t = Hashcons.create 100003
  in
  (* at_exit (fun () -> *)
  (*     let (len,num,_,min,med,max) = Hashcons.stats t in *)
  (*     Format.printf "Formulas: len=%i, num=%i, min=%i, med=%i, max=%i\n" len num min med max *)
  (*   ); *)
  Hashcons.hashcons hash equal t

(* pretty printing *)
let print_atom b f =
  if not b then function
  | Lt(i,j) -> Format.fprintf f "%s<%s" i j
  | Eq(i,j,0) -> Format.fprintf f "%s=%s" i j
  | Eq(i,j,k) -> Format.fprintf f "%s=%s+%i" i j k
  | EqK(true,i,k) -> Format.fprintf f "%s=%i" i k
  | EqK(false,i,k) -> Format.fprintf f "%s[=]%i" i k
  | Len(k) -> Format.fprintf f "|.|=%i" k
  | Sub(i,j) -> Format.fprintf f "%s⊆%s" i j
  | Mem(i,j) -> Format.fprintf f "%s in %s" i j
  | Emp(i) -> Format.fprintf f "%s=∅" i
  | R1 i -> Format.fprintf f "R1(%s)" i
  | R0 i -> Format.fprintf f "R0(%s)" i
  | V0 i -> Format.fprintf f "%s" i
  else function
  | Lt(i,j) -> Format.fprintf f "%s≤%s" j i
  | Eq(i,j,0) -> Format.fprintf f "%s≠%s" i j
  | Eq(i,j,k) -> Format.fprintf f "%s≠%s+%i" i j k
  | EqK(true,i,k) -> Format.fprintf f "%s≠%i" i k
  | EqK(false,i,k) -> Format.fprintf f "%s[≠]%i" i k
  | Len(k) -> Format.fprintf f "|.|≠%i" k
  | Sub(i,j) -> Format.fprintf f "%s⊄%s" i j
  | Mem(i,j) -> Format.fprintf f "%s notin %s" i j
  | Emp(i) -> Format.fprintf f "%s≠∅" i
  | R1 i -> Format.fprintf f "¬R1(%s)" i
  | R0 i -> Format.fprintf f "¬R0(%s)" i
  | V0 i -> Format.fprintf f "¬%s" i

let rec print_expr l f x =
  match head x with
  | A(p,a) -> print_atom p f a
  | B(false,xs) -> Format.fprintf f (paren l 1 "%a") (print_b "∨" "⊥" 1) (Hset.elements xs)
  | B(true, xs) -> Format.fprintf f (paren l 2 "%a") (print_b "∧" "⊤" 2) (Hset.elements xs)
  | Q(p,vs,x) -> Format.fprintf f (paren l 0 "%s%a: %a") (if p then "∀" else "∃")
                                print_varset vs (print_expr 0) x
and print_b b c l f = function
  | [] -> Format.fprintf f "%s" c
  | [x] -> print_expr l f x
  | x::y -> Format.fprintf f "%a %s %a" (print_expr l) x b (print_b b c l) y
let print_expr = print_expr 0


(* free variables *)
let fv_atom = function
  | Lt(i,j) | Eq(i,j,_) -> vs_of_lists [] [i;j] []
  | Sub(i,j) -> vs_of_lists [] [] [i;j]
  | Mem(i,j) -> vs_of_lists [] [i] [j]
  | Emp(i) -> vs_of_lists [] [] [i]
  | R1 i | EqK(_,i,_) -> vs_of_lists [] [i] []
  | V0 i | R0 i -> vs_of_lists [i] [] []
  | Len _ -> vs_empty


(* M2L output *)
let dontcare = false
let epsilon_atom = function
  | Sub(_,_) | Emp _ -> true
  | Eq(_,_,_) | EqK(_,_,_) | V0 _ | Lt(_,_) | Mem(_,_) -> dontcare
  | R1 _ | R0 _ -> false
  | Len n -> n=0

let pnot p x = if p then not x else x
let pb p = if p then Hset.for_all else Hset.exists

(* low level constructors for formula *)
let a_ p a =
  hashcons {
      h=A(p,a);
      v=fv_atom a;
      o=pnot p (epsilon_atom a) }
let a = a_ false

let b_ p xs =
  hashcons {
      h=B(p,xs);
      v=Hset.fold (fun x l -> vs_union (fv x) l) xs vs_empty;
      o=pb p epsilon_m2l xs}
let q_ p vs x =
  if vs=vs_empty then x
  else
    let o =
      if Set.is_empty vs.s0 && Set.is_empty vs.s1 then epsilon_m2l x
      else p
    in
    hashcons {
        h=Q(p,vs,x);
        v=vs_diff (fv x) vs;
        o }


(* reversal *)
let rev_atom = function
  | Lt(i,j) -> Lt(j,i)
  | Eq(i,j,n) -> Eq(j,i,n)
  | EqK(b,i,n) -> EqK(not b,i,n)
  | x -> x

let rec rev x = match head x with
  | A(p,a) -> let a'=rev_atom a in if a==a' then x else hashcons {x.node with h=A(p,a')}
  | B(p,xs) -> hashcons {x.node with h=B(p,Hset.map rev xs)}
  | Q(p,vs,y) -> hashcons {x.node with h=Q(p,vs,rev y)}


(* negation *)
let rec neg x = match head x with
  | A(p,a) -> hashcons {x.node with h=A(not p,a); o=not x.node.o}
  | B(p,xs) -> hashcons {x.node with h=B(not p,Hset.map neg xs); o=not x.node.o}
  | Q(p,vs,y) -> hashcons {x.node with h=Q(not p,vs,neg y); o=not x.node.o}

let pneg p x = if p then neg x else x


(* Boolean constants *)
let b0_ p = b_ p Hset.empty
let ff = b0_ false
let tt = b0_ true

(* slightly normalising Boolean connectives: it remains to get rid of the opposite constants *)
let b'_ p xs = match Hset.cardinal xs with
  | 1 -> Hset.choose xs
  | _ -> b_ p xs

let lb_ p =
  let rec lb_ s xs =
    Hset.fold
      (fun x s ->
       match head x with
       | B(p',ys) when p=p' -> lb_ s ys
       | _ -> Hset.add x s
      ) xs s
  in lb_

(* normalising n-ary connectives *)
let b_ p xs =
  let c = b0_ (not p) in
  if Hset.mem c xs then c else b'_ p (lb_ p Hset.empty xs)

(* normalising binary connectives *)
let b2_ p e f =
  let c = b_ (not p) Hset.empty in
  if e==f then e else
  if e==c || f==c then c else
    match head e,head f with
    | B(p',xs),B(p'',ys) when p=p' && p=p'' -> b'_ p (Hset.union xs ys)
    | B(p',xs),_ when p=p' -> b'_ p (Hset.add f xs)
    | _,B(p',ys) when p=p' -> b'_ p (Hset.add e ys)
    | _,_ -> b'_ p (Hset.add e (Hset.singleton f))

(* user-friendly exported functions *)
let sup = b_ false
let inf = b_ true
let dsj = b2_ false
let cnj = b2_ true
let imp x y = dsj (neg x) y
let iff x y = cnj (imp x y) (imp y x)
let pmi x y = cnj (neg x) y
let xor x y = dsj (pmi x y) (pmi y x)


(* M2L normalising quantifiers: only 2nd order useless quantifiers are removed *)
let q'_ = q_
let vs_inter2 u v = { u with s2 = Set.inter u.s2 v.s2 }
let rec q_ p vs x =
  match head x with
  | Q(p',ws,x) when p=p' -> q_ p (vs_union vs ws) x
  | B(p',xs) when p=p' -> b_ p (Hset.map (q_ p vs) xs)
  | _ -> q'_ p (vs_inter2 vs (fv x)) x

let strip_B p x =
  match head x with
  | B(p',xs) when p=p' -> xs
  | _ -> Hset.singleton x
type rhs =
  | C of int          (* n, n >= 0 *)
  | V of var1         (* x *)
  | VC of var1 * int  (* x + n, n may be negative *)
let subst_atom p v rhs x =
  match x with
  | Lt(i,j)  when (v=i || v=j) -> None
  | Mem(i,j) when v=i -> None
  | EqK(false,i,_) when v=i -> None
  | R1 i when v=i -> None
  | Eq(i,j,n) when v=i ->
      (match rhs with
      | V(k) -> Some (if k=j then b0_ (pnot p (n=0)) else a_ p (Eq(k,j,n)))
      | C(m) -> if m>=n then Some (a_ p (EqK(true,j,m-n))) else None
      | VC(k,m) -> if n>=m then Some (a_ p (Eq(k,j,n-m))) else Some (a_ p (Eq(j,k,m-n))))
  | Eq(i,j,n) when v=j ->
      (match rhs with
      | V(k) -> Some (if k=i then b0_ (pnot p (n=0)) else a_ p (Eq(i,k,n)))
      | C(m) -> Some (a_ p (EqK(true,i,m+n)))
      | VC(k,m) -> if -m>=n then Some (a_ p (Eq(k,i,-m-n))) else Some (a_ p (Eq(i,k,m+n))))
  | EqK(true,i,n) when v=i ->
      (match rhs with
      | V(j) -> Some (a_ p (EqK(true,j,n)))
      | C(m) -> Some (b0_ (pnot p (m=n)))
      | VC(j,m) -> if n>=m then Some (a_ p (EqK(true,j,n-m))) else None)
  |_ -> Some (a_ p x)
let option_map f = function (*Library?*)
  | None -> None
  | Some x -> Some (f x)
let the_default d = function (*Library?*)
  | None -> d
  | Some x -> x
let image_option f xs = Hset.fold (fun x -> function
    | None -> None
    | Some xs ->
      match f x with
      | None -> None
      | Some x -> Some (Hset.add x xs)) xs (Some Hset.empty)
let rec subst v rhs x =
  match head x with
  | A(p,x) -> subst_atom p v rhs x
  | B(p,xs) -> option_map (b_ p) (image_option (subst v rhs) xs)
  | Q(p,vs,x) -> if Set.mem v vs.s1 then Some (q'_ p vs x) else option_map (q_ p vs) (subst v rhs x)
let subst_one_point p vs x xs =
  match head x with
  | A(p',Eq(i,j,n)) when p=p' && Set.mem i vs ->
     the_default xs (image_option (subst i (if n = 0 then V j else VC(j,n))) (Hset.remove x xs))
(*  | A(p',Eq(i,j,n)) when p=p' && Set.mem j vs ->
     the_default xs (image_option (subst j (if n = 0 then V i else VC(i,-n))) (Hset.remove x xs))*)
  | A(p',EqK(true,i,n)) when p=p' && Set.mem i vs ->
     the_default xs (image_option (subst i (C n)) (Hset.remove x xs))
  | _ -> xs
let one_point_q p vs x =
  let y = b_ (not p) (let xs = strip_B (not p) x in Hset.fold (subst_one_point p vs.s1) xs xs) in
  q_ p (vs_inter vs (fv y)) y

(* WS1S normalising quantifiers: all useless quantifiers are removed *)
let rec q_ws1s p vs x =
  match head x with
  | Q(p',ws,x) when p=p' -> q_ws1s p (vs_union vs ws) x
  | B(p',xs) when p=p' -> b_ p (Hset.map (q_ws1s p vs) xs)
  | _ -> (*one_point_q*) q'_ p (vs_inter vs (fv x)) x

(* exported quantifiers *)
let ex_r vs x = q_ false vs x
let fa_r vs x = q_ true vs x
let ex0 x = ex_r (vs_singleton K0 x)
let ex1 x = ex_r (vs_singleton K1 x)
let ex2 x = ex_r (vs_singleton K2 x)
let fa0 x = fa_r (vs_singleton K0 x)
let fa1 x = fa_r (vs_singleton K1 x)
let fa2 x = fa_r (vs_singleton K2 x)

(* restrictions *)
let r1 i = a (R1 i)
let r0 i = a (R0 i)

(* adding toplevel restrictions *)
let close p l x =
  Set.fold (fun v -> b2_ (not p) (pneg p (r1 v))) l.s1
 (Set.fold (fun v -> b2_ (not p) (pneg p (r0 v))) l.s0 x)
let restrict1 x =
  close false (fv x) x
let restrict2 x y =
  let l = vs_union (fv x) (fv y) in
  close false l x, close false l y

(* a preorder approximating logical implication *)
let rec leq ?(n=2) x y =
  x==y || match n with
          | 0 ->
             (match head x,head y with
             | B(false,xs),_ -> Hset.for_all (geq ~n:0 y) xs
             | _,B(true,ys) -> Hset.for_all (leq ~n:0 x) ys
             | B(true,xs),_ -> Hset.mem y xs
             | _,B(false,ys) -> Hset.mem x ys
             | _ -> false)
          | n ->
             (match head x,head y with
             | B(false,xs),_ -> Hset.for_all (geq ~n:(n-1) y) xs
             | _,B(true,ys) -> Hset.for_all (leq ~n:(n-1) x) ys
             | B(true,xs),_ -> Hset.exists (geq ~n:(n-1) y) xs
             | _,B(false,ys) -> Hset.exists (leq ~n:(n-1) x) ys
             | _ -> false)
and geq ?n x y = leq ?n y x

let rec compare ?n x y =
  if leq ?n y x then `Ge else if leq ?n x y then `Lt else `Un


(* exported atomic formula *)
let lt i j = if i=j then ff else a (Lt(i,j))
let le i j = neg (lt j i)
let mem i j = assert(i<>j); a (Mem(i,j))
let sub i j = if i=j then tt else a (Sub(i,j))
let emp i = a (Emp i)
let len n =
  if n<0 then ff else a (Len n)
let eqn i j n =
  if i=j then if n=0 then tt else ff
  else if n>=0 then a (Eq(i,j,n))
  else a (Eq(j,i,-n))
let eqk i n =
  if n<0 then ff else a (EqK(true,i,n))
let v0 i = a (V0 i)


(* (left/right) derivation *)

let m = Bdd.init ~size:10000 hash (==)
let k = Bdd.constant m
let n i = Bdd.node m i
let nk i x y = n i (k x) (k y)
let ite i j x00 x01 x10 x11 =
  if i<j then n i (nk j x00 x01) (nk j x10 x11)
  else n j (nk i x00 x10) (nk i x01 x11)

let deriv_atom a ff tt slf = function
  | Lt(i,j)  -> ite i j slf ff  tt ff
  | Mem(i,j) -> ite i j slf slf ff tt
  | Sub(i,j) -> ite i j slf slf ff slf
  | Emp(i) -> nk i slf ff
  | Eq(i,j,0) -> ite i j slf ff ff tt
  | Eq(i,j,n) -> ite i j slf (a (EqK(true,i,n-1))) ff ff
  | EqK(true,i,0) -> nk i ff tt
  | EqK(true,i,n) -> nk i (a (EqK(true,i,n-1))) ff
  | EqK(false,i,n) -> nk i slf (a (Len n))
  | Len 0 -> k ff
  | Len n -> k (a (Len (n-1)))
  | V0 i -> nk i ff tt
  | R1 i -> nk i slf tt
  | R0 _ -> k tt

let bdd_hide qp k2p vs =
  let {s0;s1;s2} = vs in
  let rec hide x ss1 =
    match Bdd.head x with
    | Bdd.V e -> Bdd.constant m (qp {s0=Set.empty;s1=ss1;s2} e)
    | Bdd.N(b,l,r) ->
       if Set.mem b s0 || Set.mem b s2 then
         k2p (hide l ss1) (hide r ss1)
       else if Set.mem b s1 then
         k2p (hide l (Set.add b ss1)) (hide r ss1)
       else Bdd.node m b (hide l ss1) (hide r ss1)
  in fun x -> hide x Set.empty

let kff,ktt = k ff,k tt
let kdsj,kcnj = Bdd.binary m dsj, Bdd.binary m cnj
let k0 = function false -> kff | true -> ktt
let k2 = function false -> kdsj | true -> kcnj
let adf,adt = deriv_atom (a_ false) ff tt, deriv_atom (a_ true) tt ff
let a_deriv = function false -> adf | true -> adt
let deriv =
  memo_rec1
    (fun deriv x ->
      match head x with
      | A(p,a) -> a_deriv p x a
      | B(p,xs) ->
	 let k0np = k0 (not p) in
	 Hset.fold (fun x a -> if a==k0np then k0np else k2 p (deriv x) a) xs (k0 p)
      | Q(p,vs,x) -> bdd_hide (q_ p) (k2 p) vs (deriv x))

let lderiv x = Span.single () (deriv x) (k0 false)
let rderiv x = Span.map (Bdd.unary m rev) (lderiv (rev x))

(* right derivation restricted to 0s *)
let rec left x = match Bdd.head x with
   | Bdd.V y -> y
   | Bdd.N(_,x,_) -> left x
let left x = left (Span.get x ())
let rderiv_0 x = rev (left (lderiv (rev x)))

(* futurisation, to compute WS1S outputs *)
let rec fut x xs =
  if Hset.mem x xs then xs
  else fut (rderiv_0 x) (Hset.add x xs)
let futurize p x =
  let x' = b_ p (fut x Hset.empty) in
  (* Format.eprintf "fut:\n   %a\n   %a\n\n%!" print_expr x print_expr x'; *)
  x'
let enc =
  memo_rec1
    (fun enc x ->
      let x' = match head x with
        | A(_,_) -> x
        | B(p,xs) -> b_ p (Hset.map enc xs)
        | Q(p,vs,x) -> futurize p (q_ p vs (enc x))
      in
      (* Format.eprintf "enc:\n   %a\n   %a\n\n%!" print_expr x print_expr x'; *)
      x')

(* WS1S output *)

(* let epsilon = *)
(*   memo_rec1 (fun epsilon x -> *)
(*       match head x with *)
(*       | A(false,a) -> A.epsilon a *)
(*       | A(true,a) -> not (A.epsilon a) *)
(*       | B(false,xs) -> Hset.exists epsilon xs *)
(*       | B(true,xs) -> Hset.for_all epsilon xs *)
(*       | Q(p,vs,x) -> epsilon_m2l (futurize p (q_ p vs (enc x)))) *)
(* in fun x -> Set.is_empty (fv x).s1 && Set.is_empty (fv x).s0 && epsilon x *)
let epsilon_ws1s x =
  if Set.is_empty (fv x).s1 && Set.is_empty (fv x).s0 then
    let x' = enc x in
    (* Format.eprintf "enc:\n   %a\n   %a\n\n%!" print_expr x print_expr x'; *)
    epsilon_m2l x'
  else false

(* normalisation function for WS1S *)
let ws1s_simplify =
  memo_rec1 (fun simp x ->
      if fv x = vs_empty then if epsilon_ws1s x then tt else ff else
        match head x with
        | A(_,_) -> x
        | B(p,xs) -> b_ p (Hset.map simp xs)
        | Q(p,vs,x) -> q_ws1s p vs (simp x))

(* WS1S normalised derivatives *)
let ws1s_lderiv x = Span.map (Bdd.unary m ws1s_simplify) (lderiv x)


(* exported (packed) automata *)
let brzozowski_m2l =
  Automata.SDFA.{
      m; t=lderiv; o=epsilon_m2l;
      norm = generic_norm;
      output_check = generic_output_check epsilon_m2l;
      state_info = print_expr }
let brzozowski_ws1s =
  Automata.SDFA.{
      m; t=lderiv; o=epsilon_ws1s;
      norm = generic_norm;
      output_check = generic_output_check epsilon_ws1s;
      state_info = print_expr }
let brzozowski_ws1s' =
  Automata.SDFA.{
      m; t=ws1s_lderiv; o=epsilon_ws1s;
      norm = ws1s_simplify;
      output_check = generic_output_check epsilon_ws1s;
      state_info = print_expr }
