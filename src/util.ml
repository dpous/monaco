(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** shared types *)

open Common
open Types

(* pretty printing *)
let print_unit f () = Format.pp_print_char f '.'
let print_bool f b = Format.pp_print_char f (if b then '1' else '0')
let print_var = Format.pp_print_string
let print_gstring = print_gstring print_unit print_var

(* raising errors *)
let failwith fmt = Format.kasprintf failwith fmt 
let error i fmt = Format.kasprintf (fun s -> prerr_endline s; exit i) fmt

(* lexing/parsing errors *)
open Lexing
exception ParsingError_ of position*position*string
exception ParsingError of string
                                                          
let parsing_error i j fmt = Format.kasprintf (fun s -> raise (ParsingError_(i,j,s))) fmt
let lexing_error lexbuf fmt = parsing_error (lexeme_start_p lexbuf) (lexeme_end_p lexbuf) fmt

let locate_parsing_errors lexbuf e msg f x = 
  let r i j fmt = 
     Format.kasprintf
       (fun s -> raise (ParsingError s))
       ("%sline %d, characters %d-%d: "^^fmt) msg
       i.pos_lnum (i.pos_cnum - i.pos_bol) (j.pos_cnum - i.pos_bol)
  in
  try f x with
  | ParsingError_(i,j,s) -> r i j "%s" s
  | e' when e=e' -> r (lexeme_start_p lexbuf) (lexeme_end_p lexbuf)
                      "syntax error near token `%s'" (lexeme lexbuf)

let exit_on_parsing_errors f x =
  try f x with ParsingError s -> prerr_endline s; exit 6

let vs_empty =
  {s0 = Set.empty; s1 = Set.empty; s2 = Set.empty}
let vs_union u v = {
    s0 = Set.union u.s0 v.s0;
    s1 = Set.union u.s1 v.s1;
    s2 = Set.union u.s2 v.s2 }
let vs_inter u v = {
    s0 = Set.inter u.s0 v.s0;
    s1 = Set.inter u.s1 v.s1;
    s2 = Set.inter u.s2 v.s2 }
let vs_diff u v = {
    s0 = Set.diff u.s0 v.s0;
    s1 = Set.diff u.s1 v.s1;
    s2 = Set.diff u.s2 v.s2 }
let vs_of_lists l0 l1 l2 =
  {s0 = Set.of_list l0; s1 = Set.of_list l1; s2 = Set.of_list l2}
let vs_singleton k v = match k with
  | K0 -> {vs_empty with s0 = Set.singleton v}
  | K1 -> {vs_empty with s1 = Set.singleton v}
  | K2 -> {vs_empty with s2 = Set.singleton v}
let vs_flatten = List.fold_left vs_union vs_empty
let vs_fold f u x =
  Set.fold (f K0) u.s0
 (Set.fold (f K1) u.s1
 (Set.fold (f K2) u.s2 x))
let vs_mem (k,i) u =
  match k with
  | K0 -> Set.mem i u.s0  
  | K1 -> Set.mem i u.s1  
  | K2 -> Set.mem i u.s2
let vs_conflicts u =
  Set.union (Set.inter u.s0 u.s1)
 (Set.union (Set.inter u.s1 u.s2)
            (Set.inter u.s0 u.s2))
let set_of_varset u = Set.union u.s0 (Set.union u.s1 u.s2)
let print_varset f u = Set.print ~sep:" " print_var f (set_of_varset u)
