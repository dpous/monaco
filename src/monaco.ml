(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Common
open Automata
open Types
open Util

(* generic symbolic algorithm*)
module Symb = Safa.Make(Queues.BFS) 

(* quiet or not, stats or not *)
let quiet = ref !Sys.interactive
let stats = ref false

(* recording traces or not *)
let trace = ref false

(* do we use Hopcroft and Karp's version? *)
let algo = ref `HK

(* which construction to use *)
let construction = ref Formula.brzozowski_ws1s'

(* do we restrict free 1st-order variables? (only for -dfa, -cat, and -rdv) *)
let dontrestrict = ref false
let restrict x = if !dontrestrict then x else Formula.restrict1 x

(* the three considered algorithms *)
let symb_hk ?tracer a x y =
  let x,y = Formula.restrict2 x y in
  match Symb.equiv ?tracer (Bdd.unify_dsf !trace) a x y with
  | None -> None | Some(_,_,w) -> Some([],w)

let symb_nv ?tracer a x y =
  let x,y = Formula.restrict2 x y in
  match Symb.equiv ?tracer (Bdd.unify_naive !trace) a x y with
  | None -> None | Some(_,_,w) -> Some([],w)

let symb_ac ?n a x y =
  let z = Formula.xor x y in
  let z = Formula.restrict1 z in
  let cmp x y = Formula.compare ?n y x in
  match Safa.constant_ac a cmp not z with
  | None -> None | Some(_,w) -> Some([],w)

(* equivalence check *)
let equiv x y =
  let x',y' = Formula.restrict2 x y in
  let a = !construction in
  let tracer = if !trace then (
    Trace.clear();
    let pp = SDFA.trace print_unit print_var print_bool a in
    pp (a.SDFA.norm y'); pp (a.SDFA.norm x');
    Some (fun k x y ->
      let x = Bdd.tag (Bdd.constant a.SDFA.m x) in
      let y = Bdd.tag (Bdd.constant a.SDFA.m y) in
      match k with
	| `CE -> Trace.ce x y
	| `OK -> Trace.ok x y
	| `Skip -> Trace.skip x y
    ) 
  ) else None 
  in
  match !algo with
  | `HK -> symb_hk ?tracer a x y
  | `Nv -> symb_hk ?tracer a x y
  | `AC n -> symb_ac ~n a x y
			
(* full comparison *)
let compare x y = 
  Stats.reset();
  let z = Formula.dsj x y in
  match equiv x y with
    | None -> `E
    | Some w -> Stats.reset(); Trace.save(); match equiv z y with
	| None -> `L w
	| Some w1 -> Stats.reset(); match equiv z x with
	    | None -> `G w
	    | Some w2 -> Trace.restore(); `D (w1,w2)

(* printing the dfa of an expression in DOT format *)
let print_dfa ?labels x =
  Trace.clear();
  let x = restrict x in
  let a = !construction in
  SDFA.trace ?labels print_unit print_var print_bool a x;
  print_endline (Trace.render (!algo=`HK) "\"")

(* printing the size of the dfa of an expression *)
let print_size x =
  let x = restrict x in
  let a = !construction in
  let s,n = SDFA.size a [x] in
  Format.printf "%i states and %i BDD nodes\n" s n

(* print the parsed and normalised version of a given expression *)
let cat x =
  let x = restrict x in
  Format.printf "%a\n%!" Formula.print_expr x

(* display the [n]-th first right derivatives of the expression [x] *)
let rdv n x =
  let x = restrict x in
  let rec loop n x =
    if n>=0 then (
      Format.printf "%a << %a\n%!"
		    print_bool (Formula.epsilon_m2l x) Formula.print_expr x;
      loop (n-1) (Formula.rderiv_0 x)
    )
  in loop n x

(* display an answer *)
let answer fmt = 
  if !quiet then Format.ifprintf Format.std_formatter fmt
  else if !trace then Format.printf ("//"^^fmt)
  else Format.printf fmt

(* check for equivalence of [x] and [y] *)
let run x y =
  let x',y' = Sugar.parse_strings x y in
  let e = match equiv x' y' with
    | None -> 
      answer "%s = %s\n" x y; 0
    | Some g -> 
      answer "%s ≠ %s\n" x y; 
      answer "(counter-example: %a)\n" print_gstring g; 1
  in
  if !stats then Stats.print Format.std_formatter;
  if !trace then Format.printf "%s@." (Trace.render (!algo=`HK) "\"");
  e

(* usage message *)
let usage () =
  Format.printf "This programs checks equivalence or satisfiability WS1S formulas.
See http://perso.ens-lyon.fr/damien.pous/monaco/ for a complete description.

Usage: %s [options] {command}

Options are the following ones:
 -q     : quiet mode
 -s     : print statistics
 -t     : trace the execution of the algorithm, and print it in DOT format
 -hk    : use the symbolic version of Hopcroft and Karp's algorithm (default)
 -ac n  : use the symbolic version of Antichain's algorithm, with agressivity [n]
 -nv    : use the symbolic version of the naive algorithm
 -m2l   : use M2L semantics rather than WS1S
 -nos   : do not simplify closed formula (implies WS1S)
 -raw   : do not add toplevel restrictions (only for commands -cat, -dfa, -rdv)
 -help  : print this usage message

Commands are:
  x        : check whether expression [x] is true
  x y      : check expressions [x] and [y] for equivalence
 -cat x    : display the normalised version of the expression [x]
 -dfa x    : display the DFA computed for expression [x], in DOT format
 -afa x    : display the anonymised DFA computed for expression [x], in DOT format
 -sze x    : display the size of the DFA computed for expression [x]
 -rdv n x  : display the [n]-th first right derivatives of the expression [x]
" Sys.argv.(0) 

let is_option = function
  | "" -> false
  | s when s.[0] = '-' -> true
  | _ -> false
	
let check_empty = function
  | [] -> ()
  | s::_ when is_option s ->
     error 4 "Unexpected option: `%s'\nPlease specify the options before the command" s
  | s::_ -> error 4 "Unexpected argument: `%s'" s

(* processing arguments *)
let rec process_args = function
  | "-q"::l -> quiet := true; process_args l
  | "-s"::l -> stats := true; process_args l
  | "-t"::l -> trace := true; process_args l
  | "-hk"::l -> algo := `HK; process_args l
  | "-ac"::n::l -> algo := `AC (int_of_string n); process_args l
  | "-nv"::l -> algo := `Nv; process_args l
  | "-m2l"::l -> construction := Formula.brzozowski_m2l; process_args l
  | "-nos"::l -> construction := Formula.brzozowski_ws1s; process_args l
  | "-raw"::l -> dontrestrict := true; process_args l
  | "-help"::_ -> usage(); exit 0
  | "-dfa"::x::l -> check_empty l; print_dfa (Sugar.parse_string x); exit 0
  | "-afa"::x::l -> check_empty l; print_dfa ~labels:false (Sugar.parse_string x); exit 0
  | "-sze"::x::l -> check_empty l; print_size (Sugar.parse_string x); exit 0
  | "-cat"::x::l -> check_empty l; cat (Sugar.parse_string x); exit 0
  | "-rdv"::n::x::l -> check_empty l; rdv (int_of_string n) (Sugar.parse_string x); exit 0
  | s::_ when is_option s -> error 4 "Unknown option: `%s'" s
  | _::s::_ when is_option s -> error 4 "Second expression expected rather than `%s'" s
  | [x] -> exit (run x "true")
  | x::y::l -> check_empty l; exit (run x y)
  | [] -> ()				(* required for the web-applet *)

(* main entry point, for standalone program *)
let _ =
  if not !Sys.interactive then 
    exit_on_parsing_errors
      process_args (List.tl (Array.to_list Sys.argv))
