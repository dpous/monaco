(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** High-level interface of the library, main entry point for standalone program *)

open Common
open Types
open Formula


(** which construction do we use *)
val construction: (expr,unit,var,bool) Automata.sdfa ref

(** which algorithm do we use? *)
val algo: [`HK | `Nv | `AC of int] ref

(** do we restrict free 1st-order variables? *)
val dontrestrict: bool ref

(** do we trace unifications in [Format.str_formatter] *)
val trace: bool ref

(** equivalence check, using the above parameters; returns a
    counter-example if any. *)
val equiv: expr -> expr -> gstring option

(** full comparison: [compare x y] returns
    - [`E] if [x] and [y] are equivalent,
    - [`L w] if [x] is strictly contained in [y] (then [w] is a witness for strictness),
    - [`G w] if [y] is strictly contained in [x] (then [w] is a witness for strictness),
    - [`D(w1,w2)] if [x] and [y] are incomparable (then [w1] is a witness against [x<=y], and [w2] is a witness against [y<=x]).
*)
val compare: expr -> expr -> [`E | `L of gstring | `G of gstring | `D of gstring * gstring ]

(** print the expression (after restriction) to stdout *)
val cat: expr -> unit

(** print the dfa of an expression in DOT format to stdout.
    if [labels] false, then label states with identifiers rather 
    than with their content *)
val print_dfa: ?labels:bool -> expr -> unit
