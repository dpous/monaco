(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** shared types *)

open Common

type kind = K0 | K1 | K2
type var = string
type var0 = var
type var1 = var
type var2 = var
type gstring = (unit,var) Common.gstring
type varset = {s0: var0 set; s1: var1 set; s2: var2 set}

type 's mem = ('s,var) Bdd.mem
type 's node = ('s,var) Bdd.node
type 'a span = (unit,'a) Common.span
    
(* intermediate explicit type for formulae *)
type i2 =
  | I_ID2 of var2
  | I_EMPTY
  | I_UNION of i2 * i2
  | I_INTER of i2 * i2
  | I_DIFF of i2 * i2
  | I_PLUS2 of i2 * int
  | I_PCONST of int
  | I_SET of i1 list
 and i1 =
  | I_ID1 of var1
  | I_NUM of int
  | I_MIN of i2
  | I_MAX of i2
  | I_PLUS1 of i1 * int
  | I_PLUS_MOD of i1 * int * i1
type i0 =
  | I_ID0 of var0
  | I_TRUE
  | I_FALSE
  | I_NEG of i0
  | I_CNJ of i0 * i0
  | I_DSJ of i0 * i0
  | I_IMP of i0 * i0
  | I_IFF of i0 * i0
  | I_EQ1 of i1 * i1
  | I_EQ2 of i2 * i2
  | I_LT of i1 * i1
  | I_LE of i1 * i1
  | I_SUB of i2 * i2
  | I_IN of i1 * i2
  | I_ISEMPTY of i2
  | I_EXISTS of kind * string * i0
  | I_FORALL of kind * string * i0

(* AST for mona files *)
type ast_ =
  | A_TRUE
  | A_FALSE
  | A_EMPTY
  | A_NUM of int
  | A_ID of string
  | A_NEG of ast
  | A_MIN of ast
  | A_MAX of ast
  | A_CNJ of ast * ast
  | A_DSJ of ast * ast
  | A_IMP of ast * ast
  | A_IFF of ast * ast
  | A_EQ of ast * ast
  | A_EQ1 of ast * ast
  | A_EQ2 of ast * ast
  | A_NEQ of ast * ast
  | A_NEQ1 of ast * ast
  | A_NEQ2 of ast * ast
  | A_LT of ast * ast
  | A_GT of ast * ast
  | A_LE of ast * ast
  | A_GE of ast * ast
  | A_SUB of ast * ast
  | A_NSUB of ast * ast
  | A_IN of ast * ast
  | A_NIN of ast * ast
  | A_UNION of ast * ast
  | A_INTER of ast * ast
  | A_DIFF of ast * ast
  | A_MULT of ast * ast
  | A_DIV of ast * ast
  | A_PLUS of ast * ast
  | A_MINUS of ast * ast
  | A_PLUS_MOD of ast * ast * ast
  | A_MINUS_MOD of ast * ast * ast
  | A_ISEMPTY of ast
  | A_PCONST of ast
  | A_SET of ast list
  | A_CALL of string * ast list
  | A_EXISTS_ of string list * ast
  | A_FORALL_ of string list * ast
  | A_EXISTS of kind * string * ast * ast
  | A_FORALL of kind * string * ast * ast
  | A_LET of kind * string * ast * ast
 and ast = Lexing.position * Lexing.position * ast_
               
type decl_ =
| D_FORMULA of ast
| D_CONST of string * ast
| D_DFTW of kind * string * ast
| D_ASSERT of ast
| D_VAR of kind * (string * ast) list
| D_MACRO of string * (kind * string) list * ast
 and decl = Lexing.position * Lexing.position * decl_       

