(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Common
open Types
open Util
                
(* free variables of the intermediate representation *)
let rec fv2 = function
  | I_ID2 x -> Set.singleton x
  | I_UNION(e,f) | I_INTER(e,f) | I_DIFF(e,f) -> Set.union (fv2 e) (fv2 f)
  | I_PLUS2(e,_) -> fv2 e
  | I_SET e -> List.fold_left (fun acc e -> Set.union acc (fv1 e)) Set.empty e
  | I_EMPTY | I_PCONST _ -> Set.empty
and fv1 = function               
  | I_ID1 x -> Set.singleton x
  | I_MIN e | I_MAX e -> fv2 e
  | I_PLUS1(e,_) -> fv1 e
  | I_PLUS_MOD(e,_,f) -> Set.union (fv1 e) (fv1 f)
  | I_NUM _ -> Set.empty
let rec fv0 = function
  | I_ID0 x -> Set.singleton x
  | I_NEG e -> fv0 e
  | I_CNJ(e,f) | I_DSJ(e,f) | I_IMP(e,f) | I_IFF(e,f) -> Set.union (fv0 e) (fv0 f)
  | I_EQ1(e,f) | I_LT(e,f)  | I_LE(e,f) -> Set.union (fv1 e) (fv1 f)
  | I_EQ2(e,f) | I_SUB(e,f) -> Set.union (fv2 e) (fv2 f)
  | I_IN(e,f) -> Set.union (fv1 e) (fv2 f)
  | I_ISEMPTY e -> fv2 e
  | I_EXISTS(_,x,e) | I_FORALL(_,x,e) -> Set.rem x (fv0 e)
  | I_TRUE | I_FALSE -> Set.empty


let vidx k x = match k with
  | K0 -> `V0 (I_ID0 x)
  | K1 -> `V1 (I_ID1 x)
  | K2 -> `V2 (I_ID2 x)


(* extend the substitution l to avoid capture on x *)
let freshen k x l =
  let fv = List.fold_left
             (fun fv (_,v) -> match v with
                               | `V0 e -> Set.union (fv0 e) fv
                               | `V1 e -> Set.union (fv1 e) fv
                               | `V2 e -> Set.union (fv2 e) fv
             ) (Set.of_list (List.map fst l)) l
  in
  let rec next n =
    let x' = x^string_of_int n in
    if Set.mem x' fv then next (n+1)
    else x'
  in
  if Set.mem x fv then
    let x' = next 0 in
    x', (x,vidx k x')::l
  else x,l
  
(* TOTHINK: are the commented optimisations below safe? *)
let i_plus2 x n = match x with
  (* | I_PLUS2(x,m) -> I_PLUS2(x,n+m) *)
  | _ -> I_PLUS2(x,n)
let i_plus1 x n = match x with
  | I_NUM m -> I_NUM(n+m)
  (* | I_PLUS1(x,m) -> I_PLUS1(x,n+m) *)
  | _ -> I_PLUS1(x,n)
let i_plus_mod x n y = match x with
  (* | I_NUM m -> I_NUM(n+m) ?? *)
  (* | I_PLUS1(x,m) -> I_PLUS_MOD(x,n+m,y) *)
  | _ -> I_PLUS_MOD(x,n,y)

(* capture avoiding substitution *)
let rec subst2 l = function
  | I_ID2 s as x ->
     (try match List.assoc s l with `V2 y -> y | _ -> assert false with Not_found -> x)
  | I_UNION(e,f) -> I_UNION(subst2 l e, subst2 l f)
  | I_INTER(e,f) -> I_INTER(subst2 l e, subst2 l f)
  | I_DIFF(e,f) -> I_DIFF(subst2 l e, subst2 l f)
  | I_PLUS2(e,n) -> i_plus2 (subst2 l e) n
  | (I_EMPTY | I_PCONST _) as x -> x
  | I_SET(e) -> I_SET(List.map (subst1 l) e)
and subst1 l = function
  | I_ID1 s as x ->
     (try match List.assoc s l with `V1 y -> y | _ -> assert false with Not_found -> x)
  | I_MIN e -> I_MIN (subst2 l e)
  | I_MAX e -> I_MAX (subst2 l e)
  | I_PLUS1(e,n) -> i_plus1 (subst1 l e) n
  | I_PLUS_MOD(e,n,f) -> i_plus_mod (subst1 l e) n (subst1 l f)
  | I_NUM _ as x -> x
let rec subst0 l = function
  | I_ID0 s as x ->
     (try match List.assoc s l with `V0 y -> y | _ -> assert false with Not_found -> x)
  | (I_TRUE | I_FALSE) as x -> x
  | I_NEG x -> I_NEG (subst0 l x)
  | I_CNJ(e,f) -> I_CNJ(subst0 l e, subst0 l f)
  | I_DSJ(e,f) -> I_DSJ(subst0 l e, subst0 l f)
  | I_IMP(e,f) -> I_IMP(subst0 l e, subst0 l f)
  | I_IFF(e,f) -> I_IFF(subst0 l e, subst0 l f)
  | I_EQ1(e,f) -> I_EQ1(subst1 l e, subst1 l f)
  | I_EQ2(e,f) -> I_EQ2(subst2 l e, subst2 l f)
  | I_LT(e,f) -> I_LT(subst1 l e, subst1 l f)
  | I_LE(e,f) -> I_LE(subst1 l e, subst1 l f)
  | I_SUB(e,f) -> I_SUB(subst2 l e, subst2 l f)
  | I_IN(e,f) -> I_IN(subst1 l e, subst2 l f)
  | I_ISEMPTY e -> I_ISEMPTY(subst2 l e)
  | I_EXISTS(k,x,e) ->
     let l = List.filter (fun (y,_) -> x<>y) l in
     let x,l = freshen k x l in
     I_EXISTS(k,x,subst0 l e)
  | I_FORALL(k,x,e) -> 
     let l = List.filter (fun (y,_) -> x<>y) l in
     let x,l = freshen k x l in
     I_FORALL(k,x,subst0 l e)
