(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** Syntactic sugar *)

(** from the wild AST to the intermediate language  *)
val i0_of_ast: ?strict:bool -> ?fv:Types.varset -> Types.ast -> Types.i0

(** from the intermediate language to our efficient representation of formulas *)
val expr_of_i0: Types.i0 -> Formula.expr

(** parse a MONA file into an expression *)
val parse_monafile: string -> Formula.expr
                                
(** parse a string into an expression
   the flag [file] allows or disallows the possibility to interpret the
   string as the name of a MONA files 
   the [msg] argument is for error reporting
   [fv] can be used to declare constraints on free variables *)
val parse_string: ?file:bool -> ?msg:string -> ?fv:Types.varset -> string -> Formula.expr

(** parse two strings and returns a pair of formulas whose shared free 
   variables are assigne the same kind
   the flag [file] allows or disallows the possibility to interpret the
   strings as names of MONA files  *)
val parse_strings: ?file:bool -> string -> string -> Formula.expr * Formula.expr

