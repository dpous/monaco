/*******************************************************************
 *     This is part of MonaCo, it is distributed under the         *
 *  terms of the GNU Lesser General Public License version 3       *
 *           (see file LICENSE for more details)                   *
 *                                                                 *
 *  Copyright 2014-2016:                                           *
 *  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *
 *  Dmitriy Traytel. (ETH Zürich)                                  *
 *******************************************************************/

%{
open Types
let macro i j n l y =
  let rec d k = function
  | [] -> []
  | (None,x)::q -> (k,x)::d k q
  | (Some k,x)::q -> (k,x)::d k q
  in match l with
  | [] -> i,j,D_MACRO(n,[],y)
  | (None,_)::_ -> Util.parsing_error i j "unspecified argument kind"
  | (Some k,x)::q -> i,j,D_MACRO(n,(k,x)::d k q,y)
let a_exists i j k = List.fold_right (fun (x,y) acc -> i,j,A_EXISTS(k,x,y,acc))
let a_forall i j k = List.fold_right (fun (x,y) acc -> i,j,A_FORALL(k,x,y,acc))
let a_let i j k = List.fold_right (fun (x,y) acc -> i,j,A_LET(k,x,y,acc))
%}

%token <string> ID
%token <int> NUM
%token <Types.kind> DFTW
%token <Types.kind> LET
%token <Types.kind> VAR
%token <Types.kind> E
%token <Types.kind> A
%token <[`WS1S|`M2L]> HDR
%token LPAR RPAR LBRK RBRK FALSE TRUE NEG CNJ DSJ IMP IFF EOF WHERE ASSERT
%token LT GT IN NIN SUB NSUB COMMA COLON SEMI LE GE EQ EQ1 EQ2 NEQ NEQ1 NEQ2 EMPTY UNION
%token PLUS MINUS MULT DIV BACKSLASH PERCENT MIN MAX CONST PCONST MACRO INTER A_ E_

%nonassoc COLON
%right IFF
%right IMP
%left DSJ
%left CNJ
%nonassoc NEG
%nonassoc IN NIN SUB NSUB
%nonassoc EQ EQ1 EQ2 NEQ NEQ1 NEQ2 GT GE LT LE
%nonassoc MAX MIN
%left UNION
%left INTER
%left BACKSLASH
%left PLUS MINUS
%left MULT DIV PERCENT

%type <[`WS1S|`M2L] * Types.decl list> main
%type <Types.ast> expr
%start main expr

%%
main:
| header decls EOF { ($1, $2) }

expr:
| e EOF { $1 }

header:
| { `WS1S }
| HDR SEMI { $1 }

decls:
| { [] }
| decl SEMI decls { $1 :: $3 }

decl:
| e { $startpos,$endpos,D_FORMULA $1 }
| CONST ID EQ e { $startpos($1),$endpos($1),D_CONST($2,$4) }
| DFTW LPAR ID RPAR EQ e { $startpos($1),$endpos($1),D_DFTW($1,$3,$6) }
| VAR separated_nonempty_list(COMMA,varwhere) { $startpos($1),$endpos($1),D_VAR($1,$2) }
| ASSERT e { $startpos($1),$endpos($1),D_ASSERT $2 }
| MACRO ID EQ e { $startpos($1),$endpos($2),D_MACRO($2,[],$4) }
| MACRO ID LPAR separated_list(COMMA,ovar) RPAR EQ e { macro $startpos($1) $endpos($2) $2 $4 $7 }

ovar:
| ID { None,$1 }
| VAR ID { Some $1,$2 }

varwhere:
| ID { $1,($endpos,$endpos,A_TRUE) }
| ID WHERE e { $1,$3 }

binding:
| ID EQ e { $1,$3 }

e:
| LPAR e RPAR          { $2 }
| TRUE                 { $startpos,$endpos,A_TRUE }
| FALSE                { $startpos,$endpos,A_FALSE }
| EMPTY		       { $startpos,$endpos,A_EMPTY }
| NUM                  { $startpos,$endpos,A_NUM $1 }
| ID                   { $startpos,$endpos,A_ID $1} 
| NEG e                { $startpos,$endpos,A_NEG $2 }
| MIN e		       { $startpos,$endpos,A_MIN $2 }
| MAX e                { $startpos,$endpos,A_MAX $2 }
| e EQ e               { $startpos,$endpos,A_EQ($1,$3) }
| e EQ1 e              { $startpos,$endpos,A_EQ1($1,$3) }
| e EQ2 e              { $startpos,$endpos,A_EQ2($1,$3) }
| e LT e               { $startpos,$endpos,A_LT($1,$3) }
| e GT e               { $startpos,$endpos,A_GT($1,$3) }
| e LE e               { $startpos,$endpos,A_LE($1,$3) }
| e GE e               { $startpos,$endpos,A_GE($1,$3) }
| e IN e               { $startpos,$endpos,A_IN($1,$3) }
| e CNJ e              { $startpos,$endpos,A_CNJ($1,$3) }
| e DSJ e              { $startpos,$endpos,A_DSJ($1,$3) }
| e IMP e              { $startpos,$endpos,A_IMP($1,$3) }
| e IFF e              { $startpos,$endpos,A_IFF($1,$3) }
| e NEQ e              { $startpos,$endpos,A_NEQ($1,$3) }
| e NEQ1 e             { $startpos,$endpos,A_NEQ1($1,$3) }
| e NEQ2 e             { $startpos,$endpos,A_NEQ2($1,$3) }
| e SUB e              { $startpos,$endpos,A_SUB($1,$3) }
| e NIN e              { $startpos,$endpos,A_NIN($1,$3) }
| e NSUB e             { $startpos,$endpos,A_NSUB($1,$3) }
| e UNION e	       { $startpos,$endpos,A_UNION($1,$3) }
| e INTER e	       { $startpos,$endpos,A_INTER($1,$3) }
| e BACKSLASH e	       { $startpos,$endpos,A_DIFF($1,$3) }
| e MULT e             { $startpos,$endpos,A_MULT($1,$3) }
| e DIV e              { $startpos,$endpos,A_DIV($1,$3) }
| e PLUS e             { $startpos,$endpos,A_PLUS($1,$3) }
| e MINUS e            { $startpos,$endpos,A_MINUS($1,$3) }
| e PLUS e PERCENT e   { $startpos,$endpos,A_PLUS_MOD($1,$3,$5) }
| e MINUS e PERCENT e  { $startpos,$endpos,A_MINUS_MOD($1,$3,$5) }
| EMPTY LPAR e RPAR    { $startpos($1),$endpos($1),A_ISEMPTY $3 }
| PCONST LPAR e RPAR   { $startpos($1),$endpos($1),A_PCONST $3 }
| LBRK separated_list(COMMA,e) RBRK { $startpos,$endpos,A_SET $2 }
| ID LPAR separated_nonempty_list(COMMA,e) RPAR { $startpos,$endpos,A_CALL($1,$3) }
| E_ nonempty_list(ID) COLON e { $startpos($1),$endpos($1),A_EXISTS_($2,$4) }
| A_ nonempty_list(ID) COLON e { $startpos($1),$endpos($1),A_FORALL_($2,$4) }
| E separated_nonempty_list(COMMA,varwhere) COLON e { a_exists $startpos($1) $endpos($1) $1 $2 $4 }
| A separated_nonempty_list(COMMA,varwhere) COLON e { a_forall $startpos($1) $endpos($1) $1 $2 $4 }
| LET separated_nonempty_list(COMMA,binding) IN e { a_let $startpos($1) $endpos($1) $1 $2 $4 }

