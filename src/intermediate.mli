(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Common
open Types

val fv0: i0 -> var0 set
val fv1: i1 -> var1 set
val fv2: i2 -> var2 set
                     
val vidx: kind -> var -> [> `V0 of i0 | `V1 of i1 | `V2 of i2 ]
val freshen: kind -> var -> (var * [ `V0 of i0 | `V1 of i1 | `V2 of i2 ]) list ->
             var * (var * [ `V0 of i0 | `V1 of i1 | `V2 of i2 ]) list

val i_plus2 : i2 -> int -> i2
val i_plus1 : i1 -> int -> i1
val i_plus_mod : i1 -> int -> i1 -> i1
                                      
val subst0: (var * [ `V0 of i0 | `V1 of i1 | `V2 of i2 ]) list -> i0 -> i0
val subst1: (var * [> `V1 of i1 | `V2 of i2 ]) list -> i1 -> i1
val subst2: (var * [> `V1 of i1 | `V2 of i2 ]) list -> i2 -> i2
