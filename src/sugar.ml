(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Common
open Types
open Intermediate
open Formula
open Util

       
(** * from [Types.i0] to [Formula.expr] *)
       
let v n = "@"^string_of_int n

let rec eq_xe n x = function
  | I_ID1 y -> eqn x y 0
  | I_NUM n -> eqk x n
  | I_PLUS1(e,0) -> eq_xe n x e
  | I_PLUS1(e,m) ->
     let y = v n in ex1 y (cnj (eq_xe (n+1) y e) (eqn x y m))
  | I_MIN e ->
     let y = v n in cnj (mem_xe n x e) (fa1 y (imp (mem_xe (n+1) y e) (neg (lt y x))))
  | I_MAX e -> 
     let y = v n in cnj (mem_xe n x e) (fa1 y (imp (mem_xe (n+1) y e) (neg (lt x y))))
  | I_PLUS_MOD(_,_,_) -> failwith "[Sugar.eq_xe]: modulo computations not yet supported"

and red n k = function
  | I_ID1 x -> k n x
  | e -> let x = v n in ex1 x (cnj (eq_xe (n+1) x e) (k (n+1) x))
                               
and mem_xe n x = function
  | I_ID2 y -> mem x y
  | I_EMPTY -> ff
  | I_UNION(e,f) -> dsj (mem_xe n x e) (mem_xe n x f)
  | I_INTER(e,f) -> cnj (mem_xe n x e) (mem_xe n x f)
  | I_DIFF(e,f) -> cnj (mem_xe n x e) (neg (mem_xe n x f))
  | I_PLUS2(e,0) -> mem_xe n x e
  | I_PLUS2(e,m) ->
     let y = v n in
     ex1 y (cnj (mem_xe (n+1) y e) (eqn x y m))
  | I_PCONST(n) -> failwith "pconst not yet supported"
  | I_SET(l) -> List.fold_left (fun acc e -> dsj acc (eq_xe n x e)) ff l

let mem_ef n e f =
  red n (fun n x -> mem_xe n x f) e

let lt_ef n e f =
  red n (fun n x -> red n (fun _ y -> lt x y) f) e

let eq_ef n e f =
  red n (fun n x -> eq_xe n x f) e

let expr_of_i0 =
  let rec translate n = function
    | I_ID0 x -> v0 x
    | I_TRUE -> tt
    | I_FALSE -> ff
    | I_NEG e -> neg (translate n e)
    | I_CNJ(e,f) -> cnj (translate n e) (translate n f)
    | I_DSJ(e,f) -> dsj (translate n e) (translate n f)
    | I_IMP(e,f) -> imp (translate n e) (translate n f)
    | I_IFF(e,f) -> iff (translate n e) (translate n f)
    | I_EQ1(e,f) -> eq_ef n e f
    | I_EQ2(e,f) ->
       let x = v n in
       fa1 x (iff (mem_xe (n+1) x e) (mem_xe (n+1) x f))
    (* | I_SUB(I_ID2 x,I_ID2 y) -> subset x y *)
    | I_SUB(e,f) ->
       let x = v n in
       fa1 x (imp (mem_xe (n+1) x e) (mem_xe (n+1) x f))
    (* | I_ISEMPTY(I_ID2 x) -> empty x *)
    | I_ISEMPTY e ->
       let x = v n in
       fa1 x (neg (mem_xe (n+1) x e))
    | I_LT(e,f) -> lt_ef n e f
    | I_LE(e,f) -> neg (lt_ef n f e)
    | I_IN(e,f) -> mem_ef n e f
    | I_EXISTS(k,x,e) -> ex_r (vs_singleton k x) (translate n e)
    | I_FORALL(k,x,e) -> fa_r (vs_singleton k x) (translate n e)
  in translate 0


(** * from MONA ast to [Types.i0] *)

let i0_of_content ?(strict=true) (hdr,decls) =
  if hdr <> `WS1S then error 5 "only WS1S is supported for now";
  
  let d = Hashtbl.create 3 in
  let dftw_fv = ref Set.empty in
  let dftw i j k x p =
    if Hashtbl.mem d k then parsing_error i j "defaultwhere should be used at most once";
    Hashtbl.add d k (x,p);
    dftw_fv := Set.union !dftw_fv (Set.rem x (fv0 p))
  in
  let dftw_get k y =
    try let x,p = Hashtbl.find d k in
        subst0 [x,vidx k y] p
    with Not_found -> I_TRUE
  in
  let check_hiding i j x =
    if Set.mem x !dftw_fv then
      parsing_error i j "hiding free variable (%s) from a defaultwhere clause is not yet supported" x
  in

  let t = Hashtbl.create 51 in
  let get i j x =
    try Hashtbl.find t x
    with Not_found ->
      if strict then parsing_error i j "unbound variable: %s" x
      else `U
  in
  let refine x k = Hashtbl.replace t x (`D k) in
  let bind i j x v = check_hiding i j x; Hashtbl.add t x v in
  let unset x = Hashtbl.remove t x in
  let rec num (i,j,e) = match e with
    | A_NUM n -> n
    | A_ID x ->
       (match get i j x with
        | `N n -> n
        | _ -> parsing_error i j "non numerical variable %s used as a numerical constant" x)
    | A_PLUS(e,f) -> num e + num f
    | A_MINUS(e,f) -> num e - num f
    | A_MULT(e,f) -> num e * num f
    | A_DIV(e,f) -> num e / num f
    | A_PLUS_MOD(e,f,g) -> num e + num f mod num g
    | A_MINUS_MOD(e,f,g) -> num e - num f mod num g
    | _ -> parsing_error i j "invalid arithmetic constant"
  in let comm e f k = try let n = num f in k e n with ParsingError_ _ -> k f (num e)  
  in let rec e2 (i,j,e) = match e with
    | A_EMPTY -> I_EMPTY
    | A_ID x -> 
       (match get i j x with
        | `D K2 -> I_ID2 x
        | `U -> refine x K2; I_ID2 x
        | `V2 v -> v
        | _ -> parsing_error i j "variable %s cannot be used as a set expression" x)
    | A_SET l -> I_SET(List.map e1 l)
    | A_UNION(e,f) -> I_UNION(e2 e, e2 f)
    | A_INTER(e,f) -> I_INTER(e2 e, e2 f)
    | A_DIFF(e,f) -> I_DIFF(e2 e, e2 f)
    | A_PCONST e -> I_PCONST(num e)
    | A_PLUS(e,f) -> comm e f (fun e n -> i_plus2(e2 e) n)
    | A_MINUS(e,f) -> i_plus2(e2 e) (-num f)
    | _ -> parsing_error i j "invalid set expression"
  and e1 (i,j,e) = match e with
    | A_NUM n -> I_NUM n
    | A_ID x -> 
       (match get i j x with
        | `N n -> I_NUM n
        | `D K1 -> I_ID1 x
        | `U -> refine x K1; I_ID1 x
        | `V1 v -> v
        | _ -> parsing_error i j "variable %s cannot be used as a numerical expression" x)
    | A_PLUS(e,f) -> comm e f (fun e n -> i_plus1(e1 e) n)
    | A_MINUS(e,f) -> i_plus1 (e1 e)(-num f)
    | A_PLUS_MOD(e,f,g) -> comm e f (fun e n -> i_plus_mod(e1 e) n (e1 g))
    | A_MINUS_MOD(e,f,g) -> i_plus_mod(e1 e) (-num f) (e1 g)
    | A_MAX e -> I_MAX(e2 e)
    | A_MIN e -> I_MIN(e2 e)
    | _ -> parsing_error i j "invalid arithmetical expression"
  in let rec e0 (i,j,e) = match e with
    | A_TRUE -> I_TRUE
    | A_FALSE -> I_FALSE
    | A_ID x ->
       (match get i j x with
        | `D K0 -> I_ID0 x
        | `U -> refine x K0; I_ID0 x
        | `V0 e | `P([],e) -> e
        | _ -> parsing_error i j "invalid variable in a formula: %s" x
       )
    | A_NEG e -> I_NEG(e0 e)
    | A_CNJ(e,f) -> I_CNJ(e0 e, e0 f)
    | A_DSJ(e,f) -> I_DSJ(e0 e, e0 f)
    | A_IMP(e,f) -> I_IMP(e0 e, e0 f)
    | A_IFF(e,f) -> I_IFF(e0 e, e0 f)
    | A_EQ1(e,f) -> I_EQ1(e1 e,e1 f)
    | A_EQ2(e,f) -> I_EQ2(e2 e,e2 f)
    | A_EQ(e,f) -> (try I_EQ1(e1 e,e1 f) with ParsingError_ _ -> I_EQ2(e2 e, e2 f))
    | A_NEQ(e,f) -> I_NEG(e0(i,j,A_EQ(e,f)))
    | A_NEQ1(e,f) -> I_NEG(e0(i,j,A_EQ1(e,f)))
    | A_NEQ2(e,f) -> I_NEG(e0(i,j,A_EQ2(e,f)))
    | A_LT(e,f) | A_GT(f,e) -> I_LT(e1 e, e1 f)
    | A_LE(e,f) | A_GE(f,e) -> I_LE(e1 e, e1 f)
    | A_SUB(e,f) -> I_SUB(e2 e, e2 f)
    | A_NSUB(e,f) -> I_NEG(I_SUB(e2 e, e2 f))
    | A_IN(e,f) -> I_IN(e1 e, e2 f)
    | A_NIN(e,f) -> I_NEG(I_IN (e1 e, e2 f))
    | A_ISEMPTY e -> I_ISEMPTY(e2 e)
    | A_CALL(f,xs) ->
       (match get i j f with
        | `P(args,body) -> subst0 (es i j f (args,xs)) body
        | _ -> parsing_error i j "undefined predicate: %s" f
       )       
    | A_EXISTS_(l,e) ->
       List.iter (fun x -> bind i j x `U) l;
       let e = e0 e in
       List.fold_right (fun x e ->
           match get i j x with
           | `D k -> unset x; I_EXISTS(k,x,e)
           | `U -> unset x; e
           | _ -> assert false
         ) l e
    | A_FORALL_(l,e) ->
       List.iter (fun x -> bind i j x `U) l;
       let e = e0 e in
       List.fold_right (fun x e ->
           match get i j x with
           | `D k -> unset x; I_FORALL(k,x,e)
           | `U -> unset x; e
           | _ -> assert false
         ) l e
    | A_EXISTS(k,x,e,f) ->
       let r = dftw_get k x in
       bind i j x (`D k);
       let e = e0 e in
       let f = e0 f in
       unset x; 
       I_EXISTS(k,x,I_CNJ(I_CNJ(r,e),f))
    | A_FORALL(k,x,e,f) ->
       let r = dftw_get k x in
       bind i j x (`D k);
       let e = e0 e in
       let f = e0 f in
       unset x; 
       I_FORALL(k,x,I_IMP(I_CNJ(r,e),f))
    | A_LET(k,x,e,f) ->
       set i j k x e;
       let f = e0 f in
       unset x;
       f
    | _ -> parsing_error i j "invalid formula"
  and set i j k x e = match k with
    | K0 -> let e = e0 e in bind i j x (`V0 e)
    | K1 -> let e = e1 e in bind i j x (`V1 e)
    | K2 -> let e = e2 e in bind i j x (`V2 e)
  and es i j f = function
    | [],[] -> []
    | (K0,x)::args,(e::q) -> (x,`V0 (e0 e))::es i j f (args,q)
    | (K1,x)::args,(e::q) -> (x,`V1 (e1 e))::es i j f (args,q)
    | (K2,x)::args,(e::q) -> (x,`V2 (e2 e))::es i j f (args,q)
    | _ -> parsing_error i j "macro %s called with the wrong number of arguments" f
  in
  let rec read = function
    | [] -> I_TRUE
    | (_,_,D_FORMULA e) :: q -> I_CNJ(e0 e, read q)  
    | (_,_,D_ASSERT e) :: q -> I_IMP(e0 e, read q)  
    | (i,j,D_CONST(x,n)) :: q ->
       bind i j x (`N (num n)); read q
    | (i,j,D_DFTW(k,x,p)) :: q ->
       bind i j x (`D k);
       let p = e0 p in
       unset x;
       dftw i j k x p;
       read q
    | (i,j,D_VAR(k,l)) :: q ->
       let p = List.fold_left (fun c (x,e) ->
                   let r = dftw_get k x in
                   bind i j x (`D k);
                   I_CNJ(I_CNJ(r,e0 e),c)
                 ) I_TRUE l in
       I_IMP(p,read q)
    | (i,j,D_MACRO(x,l,e)) :: q ->
       List.iter (fun (k,x) -> bind i j x (`D k)) l;
       let body = e0 e in
       List.iter (fun (k,x) -> unset x) l;       
       bind i j x (`P(l,body)); read q
  in
  read decls

let i0_of_ast ?strict ?(fv=vs_empty) ((i,j,_) as e) =
  let g k s = i,j,D_VAR(k,Set.fold (fun x acc -> (x,(i,j,A_TRUE))::acc) s []) in
  i0_of_content ?strict (`WS1S,[g K0 fv.s0; g K1 fv.s1; g K2 fv.s2; i,j,D_FORMULA e])


(** * parsing MONA files *)

let read_monalexbuf l =
  let e = Parser.main (Lexer.token ~mona:true) l in
  i0_of_content e
let parse_monafile f =
  let i = open_in f in
  let l = Lexing.from_channel i in
  let e = locate_parsing_errors l Parser.Error ("File \""^f^"\", ") read_monalexbuf l in
  close_in i;
  expr_of_i0 e

(** * parsing strings (or MONA files, indirectly) *)

let read_lexbuf ?fv l =
  let e = Parser.expr Lexer.token l in
  i0_of_ast ~strict:false ?fv e
let dotmonafile s =
  let n = String.length s in
  try String.sub s (n-5) 5 = ".mona"
  with Invalid_argument _ -> false
let parse_string ?(file=true) ?(msg="") ?fv s =
  if file && dotmonafile s then parse_monafile s
  else
    let l = Lexing.from_string s in
    let e = locate_parsing_errors l Parser.Error msg (read_lexbuf ?fv) l in
    expr_of_i0 e
let parse_strings ?file x y =
  let x = parse_string ~msg:"first expression: " x in
  let fvx = fv x in
  let y = parse_string ~msg:"second expression: " ~fv:fvx y in
  let fvy = fv y in
  let c = vs_conflicts (vs_union fvx fvy) in
  if not (Set.is_empty c) then
    error 6 "variables %a are used inconsistently in the two expressions"
          (Set.print print_var) c;
  x,y

