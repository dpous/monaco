(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** a few sanity checks *)

open Monaco

let cnv = function `E -> `E | `L _ -> `L | `G _ -> `G | `D _ -> `D

let check x y =
  let x,y = Sugar.parse_strings x y in
  cnv (compare x y)

let main () =
  assert(`L=check "false" "true");
  assert(`E=check "x<y | y<=x" "true");
  assert(`E=check "x<y & x>y" "false");
  assert(`L=check "x<y | x>y" "true");
  assert(`D=check "x<y" "y<x");
  assert(`L=check "x<y" "x<=y");
  assert(`D=check "x<y" "y<=x");
  assert(`E=check "x<y & y<=x" "false");
  assert(`E=check "x<y & y<x" "false");
  assert(`E=check "x<y => y<z => x<z" "true");
  assert(`E=check "x<y => y<=z => x<z" "true");
  assert(`E=check "x<=y => y<z => x<z" "true");
  assert(`E=check "x<=y => y<=z => x<=z" "true");
  assert(`D=check "ex x: x<y" "ex y: y<x");
  assert(`L=check "ex x: x<y" "ex x: x<=y");
  assert(`D=check "ex x: x<y" "ex y: y<x");
  assert(`E=check "ex y: x<y" "true");
  assert(`L=check "ex x: x<y" "ex x: y<x");
  assert(`L=check "all x: x<y" "all x: y<=x");
  assert(`L=check "all x: x<y" "ex y: y<x");
  assert(`E=check "ex Y: all y: x<y => y in Y" "false");
  assert(`E=check "ex Y: all y: y<x => y in Y" "true");
  assert(`E=check "ex x: all y: y in X -> y<=x" "true");
  assert(`E=check "ex X z: z in X & all x y: y in X -> x in X -> y<=x" "true");
  assert(`E=check "ex X z: z in X & all x y: y in X -> x in X -> y<x" "false");
  assert(`E=check "(all x: ~ x in X | ~ x in Y) & ex x: x in X & x in Y" "false");
  assert(`E=check "all X: (all x: ~ x in X | ~ x in Y) -> all x: ~ x in X" "false");
  assert(`E=check "(all x: x<=t => x in X1) => (all x: x<=t => x in X1)" "true");
  assert(`E=check "(all x: x<=t => x in X1 => x in X2) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2)" "true");
  assert(`E=check "(all x: x<=t => x in X1 => x in X2 => x in X3) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2) => (all x: x<=t => x in X3)" "true");
  assert(`E=check "all t: (all x: x<=t => x in X1) => (all x: x<=t => x in X1)" "true");
  assert(`E=check "all t: (all x: x<=t => x in X1 => x in X2) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2)" "true");
  assert(`E=check "all t: (all x: x<=t => x in X1 => x in X2 => x in X3) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2) => (all x: x<=t => x in X3)" "true");

  assert(`E=check "ex x: x < x" "false");
  assert(`E=check "all x: x < x" "false");
  assert(`E=check "ex x: ex y: y < x" "true");
  assert(`E=check "ex x: all y: y < x" "false");
  assert(`E=check "all x: ex y: y < x" "false");
  assert(`E=check "all x: all y: y < x" "false");
  assert(`E=check "ex x: ex y: x < y" "true");
  assert(`E=check "ex x: all y: x < y" "false");
  assert(`E=check "all x: ex y: x < y" "true");
  assert(`E=check "all x: all y: x < y" "false");

  assert(`E=check "ex X: ex y: y in X" "true");
  assert(`E=check "ex X: all y: y in X" "false");
  assert(`E=check "all X: ex y: y in X" "false");
  assert(`E=check "all X: all y: y in X" "false");
  assert(`E=check "ex x: ex Y: x in Y" "true");
  assert(`E=check "ex x: all Y: x in Y" "false");
  assert(`E=check "all x: ex Y: x in Y" "true");
  assert(`E=check "all x: all Y: x in Y" "false");

  assert(`E=check "ex x: ex y: x < y & x < y" "true");
  assert(`E=check "ex x: ex y: y < x & y < x" "true");
  assert(`E=check "all x: ex y: x < y & x < y" "true");
  assert(`E=check "ex x: ex y: ~ (~(x < y)) | (~(x < y))" "true");
  assert(`E=check "ex x: ex y: ex z: z < y & (y < x | x < z)" "true");
  assert(`E=check "all x: ex y: x < y | y < x" "true");

  assert(`E=check "ex x: ex y: x < y & y < x" "false");
  assert(`E=check "ex x: ex y: ~ (~(x < y) | ~(y < x))" "false");
  assert(`E=check "ex x: ex y: ex z: z < y & y < x & x < z" "false");
  assert(`E=check "all x: all y: x < y | y < x" "false");
  assert(`E=check "all x: all y: x < y & y < x" "false");

  assert(`E=check "X union Y = Y union X union empty" "true");
  assert(`L=check "X union (Y inter Z) = (X union Z) inter (Y union Z)" "true");
  assert(`E=check "X union (Y inter Z) = (X union Y) inter (X union Z)" "true");
  assert(`E=check "X inter (Y union Z) = (X inter Y) union (X inter Z)" "true");

  assert(`E=check "p = 1000000000 & p = 100" "false");
  assert(`E=check "ex p: p = 10" "true");
  assert(`E=check "ex p: p <= 10" "true");
  assert(`E=check "ex p: p >= 10" "true");
  assert(`E=check "ex p: p ~= 10" "true");
  assert(`E=check "ex p: p < 10" "true");
  assert(`E=check "ex p: p > 10" "true");
  assert(`E=check "ex p: p < 10 & p >= 9" "true");
  assert(`E=check "p < 10 | p >= 10" "true");
  assert(`E=check "all p: p < 10 | p >= 10" "true");
  assert(`E=check "(all p: 5 < p => p < 20 => p in X) => 9 in X" "true");
  assert(`E=check "all p: 5 <= p => p < 7 => p in X" "5 in X & 6 in X");
  assert(`E=check "ex q: p = q + 1" "p > 0");
  assert(`E=check "ex q: p = q + 100" "p > 99");
  assert(`E=check "all p: p > 0 => ex q: p - 1 = q" "true");
  assert(`E=check "all p: p > 9 => ex q: p - 10 = q" "true");
  assert(`E=check "all p: ex q: p = q" "true");
  assert(`L=check "p = q - 10" "p < q");
  assert(`E=check "p = 10 + q" "p = q + 10");
  assert(`E=check "p - 10 = q" "p = q + 10");
  assert(`E=check "10 + q = p" "p = q + 10");
  assert(`E=check "q + 10 = p" "p = q + 10");
  assert(`E=check "q = p - 10" "p = q + 10");
  assert(`E=check "p = p + 1" "false");
  assert(`E=check "p = q + 1" "q < p & ~(ex r: q < r & r < p)");
  assert(`E=check "p = q & p = q + 1" "false");
  assert(`E=check "p = q & q = r + 1 & p = r" "false");
  assert(`E=check "X = {}" "all x: ~(x in X)");
  (* assert(`E=check "#X" "ex x: x in X & (all y: y in X => x = y)"); *)
  assert(`E=check "all x: (x in X => x in Y) & (x in Y => x in X)" "X = Y");
  assert(`E=check "ex x: (x in X & ~(x in Y)) | (x in Y & ~(x in X))" "X ~= Y");
  assert(`E=check "X sub Y" "all x: x in X => x in Y");
  (* assert(`E=check "X < Y" "(all x: x in X => x in Y) & ex y: y in Y & ~(y in X)"); *)
  (* assert(`E=check "Y > X" "(all x: x in X => x in Y) & ex y: y in Y & ~(y in X)"); *)
  assert(`E=check "X = {}" "X sub X+1");
  assert(`E=check "X ~= {}" "X+1 ~= X");
  assert(`E=check "X = {} & Y = {}" "X = Y & X = Y+1");
  assert(`E=check "X sub Y+1" "all x: x in X => (ex y: y = x - 1 & y in Y)");
  assert(`L=check "0 in P & (all p: p in P => (ex q: q = p + 1 & q in P))" "n in P");
  (* assert(`E=check "X = 2047_2" "(all x: x in X <=> x < 11)"); *)
  (* assert(`E=check "X = 4294967295_2" "(all x: x in X <=> x <= 31)"); *)
  (* assert(`E=check "X = 4294967295_2 & 100 in X" "false"); *)
  assert(`L=check "X = Y union Z & Z = X \\ Y" "Z inter Y = {}");
  assert(`L=check "X = Y inter Z & x in X" "x in Y & x in Z");
  assert(`E=check "all X Y: X sub Y => ex Z: X = Y \\ Z" "true");
  assert(`E=check "X = Y union Z" "ex X' Y' Z': X' = X+1 & Y' = Y+1 & Z' = Z+1 & X' = Y' union Z'");
  assert(`E=check "x = max X => x in X" "true");
  assert(`E=check "x = max X" "x in X & (all y: y in X => y < x + 1)");
  assert(`E=check "x = min X" "x in X & (all y: y in X => x < y + 1)");
  assert(`E=check "x = min X & x = max X" "x in X & all y: y in X -> x=y");
  assert(`E=check "~(X = {})" "ex x y: x = max X & y = min X");

  construction := Formula.brzozowski_m2l;
  assert(`E=check "x<y & x>y" "false");
  assert(`L=check "x<y | x>y" "true");
  assert(`E=check "x<=y => y<=z => x<=z" "true");
  assert(`E=check "x<y & y<x" "false");
  assert(`L=check "ex y: x<y" "true");
  assert(`G=check "ex Y: all y: x<y => y in Y" "false");
  assert(`E=check "ex Y: all y: y<x => y in Y" "true");
  assert(`E=check "(all x: x<=t => x in X1) => (all x: x<=t => x in X1)" "true");
  assert(`E=check "(all x: x<=t => x in X1 => x in X2) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2)" "true");
  assert(`E=check "(all x: x<=t => x in X1 => x in X2 => x in X3) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2) => (all x: x<=t => x in X3)" "true");
  assert(`E=check "all t: (all x: x<=t => x in X1) => (all x: x<=t => x in X1)" "true");
  assert(`E=check "all t: (all x: x<=t => x in X1 => x in X2) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2)" "true");
  assert(`E=check "all t: (all x: x<=t => x in X1 => x in X2 => x in X3) => (all x: x<=t => x in X1) => (all x: x<=t => x in X2) => (all x: x<=t => x in X3)" "true");

  assert(`E=check "ex X: all x: x in X" "true");
  assert(`E=check "all x: ex y: x < y" "all X: X = empty");
  
  ()

let _ = Util.exit_on_parsing_errors main ()
