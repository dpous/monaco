(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(*benchmarks from
  Minimization of Symbolic Automata, D'Antoni & Veanes, POPL 2014*)

open Monaco
open Formula

let x n = "x" ^ string_of_int n

let rec upt i n = if i >= n then [] else i :: upt (i + 1) n

let testa n =
  List.fold_right (fun i -> ex1 (x i)) (upt 0 n) (List.fold_right cnj
    (List.map2 (fun i j -> lt (x i) (x j)) (upt 0 (n - 1)) (upt 1 n)) tt)

let testb n =
  ex2 "A" (List.fold_right (fun i -> ex1 (x i)) (upt 0 n) (cnj
    (List.fold_right cnj (List.map2 (fun i j -> lt (x i) (x j)) (upt 0 (n - 1)) (upt 1 n)) tt)
    (List.fold_right cnj (List.map (fun i -> mem (x i) "A") (upt 0 n)) tt)))

let testc n =
  dsj (testb n)
  (ex2 "C" (ex1 "y" (mem "y" "C")))

let testd n =
  let aux n = dsj (cnj (lt (x (n - 1)) (x n)) (mem (x (n - 1)) "A")) (mem (x (n - 1)) "C") in
  let rec f n = match n with
    | 2 -> aux 2
    | _ -> cnj (f (n - 1)) (aux n) in
  ex2 "C" (ex2 "A" (List.fold_right (fun i -> ex1 (x i)) (upt 0 (n + 1)) (f n)))

let run_test test1 test2 l h = for i=l to h; do
  let e,f = (test1 i,test2 i) in
  let t,_ = Common.time (fun () -> assert (None=equiv e f)) () in
  Format.printf "%2i: %5.2fs\n%!" i t
done; ()

let _ =
  run_test testa (fun _ -> tt) 15 20;
  run_test testb (fun _ -> tt) 15 20;
  run_test testc (fun _ -> tt) 15 20;
  run_test testd (fun _ -> tt) 10 14;
  (* run_test testd (fun _ -> tt) 14 14; *)
  Stats.print Format.std_formatter

