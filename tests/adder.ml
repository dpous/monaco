(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

open Monaco
open Formula

let ball x f = fa1 "ballx" (imp (mem "ballx" x) f)
let subset x y = ball x (mem "ballx" y)
let equal x y = cnj (subset x y) (subset y x)
let first x = neg(ex1 "firstx" (lt "firstx" x))
let last x = neg(ex1 "lastx" (lt x "lastx"))
let successor sucx x =
  cnj (lt x sucx) (neg (ex1 "sucx" (cnj (lt x "sucx") (lt "sucx" sucx))))

let mod_two a b c d = iff a (iff b (iff c d))
let at_least_two a b c d = iff d (dsj (cnj a b) (dsj (cnj b c) (cnj a c)))

let add s a b =
  ex2 "C"
     (cnj
       	(fa1 "zero" (imp (first "zero") (neg(mem "zero" "C"))))
	(fa1 "p"
	      (cnj
		 (mod_two (mem "p" a) (mem "p" b) (mem "p" "C") (mem "p" s))
		 (at_least_two (mem "p" a) (mem "p" b) (mem "p" "C")
	   		       (fa1 "sucp" (imp (successor "sucp" "p")
   						 (mem "sucp" "C"))))
	      )
	)
     )

let smaller_add s a b =
  fa1 "p" (mod_two (mem "p" a) (mem "p" b) (mem "p" "C") (mem "p" s))


let _ =
  (* trace := true; *)
  (* cat (add "S" "A" "B"); exit 0; *)
  let e,f = add "S" "A" "B", add "S" "B" "A" in
  let t,_ = Common.time (fun () -> assert (None=equiv e f)) () in
  Format.printf "// %5.2fs\n%!" t;
  Stats.print Format.std_formatter;
  (* Format.printf "%s@." (Trace.render !hk "\""); *)
  ()
