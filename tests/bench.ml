(*******************************************************************)
(*     This is part of MonaCo, it is distributed under the         *)
(*  terms of the GNU Lesser General Public License version 3       *)
(*           (see file LICENSE for more details)                   *)
(*                                                                 *)
(*  Copyright 2014-2016:                                           *)
(*  Damien Pous. (CNRS, LIP - ENS Lyon, UMR 5668)                  *)
(*  Dmitriy Traytel. (ETH Zürich)                                  *)
(*******************************************************************)

(** a few benchmarks *)

open Monaco
open Formula
       
let phi n =
  let rec cimp j =
    if j=n then mem "x" ("X"^string_of_int n)
    else imp (mem "x" ("X"^string_of_int j)) (cimp (j+1))
  in
  let rec dimp j =
    if j=n then fa1 "x" (imp (le "x" "t") (mem "x" ("X"^string_of_int n)))
    else imp (fa1 "x" (imp (le "x" "t") (mem "x" ("X"^string_of_int j)))) (dimp (j+1))
  in
  imp (fa1 "x" (imp (le "x" "t") (cimp 1))) (dimp 1)

let _ =
  for i=1 to 50; do
    let t,_ = Common.time (fun () -> assert (None=equiv (phi i) tt)) () in
    Format.printf "%2i: %5.2fs\n%!" i t
  done;
  Stats.print Format.std_formatter
